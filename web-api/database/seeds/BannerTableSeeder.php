<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Faker\Provider\Lorem,Faker\Provider\Internet; 
use Carbon\Carbon;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker::create();
        $faker->addProvider(new Lorem($faker));
        $images=['uploads/632ff6719ef8580ee163fe58631e13f2.jpg','uploads/cd620b3f2affce53cd9ec4fe4c61ef1d.jpg','uploads/eeb597aacffb210451f4c396a2b8a7cf.jpg'];
        foreach(range(1, 10) as $index)
        {

            \App\Models\Banner::create([
                'image'                 =>$images[array_rand($images)],
                'heading'                        =>$faker->text($maxNbChars = 9), 
                'description'                    =>$faker->sentence($nbWords = 600, $variableNbWords = true),
                'link_text'                     =>$faker->text($maxNbChars = 9),
                'link'                          =>$faker->url(),
                'menu_id'                       =>$faker->randomElement(array(1,2,3,4,5)),
                'created_at'                   => date('Y-m-d H:i:s'),
                'updated_at'                   => date('Y-m-d H:i:s')
            ]);
        }
    }
}
