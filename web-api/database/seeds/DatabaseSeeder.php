<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //$this->call(SettingsTableSeeder::class);
         //$this->call(AleanTableSeeder::class);
        $this->call('UniversitysTableSeeder::class');
        $this->call('TestimonailTableSeeder::class');
        $this->call('BannerTableSeeder::class');
        $this->call('EventTableSeeder::class');
    }
}
