<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Faker\Provider\Lorem,Faker\Provider\Internet; 
use Carbon\Carbon;
use App\Models\University;

class TestimonailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $faker->addProvider(new Lorem($faker));
        $images=['uploads/632ff6719ef8580ee163fe58631e13f2.jpg','uploads/cd620b3f2affce53cd9ec4fe4c61ef1d.jpg','uploads/eeb597aacffb210451f4c396a2b8a7cf.jpg'];
        foreach(range(1, 10) as $index)
        {

            \App\Models\Testimonial::create([
                'author_name'                  =>$faker->firstName(),
                'author_photo'                 =>$images[array_rand($images)],
                'title'                        =>$faker->text($maxNbChars = 9), 
                'statement'                    =>$faker->sentence($nbWords = 600, $variableNbWords = true),
                'created_at'                   => date('Y-m-d H:i:s'),
                'updated_at'                   => date('Y-m-d H:i:s')
            ]);
        }
        DB::table('testimonial')->update(['author_photo'=>$images[array_rand($images)]]);
    }
}
