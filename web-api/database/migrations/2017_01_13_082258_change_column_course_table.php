<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course', function (Blueprint $table) {
            $table->dropColumn(['universitysID','course_offer','fees']); 
            $table->string('name',255)->nullable();
            $table->string('parent_id',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course', function (Blueprint $table) {
            $table->integer('universitysID')->unsigned();          
            $table->string('course_offer',255)->nullable();            
            $table->string('fees',255)->nullable();
            $table->dropColumn(['name','parent_id']); 
      
        });
    }
}
