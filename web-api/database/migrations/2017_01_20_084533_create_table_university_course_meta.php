<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUniversityCourseMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_course_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('university_courses_id')->unsigned();
            $table->string('type');
            $table->string('meta_key');
            $table->text('meta_value');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('university_course_meta');
    }
}
