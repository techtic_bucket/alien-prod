<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('last_education',255)->nullable();
            $table->string('last_education_percentage',255)->nullable();
            $table->string('interested_degree',255)->nullable();
            $table->string('interested_course',255)->nullable();
            $table->string('interested_country',255)->nullable();
            $table->string('markes_ietls',255)->nullable();
            $table->string('markes_toefl',255)->nullable();
            $table->string('markes_pte',255)->nullable();
            $table->string('markes_sat',255)->nullable();
            $table->string('markes_gre',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_educations');
    }
}
