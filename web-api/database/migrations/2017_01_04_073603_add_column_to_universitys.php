<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUniversitys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('universitys', function (Blueprint $table) {
            $table->dropColumn('component_filter');
            $table->string('email',255)->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('universitys', function (Blueprint $table) {
            $table->text('component_filter')->nullable();
            $table->dropColumn('email');
            $table->dropColumn('description');
        });
    }
}
