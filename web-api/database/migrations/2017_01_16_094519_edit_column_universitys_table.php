<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnUniversitysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('universitys', function (Blueprint $table) {
            $table->renameColumn('location', 'campus');
            $table->string('web_links',255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('universitys', function (Blueprint $table) {
            $table->renameColumn('campus','location');
            $table->dropColumn('web_links');
        });
    }
}
