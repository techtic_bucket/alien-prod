<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditColumnUniversitysMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('universitys_meta', function (Blueprint $table) {
             $table->string('meta_key',255)->nullable()->after('key');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('universitys_meta', function (Blueprint $table) {
           $table->dropColumn('meta_key');
        });
    }
}
