<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->index()->nullable();
            $table->string('title')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->text('body')->nullable()->change();
            $table->string('author')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('category_id');
            $table->string('title')->nullable(false)->change();
            $table->text('description')->nullable(false)->change();
            $table->text('body')->nullable(false)->change();
            $table->string('author')->nullable(false)->change();
        });
    }
}
