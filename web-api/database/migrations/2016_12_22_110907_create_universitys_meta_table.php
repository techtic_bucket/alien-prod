<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitysMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universitys_meta', function (Blueprint $table) {
            $table->increments('id');     
            $table->integer('universitys_id')->unsigned()->nullable();
            $table->string('type',255)->nullable(); 
            $table->string('key',255)->nullable();            
            $table->text('value')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universitys_meta');
    }
}
