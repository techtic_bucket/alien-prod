<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('contact_number',255)->nullable();
            $table->text('address')->nullable();
            $table->string('country',255)->nullable();
            $table->string('state',255)->nullable();
            $table->string('zip_code',255)->nullable();
            $table->string('qualification',255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['contact_number','address','country','state','zip_code','qualification']);
        });
    }
}
