<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable; 

class MenuItem extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function MenuDDArray($args = [])
    {
        if(!isset($args['select']))
            $args['select'] = '*';
        else if($args['select'] == '')
            $args['select'] = '*';

        if(empty($args['where']))
            $records = $this->select($args['select'])->get();
        else
            $records = $this->select($args['select'])->where($args['where'])->get();

        $return_array= '';

        foreach ($records as $key => $value) {
            $return_array[$value->id] = $value->name;
        }
        return $return_array;
    }
}
