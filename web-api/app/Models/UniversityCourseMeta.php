<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class UniversityCourseMeta extends Model
{
    
    use CrudTrait;
	protected $table = 'university_course_meta';
	protected $fillable = ['university_courses_id', 'type','meta_key', 'meta_value'];

	function getMetaKeyName(){
		if($this->type == 'custom'){
			return $this->meta_key;
		}else{
			return ucfirst(str_replace(['-', '_'], ' ', $this->meta_key));
		}
	}

	function Component(){
		return $this->hasOne(Component_filter::class, 'id', 'meta_key');
	}
}
