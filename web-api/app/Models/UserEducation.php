<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class UserEducation extends Model
{
    
    use CrudTrait;
    protected $table = 'user_educations';
    protected $fillable = ['id','user_id','last_education','last_education_percentage','interested_degree','interested_course','interested_country','markes_ietls','markes_toefl','markes_pte','markes_sat','markes_gre'];
   
    public function User(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function Course(){
        return $this->hasOne(Stream::class, 'id', 'interested_course');
    }
    public function Degree(){
        return $this->hasOne(Course::class, 'id', 'interested_degree');
    }
}