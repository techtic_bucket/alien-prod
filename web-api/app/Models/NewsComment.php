<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class NewsComment extends Model
{
    use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'news_comment';
    protected $fillable = ['articles_id','user_id','comment','status'];

    public function articles()
    {
        return $this->belongsTo('App\Models\Articles','articles_id');
    }
    public function users()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function getdropdownstatus(){
        $html='<a href="javascript:void(0)" class="'.$this->id.'" onclick="change_dropdown_'.$this->id.'()">'.ucfirst($this->status).'</a>';
    	$html.='<select name="status" id='.$this->id.' class="status hide" onchange="change_status_'.$this->id.'()" >';
    	/*$html.='<option value="active" ('.$this->status.'=="active") ? "selected":"">Active</option>';
    	$html.='<option value="inactive" ('.$this->status.'=="inactive") ? "selected":"">Inactive</option>';*/
    	if($this->status=="active")
    		$html.='<option value="active" selected="true">Active</option>';
    	else
    		$html.='<option value="active">Active</option>';
    	
    	if($this->status=="inactive")
    		$html.='<option value="inactive" selected="true">Inactive</option>';
    	else
    		$html.='<option value="inactive">Inactive</option>';

    	$html.='</select>';
    	$html.='
        <script> 
        function change_dropdown_'.$this->id.'()
        { 
            $("#'.$this->id.'").removeClass("hide");
            $(".'.$this->id.'").addClass("hide"); 
        } 
        function change_status_'.$this->id.'(){ 
            var value=$("#'.$this->id.'").val(); 
            $.ajax({ 
                url : "'.url("admin/newscomment/change_status/".$this->id).'",
                data : {value:value},
                method : "POST", 
                success : function(result){  
                    new PNotify({
                          title: "Modified",
                          text: "The status has been modified successfully.",
                          type: "success"
                      });
                     $("#'.$this->id.'").addClass("hide");
                     $(".'.$this->id.'").removeClass("hide");
                     $(".'.$this->id.'").text("");
                     $(".'.$this->id.'").text(result);
                  },
                    error: function(result) {
                      // Show an alert with the result
                      new PNotify({
                          title: "Not Modified",
                          text: "There\'s been an error. Your status might not have been modified.",
                          type: "warning"
                      });
                      $("#'.$this->id.'").addClass("hide");
                     $(".'.$this->id.'").removeClass("hide");
                  } }); }</script>';
    	return $html;

    }
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/
}
