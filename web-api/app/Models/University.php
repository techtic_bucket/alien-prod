<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Kodeine\Metable\Metable;
use Illuminate\Support\Facades\Storage;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\UniversityRate;
class University extends Model
{
	use CrudTrait;
	use Metable;
    use Sluggable;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

	protected $table = 'universitys';
	protected $primaryKey = 'id';
	protected $fillable = ['name', 'campus','image','cover_image','email','description','web_links','slug','map_embed_link','apply_now_link'];
	protected $metaTable = 'universitys_meta';

    public function sluggable()
    {
        return [
            'slug' => ['source' => 'name']
        ];
    }
     public function UniversityRate()
    {
        return $this->hasMany('App\Models\UniversityRate','universitys_id','id');

    }
    function avgRatings(){ 
        return $this->UniversityRate()         
        ->selectRaw('avg(rating) as average, universitys_id,count(*) as total') 
        ->groupBy('universitys_id'); 
    }
   /* function avgRatings(){ 
        return $this->UniversityRate()         
        ->selectRaw('avg(rating) as average, universitys_id') 
        ->groupBy('universitys_id'); 
    }*/
    public function getrating(){
        $rate_data=UniversityRate::where('universitys_id',$this->id)->get();
        $total=$rate_data->count();
        $sum=$rate_data->sum('rating');
        if($total!=0)
            $rate=$sum/$total;
        else
            $rate=0;
        return number_format((float)$rate, 1, '.', '');
        
    }
	public function getImageAttribute($value){
		return Storage::url('app/public/'.$value);
	}
	public function setImageAttribute($value)
    { 
        $attribute_name = "image";
        $disk = "public";
        $destination_path =  'uploads';

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $destination_path.'/'.$filename;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $width='300';
            $height='200';
            $background = \Image::canvas($width, $height);
   
		    $image = \Image::make($image)->resize($width, $height, function ($c) {
		       // $c->aspectRatio();
		       $c->upsize();
		    });
		    \Storage::disk($disk)->put($destination_path.'/thumb_'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }
    public function getCoverImageAttribute($value){
        return Storage::url('app/public/'.$value);
    }
    public function setCoverImageAttribute($value)
    { 
        $attribute_name = "cover_image";
        $disk = "public";
        $destination_path =  'uploads';

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $destination_path.'/'.$filename;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $width='300';
            $height='200';
            $background = \Image::canvas($width, $height);
   
            $image = \Image::make($image)->resize($width, $height, function ($c) {
               // $c->aspectRatio();
               $c->upsize();
            });
            \Storage::disk($disk)->put($destination_path.'/thumb_'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }


    function Courses(){
        return $this->hasMany(UniversityCourse::class, 'university_id', 'id');
    }
}
