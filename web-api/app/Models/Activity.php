<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
/**
 * Class MenuItem
 */
class Activity extends Model
{
    protected $table = 'activity';

    protected $fillable = ['type','description','data','user_id'];

    function university(){
    	return $this->hasOne('App\Models\University','id','data');
    }
    function post(){
    	return $this->hasOne('App\Models\Post','id','data');
    	
    }



}