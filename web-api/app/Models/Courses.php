<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Courses extends Model
{
    use CrudTrait;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'course';
    protected $fillable = ['name','parent_id'];
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];


    function universities(){
    	return $this->hasMany(UniversityCourse::class,'course_id','id');
    }

	function courses($parent_id=null){
		if(!empty($parent_id))
			$courses=Courses::find($parent_id);
		else
			$courses=Courses::find($this->parent_id);

		if($courses)
			return $courses->name;
		else
			return '';
	}
	function courses_name(){
		return $this->hasOne('App\Models\Courses', 'id', 'parent_id');
	}
}