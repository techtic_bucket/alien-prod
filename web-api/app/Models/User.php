<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;// <---------------------- and this one
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    
    use CrudTrait;
    use HasRoles; 
    use  Notifiable;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'users';
    protected $hidden = ['password'];
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name','email','password','contact_number','address','country','state','zip_code','qualification','jwt_token','reset_code','reset_date', 'facebook_id','google_id', 'gender', 'dob','image'];
    // protected $hidden = [];
    // protected $dates = [];
    public function newsComment()
    {
        return $this->hasMany('App\Models\NewsComment');
    }
    public function userrole()
    {
        return $this->belongsToMany('Backpack\PermissionManager\app\Models\Role','role_users')->withPivot(['role_id','user_id']);
    }
    public function userpermission()
    {
        return $this->belongsToMany('Backpack\PermissionManager\app\Models\Permission','permission_users')->withPivot(['permission_id','user_id']);
    }
    public function isAdmin() {
        return $this->hasRole('Admin');
    }
    public function UserEducation() {
        return $this->hasOne(UserEducation::class, 'user_id', 'id');
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    public function universityRate()
    {
        return $this->belongsTo('App\Models\UniversityRate','user_id','id');
    }
    public function getImageAttribute($value){
        return Storage::url('app/public/'.$value);
    }

    public function setImageAttribute($value)
    { 
        
        $attribute_name = "image";
        $disk = "public";
        $destination_path =  'uploads';

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $destination_path.'/'.$filename;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }elseif(is_object($value)){
            $filename = md5($value.time()).'.jpg';

            $value->storeAs($destination_path, $filename, $disk);

            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    } 
   
}