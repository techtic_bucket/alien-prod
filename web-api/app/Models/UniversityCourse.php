<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class UniversityCourse extends Model
{
    
    use CrudTrait;
	protected $table = 'university_courses';
	protected $fillable = ['id','university_id', 'course_id','stream_id'];
    
    function University(){
    	return $this->hasOne(University::class, 'university_id', 'id');
    }
    function Course(){
    	return $this->hasOne(Courses::class, 'id', 'course_id');
    }
    function Stream(){
    	return $this->hasOne(Stream::class, 'id','stream_id');
    }
    function Meta(){
    	return $this->hasMany(UniversityCourseMeta::class, 'university_courses_id', 'id');
    }
}
