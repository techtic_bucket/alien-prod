<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
class Post extends Model
{
    use CrudTrait;
    use Sluggable;

     /*
	|--------------------------------------------------------------------------
	| GLOBAL VARIABLES
	|--------------------------------------------------------------------------
	*/

    protected $table = 'posts'; 
    protected $fillable = ['id','title','slug','image','description','author','status']; 
	public function sluggable()
    {
        return [
            'slug' => ['source' => 'title']
        ];
    }
    public function postcategory()
    {
        return $this->belongsToMany('App\Models\BlogCategory','post_category')->withPivot(['post_id','blog_category_id'])->withTimestamps();
    }
    
    public function posttag()
    {
        return $this->belongsToMany('App\Models\BlogTag','post_tag')->withPivot(['post_id','blog_tag_id'])->withTimestamps();
    }

    public function users($value='')
    {
        return $this->hasOne('App\Models\User','id','author');
    }


     public function BlogComment()
    {
        return $this->hasMany('App\Models\BlogComment','posts_id','id');

    }
    

	public function getImageAttribute($value){
		return Storage::url('app/public/'.$value);
	}


	public function setImageAttribute($value)
    { 
        $attribute_name = "image";
        $disk = "public";
        $destination_path =  'uploads';

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $destination_path.'/'.$filename;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }
    /*
	|--------------------------------------------------------------------------
	| FUNCTIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| RELATIONS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| SCOPES
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| ACCESORS
	|--------------------------------------------------------------------------
	*/

    /*
	|--------------------------------------------------------------------------
	| MUTATORS
	|--------------------------------------------------------------------------
	*/

}
