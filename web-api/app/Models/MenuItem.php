<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MenuCategory;
/**
 * Class MenuItem
 */
class MenuItem extends \Backpack\MenuCRUD\app\Models\MenuItem
{
    protected $table = 'menu_items';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'type',
        'link',
        'page_id',
        'parent_id',
        'lft',
        'rgt',
        'depth',
        'category_id'
    ];
/*
    function getCategoryName(){

    }*/
    public function category()
    {
        return $this->belongsTo('App\Models\MenuCategory','category_id','id');
    }
public function getlinkfordelete(){
    
    $html='<a class="btn btn-xs btn-default" href="javascript:void(0)"   onclick="confirm'.$this->id.'()" ><i class="fa fa-trash"></i> Delete</a>';

    $html.='<script> 
        function confirm'.$this->id.'()
        { 
           if(confirm("Are you sure you want to delete this item?it will delete banner which is related this menu."))
           {
            delete_url = "'.url('admin/menu-item/delete/'.$this->id).'";
            
            $.ajax({
                  url: delete_url,
                  success: function(result) {
                      // Show an alert with the result
                      new PNotify({
                          title: "Item Deleted",
                          text: "The item has been deleted successfully.",
                          type: "success"
                      });
                      // delete the row from the table
                      location.reload();
                  },
                  error: function(result) {
                       new PNotify({
                          title: "NOT deleted",
                          text: "There\'s been an error. Your item might not have been deleted.",
                          type: "warning"
                      });
                  }
              });
           }else{

                    new PNotify({
                  title: "Not deleted",
                  text: "Nothing happened. Your item is safe.",
                  type: "info"
              });
           }
           
        }</script>';
        return $html;
    }

    public static function getTree($value='')
    { 
        if($value !=''){
            if(!is_numeric($value)){ 
                $k = MenuCategory::where('title',$value)->first(); 
                if($k){
                    $menu = $k->menuitem()->with(array('page' => function($query){
                        $query->select('id','title','slug');
                    }))->orderBy('lft')->get();
                }
            } else { 
                $k = MenuCategory::find($value); 
                if($k){
                    $menu = $k->menuitem()->with(array('page' => function($query){
                        $query->select('id','title','slug');
                    }))->orderBy('lft')->get();
                }
            }   
        } else
            $menu = self::with(array('page' => function($query){
                    $query->select('id','title','slug');
                }))->orderBy('lft')->get();

        if (isset($menu) && $menu->count()) {
            foreach ($menu as $k => $menu_item) {
                $menu_item->children = collect([]);

                foreach ($menu as $i => $menu_subitem) {
                    if ($menu_subitem->parent_id == $menu_item->id) {
                        $menu_item->children->push($menu_subitem);

                        // remove the subitem for the first level
                        $menu = $menu->reject(function ($item) use ($menu_subitem) {
                            return $item->id == $menu_subitem->id;
                        });
                    }
                }
            }
        }else{
            $menu = null;
        }

        return $menu;
    }

     public static function tree() {

        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', NULL)->with(array('page' => function($query){
            $query->select('id','title','slug');
        }))->orderBy('lft','asc')->get();

    }
}