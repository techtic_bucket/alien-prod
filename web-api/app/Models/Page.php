<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
/**
 * Class Page
 */
class Page extends \Backpack\PageManager\app\Models\Page
{
    use CrudTrait;
    use Sluggable;

    protected $table = 'pages';

    public $timestamps = true;

    protected $fillable = [
        'template',
        'name',
        'title',
        'slug',
        'content',
        'extras'
    ];

    protected $guarded = [];

    public function Menu($value='')
    {
        return $this->hasMany('App\Models\MenuItem','page_id','id')->where('type','=','page_link');
    }
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}