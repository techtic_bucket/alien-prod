<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Stream extends Model
{

    use CrudTrait;
	protected $table = 'streams';
	protected $fillable = ['name'];
}
