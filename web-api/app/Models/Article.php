<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
/**
 * Class MenuItem
 */
class Article extends \Backpack\NewsCRUD\app\Models\Article
{
    protected $table = 'articles';

    public $timestamps = true;

    protected $fillable = ['category_id','title','slug','content','image','status','date','featured'];


     public function newsComment()
    {
        return $this->hasMany('App\Models\NewsComment','articles_id','id');
    }
    public function getlinkforcomment(){
    	$html='<a class="btn btn-xs btn-default" href="'.url('admin/newscomment').'"><i class="fa fa-comment-o"></i> View Comments</a>';
    	return $html;
    }
    public function getImageAttribute($value){
        return Storage::url('app/public/'.$value);
    }

    public function setImageAttribute($value)
    { 
        $attribute_name = "image";
        $disk = "public";
        $destination_path =  'uploads';

        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            \Storage::disk($disk)->delete($this->image);

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        }

        // if a base64 was sent, store it in the db
        if (starts_with($value, 'data:image'))
        {
            // 0. Make the image
            $image = \Image::make($value);
            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';
            // 2. Store the image on disk.
            $destination_path.'/'.$filename;
            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
            // 3. Save the path to the database
            $this->attributes[$attribute_name] = $destination_path.'/'.$filename;
        }
    }


}