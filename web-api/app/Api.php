<?php
namespace App;

class API {
    function __construct() {
    }

    static function error($message = 'Unknown error', $http_status = "400") {
        self::respond(array(
            'message' => $message,
        ), 'error', $http_status);
    }
    static function success($data, $http_status = "200") {
      if(is_string($data)){
        $new_data['message'] = $data;
      }else{
        $new_data = $data;
      }
        self::respond($new_data, 'success', $http_status);
    }

    static function json($data) {
    	
      $status = ($data['status']) ? $data['status'] : 'success';
      $http_status = ($data['status_code']) ? $data['status_code'] : 200;
      
      self::respond($data, $status, $http_status);
    }

    static function get_json($data, $status = 'ok') {
    global $json_api;
    // Include a status value with the response
    if (is_array($data)) {
      $data = array_merge(array('status' => $status), $data);
    } else if (is_object($data)) {
      $data = get_object_vars($data);
      $data = array_merge(array('status' => $status), $data);
    }
    $json = json_encode($data);
    return $json;
  }

  static function respond($result, $status = 'ok', $http_status = 200) {
    ob_clean();
    
    global $json_api;
    $result['status_code'] = $http_status;
    $json = self::get_json($result, $status);
    if (!empty($_REQUEST['dev'])) {
      if (!headers_sent()) {
        header('HTTP/1.1 200 OK');
        header('Content-Type: text/plain; charset: UTF-8', true);
      }else{
        echo "<pre>";
      }
      echo self::prettify($json);
    }else{
      self::output($json, $http_status);
    }
    exit;
  }
  
  static function output($result, $http_status = 200) {
   	$charset = defined('DB_CHARSET') ? DB_CHARSET : 'utf8';
    if (!headers_sent()) {
      self::status_header($http_status);
      header("Content-Type: application/json; charset=$charset", true);
    }
    echo $result;
  }

  
  static function callback($callback, $result) {
    $charset = get_option('blog_charset');
    if (!headers_sent()) {
      self::status_header(200);
      header("Content-Type: application/javascript; charset=$charset", true);
    }
    echo "$callback($result)";
  }
  
  
  static function prettify($ugly) {
    $pretty = "";
    $indent = "";
    $last = '';
    $pos = 0;
    $level = 0;
    $string = false;
    while ($pos < strlen($ugly)) {
      $char = substr($ugly, $pos++, 1);
      if (!$string) {
        if ($char == '{' || $char == '[') {
          if ($char == '[' && substr($ugly, $pos, 1) == ']') {
            $pretty .= "[]";
            $pos++;
          } else if ($char == '{' && substr($ugly, $pos, 1) == '}') {
            $pretty .= "{}";
            $pos++;
          } else {
            $pretty .= "$char\n";
            $indent = str_repeat('  ', ++$level);
            $pretty .= "$indent";
          }
        } else if ($char == '}' || $char == ']') {
          $indent = str_repeat('  ', --$level);
          if ($last != '}' && $last != ']') {
            $pretty .= "\n$indent";
          } else if (substr($pretty, -2, 2) == '  ') {
            $pretty = substr($pretty, 0, -2);
          }
          $pretty .= $char;
          if (substr($ugly, $pos, 1) == ',') {
            $pretty .= ",";
            $last = ',';
            $pos++;
          }
          $pretty .= "\n$indent";
        } else if ($char == ':') {
          $pretty .= ": ";
        } else if ($char == ',') {
          $pretty .= ",\n$indent";
        } else if ($char == '"') {
          $pretty .= '"';
          $string = true;
        } else {
          $pretty .= $char;
        }
      } else {
        if ($last != '\\' && $char == '"') {
          $string = false;
        }
        $pretty .= $char;
      }
      $last = $char;
    }
    return $pretty;
  }
  
  static function replace_unicode_escape_sequence($match) {
    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
  }


  static function status_header( $code, $description = '' ) {
      if ( ! $description ) {
          $description = self::get_status_header_desc( $code );
      }
      if ( empty( $description ) ) {
          return;
      }

      $protocol = self::get_server_protocol();
      
      $status_header = "$protocol $code $description";
   
      @header( $status_header, true, $code );
  }
  

  static function get_status_header_desc( $code ) {
      $header_to_desc = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',

        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        226 => 'IM Used',

        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',

        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        421 => 'Misdirected Request',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',

        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
      );
      

      return $header_to_desc[$code];
         
    }
    static function get_server_protocol() {
      $protocol = $_SERVER['SERVER_PROTOCOL'];
      if ( ! in_array( $protocol, array( 'HTTP/1.1', 'HTTP/2', 'HTTP/2.0' ) ) ) {
          $protocol = 'HTTP/1.0';
      }
      return $protocol;
  }
}

