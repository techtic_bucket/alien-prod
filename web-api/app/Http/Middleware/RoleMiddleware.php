<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role='admin';
        $permission='All Access';
    
        if (Auth::guest()) {
                return redirect('admin/login');
            }

            if (!$request->user()->hasRole($role)) {
               return redirect('admin/logout');
            }

            if (! $request->user()->can($permission)) {
               return redirect('admin/logout');
            }
        return $next($request);
    }
}
