<?php

namespace App\Http\Controllers;

use App\Models\Footer;

class FooterController extends Controller
{ 
    /*public function index($slug)
    {
        $page = Page::findBySlug($slug);

        if (!$page)
        {
            abort(404, 'Please go back to our <a href="'.url('').'">homepage</a>.');
        }

        $this->data['title'] = $page->title;
        $this->data['page'] = $page->withFakes();

        return view('pages.'.$page->template, $this->data);
    }*/

    public function getSingleFooter($slug=''){
        if($slug == 'undefined') $slug = '';
        $result = Footer::where('meta_key',$slug)->first();
        return json_encode($result);
    }

    public function splitbanner($banner_content ='')
    {
        if($banner_content == '') return $banner_content;
        $banner_content = json_decode(trim($banner_content));

        if($banner_content->heading === NULL) return json_encode($banner_content);
        $heading_string = trim($banner_content->heading);
        $first_space = strpos($heading_string, ' ');
        $banner_content->heading  = [substr($heading_string, 0 , $first_space), trim(substr($heading_string, $first_space))];
        return $banner_content;
    }

    public function update(){
        $meta_keys = \Request::input('key');
        \Session::put('url.intended', \URL::previous());


        foreach ($meta_keys as $key => $value) {
            $value = trim($value);
            if($value =='') continue;
            $current_meta_key = $value;
            $current_meta_value = str_replace(get_storage_link('/').'/', '', \Request::input($value));

            if(starts_with($current_meta_value, 'data:image')){
                $current_meta_value = uplaod_raw_image($current_meta_value);
            } 
            if(($value =='footer_column_first'))
            {
                $value_updated = array();
                $value_updated['title'] = \Request::input('title');
                $value_updated['description'] = \Request::input('description');
                $image_array        =   array();
                $oldcontent = unserialize(get_constant_value('footer_column_first'));
                $image_section = $oldcontent['links'];
                for ($i=1; $i <=4 ; $i++) { 
                    $j = ($i-1);
                    $image = isset($image_section[$j]['image']) ? $image_section[$j]['image'] : "" ;
                    $image_array[]  = [
                        'url'   => \Request::input('link_'.$i), 
                        'image' => check_and_return_image_upload(\Request::input('image_'.$i), $image)
                    ];
                }
                $value_updated['links'] = $image_array;

           
                $value_updated    = serialize($value_updated);
                update_constant_value($value, $value_updated);
                return \Redirect::intended('/');
            }
            if(($value =='footer_column_second')){
                $value_updated = array();
                //$value_updated[0] = \Request::input('menu_item');

                $value_updated['title'] = \Request::input('title');
                $value_updated['menu_item'] = \Request::input('menu_item');

                $value_updated    = serialize($value_updated);
                update_constant_value($value, $value_updated);
                return \Redirect::intended('/');
            }


            if($value =='footer_column_third'){


                $value_updated = array();
                $value_updated['title'] = \Request::input('title');
                $value_updated['menu_item'] = \Request::input('menu_item');
                $value_updated    = serialize($value_updated);
                update_constant_value($value, $value_updated);
                return \Redirect::intended('/');
            }

            /*if($value =='footer_address'){
                $current_meta_value = \Request::input('address');
            }

            if($value =='footer_contact'){
                $current_meta_value = \Request::input('contact');
            }
*/
            if($value =='footer_column_forth'){
                $value_updated = array();
                
                $value_updated['title']  = \Request::input('title');
                $value_updated['address']  = \Request::input('address');
                $value_updated['phone']  = \Request::input('phone');
                $value_updated['email']  = \Request::input('email');
                $value_updated['website']  = \Request::input('website');
                $value_updated     = serialize($value_updated); 
                update_constant_value($value, $value_updated);
                return \Redirect::intended('/');               
            }
            if($value =='footer_column_fifth'){
                $value_updated = array();
                
                $value_updated['copyright']  = \Request::input('copyright');
                $value_updated     = serialize($value_updated); 
                update_constant_value($value, $value_updated);
                return \Redirect::intended('/');               
            }


            /*$constant = Footer::where('meta_key', $value)->first();
            $constant->meta_value = $current_meta_value;
            $constant->save();*/
        }
        return \Redirect::intended('/');
    }

    // public function 

    public function update_home_right($value='')
    {
        \Session::put('url.intended', \URL::previous());
        $count          = \Request::input('right_content_count');
       
        $previous_content = unserialize(get_constant_value('home_statics_right_content'));
        // dd($previous_content);
        $insert_info=array();
        for($i=1;$i<=$count;$i++) {
            $title=$description=$image=$image_name='';
            if(\Request::input('title_'.$i) == null) continue;
            $title          = \Request::input('title_'.$i);
            $description    = \Request::input('description_'.$i);
            $image          = \Request::input('image_'.$i);
            // echo '<br/>';
            // continue;
           
            if($image=='')
            {
                $image_name     =   '';            
            }
            else
            {
                $image_name = '';
                if(starts_with($image, 'data:image')){
                 $image_name = get_storage_link(uplaod_raw_image($image));
                } else{
                    $image_name     =   \Request::input('image_'.$i);                    
                    // $image_name     =   str_replace(get_storage_link('/').'/', '', \Request::input($value));          

                }

            }
            
            $insert_info[]=array('title'=>$title, 'description'=>$description, 'image_name'=>@$image_name);
        }
        
        $new_content = serialize($insert_info);

        update_constant_value('home_statics_right_content', $new_content);
        update_constant_value('home_statics_right_list', \Request::input('options'));
        return \Redirect::intended('/');
    }
}