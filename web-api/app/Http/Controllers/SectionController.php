<?php

namespace App\Http\Controllers;

use App\Models\Technotiles;
use App\Models\HomeLanguages;
use App\Models\Constant;
use App\Models\HomeStatistics;

class SectionController extends Controller
{ 

    public function view($slug=''){
        if($slug == 'technotile'){
            $data['all'] = Technotiles::orderBy('column_no','asc')->orderBy('lft','asc')->get();
            return view('partial/technotile', $data);
        }
        if($slug == 'homelanguage'){
            $data['all'] = HomeLanguages::orderBy('lft','asc')->get();
            return view('partial/homelanguages', $data);
        }
        if($slug == 'homestatistics'){
            $data['left']           =   HomeStatistics::orderBy('lft','asc')->get();
            $data['right_upper'] = get_constant_value('home_statics_right_content');
            $data['right_bottom'] = get_constant_value('home_statics_right_list');
            return view('partial/homestatistics', $data);
        }
        if($slug == 'footer'){
            return view('partial/footer');
        }
    }
    
}