<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsletter;

class NewsletterApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index()
    {
    	$email=\Request::input('email');
        if(empty($email)){
            \Api::error('Please enter email id.');
        }
        
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            \Api::error("Please enter valid email address.");
        }
    	
    	$data=Newsletter::where('email',$email)->where('status','active')->first();
    	if($data){
    		\Api::success('You’re Already Subscribed!');
    	}else{

    		$data_email=Newsletter::where('email',$email)->first();
    		if($data_email){
    			$update_val=array('status'=>'active');
    			$news_val=Newsletter::find($data_email->id);
    			$news=$news_val->update($update_val);
    		}else{
    			$insert_val=array('email'=>$email,'status'=>'active');
    			$news=Newsletter::create($insert_val);
    		}
    		
    		if($news)
    			\Api::success('Thanks for subscribing newsletter!');
    		else
    			\Api::error('Error occured while doing subscribe process. Please try again!!!');
    	}
    	
    }

}