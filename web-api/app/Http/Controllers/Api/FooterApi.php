<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Footer;

class FooterApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
    	$data=Footer::get()->toArray();
    	$new_data = [];
    	foreach ($data as  $key => $val) {
    		$val['meta_value'] = maybe_unserialize( $val['meta_value'] );
    		switch ($val['meta_key']) {
    			case 'footer_column_first':
    			case 'footer_column_forth':
    			case 'footer_column_fifth':
    				$val['meta_value'] = array_map(function($val){
    					return maybe_unserialize( $val );
    				}, $val['meta_value']);
    				break;
    			
    			case 'footer_column_second':
    			case 'footer_column_third':
                    $val['meta_value'] = $val['meta_value'];
    				$val['meta_value']['menu_item'] = \App\Models\MenuItem::getTree($val['meta_value']['menu_item']);
    				break;
    		}

    		$new_data[$val['meta_key']] = $val['meta_value'];
    		$data[$key] = $val;
    	}
    	\Api::success(['data'=>$new_data]);
    }

}