<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\University,App\Models\User,App\Models\Activity;

class ApplyNowApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
        $post=\Request::all();

        if(empty($post['name']))
            \Api::error("Please enter name.");
        if(empty($post['email']))
            \Api::error("Please enter email.");
        if(empty($post['mobile']))
            \Api::error("Please enter mobile number.");
        if(empty($post['comment']))
            \Api::error("Please enter comment.");
        if(empty($post['university_id']))
            \Api::error("university not found.");

        $name=$post['name'];
        $email=$post['email'];
        $mobile=$post['mobile'];
        $comment=$post['comment'];
        $id=$post['university_id'];
        $user_id=$post['user_id'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            \Api::error("Please enter valid email address.");
        }
        
        $university=University::find($id);
        if($user_id){
            $update_val=array('type'=>'apply_now','data'=>$id,'user_id'=>$user_id);
            Activity::create($update_val);    
        }
        
        $to=$university['email'];
        $mail_template_id=get_option('apply_now_mail');
        $mail_data=array('student_name'=>$name,
                         'email'=>$email,
                         'mobile'=>$mobile,
                         'comment'=>$comment);
        
        if(@send_mail($to,$mail_template_id,$mail_data)){
            $admin_to=get_option('from_email');
            @send_mail($admin_to,$mail_template_id,$mail_data);
             $data['message']="Your application submit to university.";
            \Api::success($data['message']);
        }
        else
            \Api::error("Error while apply for university.Please try again.");
    }
}