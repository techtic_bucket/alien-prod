<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Newsletter;

class ContactUsApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index()
    {
        $post=\Request::all();
        if(!isset($post['first_name']) || empty($post['first_name'])){
            \Api::error('Please enter first name.');
        }
        if(!isset($post['last_name']) || empty($post['last_name'])){
            \Api::error('Please enter last name.');
        }
        if(!isset($post['email']) || empty($post['email'])){
            \Api::error('Please enter email id.');
        }
        if(!isset($post['phone']) || empty($post['phone'])){
            \Api::error('Please enter phone no.');
        }
        if(!isset($post['message']) || empty($post['message'])){
            \Api::error('Please enter message.');
        }
        $email=$post['email'];
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            \Api::error("Please enter valid email address.");
        }

        $admin_to=get_option('contact_from_email');

        $template_id=get_option('contact_mail');
        $array_val=array('first_name'=>$post['first_name'],
                          'last_name'=>$post['last_name'],
                          'email'=>$post['email'],
                          'phone'=>$post['phone'],
                          'message'=>$post['message']);

        if(@send_mail($admin_to,$template_id,$array_val)){
            $data['message']="Your email successfully send.";
            \Api::success($data);
        }else{
            \Api::error('Error while sending email. Please try again.');
        }
    }

}