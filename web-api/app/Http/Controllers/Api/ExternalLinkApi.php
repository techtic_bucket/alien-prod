<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExternalLink; 

class ExternalLinkApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
        
        $limit = \Request::get('limit');
        $today=('Y-m-d');
        $limit = isset($limit) ? $limit : 9;
    	$data=ExternalLink::paginate($limit)->toArray();
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }
    

}
