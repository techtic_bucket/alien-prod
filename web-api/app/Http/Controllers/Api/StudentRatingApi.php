<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UniversityRate,App\Models\University,App\Models\Activity;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class StudentRatingApi extends Controller
{
    function __constrctor(){
    	
    }
   
    /*public function Index(){
    	$post=\Request::all();
        $limit = isset($post['limit']) ? $post['limit'] : 9;
        $slug=isset($post['slug']) ? $post['slug'] : '';
        $user=isset($post['user']) ? $post['user'] : '';

        $studentRate=$data=UniversityRate::with('users','university');
        if(!empty($slug)){
            $universityId=University::where('slug',$slug)->first();
            $studentRate=$studentRate->where('universitys_id',$universityId->id);
        }
        if(!empty($user)){
            $user_id=$user->id;
            $studentRate=$studentRate->where('user_id',$user_id);
        }

        $studentRate=$data=$studentRate->orderBy('created_at','desc')->paginate($limit)->toArray();
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }*/
    //get all rating list
   public function Index(){
        $post=\Request::all();
        $limit = isset($post['limit']) ? $post['limit'] : 9;
        $studentRate=$data=UniversityRate::with('users','university')->orderBy('created_at','desc')->paginate($limit)->toArray();
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }
    //get all rating by universiy id
    public function ratinglist(){
    	
    	$slug=\Request::get('slug');
    	$limit=\Request::get('limit');
    	$limit = isset($limit) ? $limit : 9; 
    	$universityId=University::where('slug',$slug)->first();
    	$ratinglist=$data=UniversityRate::with('users','university')->where('universitys_id',$universityId->id)->orderBy('created_at','desc')->paginate($limit)->toArray();
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }
    //get all rating by user id
    public function student_rating(Request $request){
        $input = $request->all();
        
        $user = JWTAuth::parseToken()->authenticate();
        $user_id=$user->id;
        
        $limit=$input['limit'];
        $limit = isset($limit) ? $limit : 9;
       
        $university_rate=$data=UniversityRate::with('university')->where('user_id',$user_id)->orderBy('created_at','desc')->paginate($limit)->toArray();
         $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }
    function rating(Request $request){ 
        try{ 
            if($user = JWTAuth::parseToken()->authenticate()){ 
                $post = $request->all(); 
                    
                   
                    $user_id=$user->id;
                    if(empty($post['rating']))
                        \Api::error("Please enter rating.");
                    if(empty($post['universitys_id']))
                        \Api::error("university not found.");
                     if(empty($post['rate_comment']))
                        \Api::error("Please enter comment.");
                    
                    $news_id=$post['rating'];
                    $universitys_id=$post['universitys_id'];
                    $val=array('user_id'=>$user_id,
                               'rating'=>$news_id,
                               'universitys_id'=>$universitys_id,
                               'comment'=>$post['rate_comment']);
                    $blog=UniversityRate::where('universitys_id',$universitys_id)->where('user_id',$user_id)->first();
                     
                    if($blog)
                    {
                        $error="Unable to update rating, Please try again.";
                        $blog_comment=UniversityRate::find($blog['id']);
                        $blog_comment->update($val);
                        $success="Rating successfully updated.";
                    }else{
                        $error="Unable to add rating, Please try again.";
                        $blog_comment=UniversityRate::create($val);
                        $success="Rating successfully added.";

                    }

                    $update_val=array('type'=>'university_rating','data'=>$universitys_id,'user_id'=>$user_id);
                    Activity::create($update_val); 

                    if($blog_comment)
                        \Api::success($success);
                    else
                        \Api::error($error);
            }else{ 
                \Api::error("User is not found."); 
            } 
        } 
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){ 
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']); 
            exit(); 
        } 
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){ 
            \Api::error(['type'=> 'invalid','message' => 'Token is invalid']); 
            exit(); 
        } 
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){ 
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']); 
            exit(); 
        } 
         
    }
     
}