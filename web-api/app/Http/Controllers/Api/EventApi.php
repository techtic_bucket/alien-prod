<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Events,App\Models\BlogComment,App\Models\User;

class EventApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
        
        $slug = \Request::get('slug');
    	
        if(!empty($slug))
        {
            $event=Events::where('slug',$slug)->orderBy('date','desc')->first();
            
            if($event)
                \Api::success(['data'=>$event]);
            else
                \Api::error("Unable to get event info, Please try again.");
                \Api::success(['data'=>$event]);
           
        }else{

            $limit = \Request::get('limit');
            $today=date('Y-m-d');
            $limit = isset($limit) ? $limit : 9;

            $data=Events::where('status','Active');

            $upcomming = \Request::get('upcomming');
            if(isset($upcomming) && !empty(isset($upcomming))){
                $data->where('date','>=',$today);
            }
            
            $search = \Request::get('search');
            if(isset($search) && !empty(isset($search))){
                $data->where(function($q) use ($search){
                    $q->where('title', 'LIKE', "%{$search}%")
                    ->orWhere('description', 'LIKE', "%{$search}%");
                });
            }
            
            $data= $data->orderBy('date','desc')->paginate($limit)->toArray();

            $data['status'] = "success";
            $data['status_code'] = "200";
            \Api::json($data);
        }
        
       
    }
  /*  public function event_detail(){
        $post=\Request::all();
        $id=$post['event_id'];
        if(empty($id))
            \Api::error("Event id is not found.");

        $event=Events::find($id);
        
        if($event)
            \Api::success(['data'=>$event]);
        else
            \Api::error("Unable to get event info, Please try again.");
    }*/

}