<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Hash;
class LoginApi extends Controller
{

    use AuthenticatesUsers;
    
    function __constrctor(){
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function Index(){
        $post=\Request::all();
        if(empty($post['email']))
            \Api::error("Please enter email.");
        if(empty($post['password']))
            \Api::error("Please enter password.");
        $data = [];
        $credentials=array('password'=>$post['password'],'email'=>$post['email']);
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                \Api::error('Invalid email or password.');
            }
        } catch (JWTException $e) {
            $data['message']='could_not_create_token';
            \Api::error($data);
            \Api::error('Error while login please try again.');
        }

        $data=compact('token');
        $update_info=array('jwt_token'=>$data['token']);
        $user=User::with('UserEducation')->where('email',$credentials['email'])->first();

        if($user->user_education){
            $edu = $user->user_education->toArray();
            $edu['interested_degree_data'] = $user->user_education->course;
            $edu['interested_course_data'] = $user->user_education->degree;

            $user->user_education = $edu;
        }


        $data['user'] = $user;
        $user->update($update_info);
        \Api::success($data); 
            

    }
    public function get_user_details(Request $request)
    {
        $input = $request->all();
        $user = JWTAuth::toUser($input['token']);

        $edu = $user->user_education->toArray();
        $edu['interested_degree_data'] = $user->user_education->course;
        $edu['interested_course_data'] = $user->user_education->degree;

        $user->user_education = $edu;

        $data['user'] = $user;
        \Api::success(['data'=>$data]); 
    }

    public function change_password(Request $request)
    {
        try{
            if($user = JWTAuth::parseToken()->authenticate()){
                $post = $request->all();
                $fields=[
                'old_password' => 'Old Password',
                'new_password' => 'New Password',
                'confirm_password' => 'Confirm Password',
                ];

                foreach ($fields as $key => $value) {
                    if(!isset($post[$key]) || empty($post[$key])){
                        \Api::error("{$value} is require.");
                    }
                }

                if(strlen($post['new_password'])<6){
                    \Api::error("Please enter password at least 6 characters.");
                }
                if($post['confirm_password'] != $post['new_password']){
                    \Api::error("Both passwords are not same.");
                }

                if(!Hash::check($post['old_password'], $user->password)){
                    \Api::error("Old Password is wrong.");
                }
                $user->password = bcrypt($post['new_password']);
                $user->save()   ;
                
                \Api::success(['message' => "Password successfully changed."]);
            }else{
                \Api::error("User is not found.");
            }
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
            \Api::error(['type'=> 'invalid','message' => 'Token is invalid']);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']);
            exit();
        }
        
    
    }


    public function social_login(Request $request){
        $input = $request->all();
        $input = $input['data'];
        if($input['provider'] == "facebook"){
            $user = User::with('UserEducation')->where('facebook_id', '=', $input['uid'])->first();

            if(!$user && isset($input['email']) && !empty($input['email'])){
                $user = User::where('email', '=', $input['email'])->first();
            }
            if(!($user)){
                $password=$input['uid'].random_int( 100, 999 );
                $user =  User::create([
                    'name' => $input['name'],
                    'email' => isset($input['email']) && !empty($input['email']) ? $input['email'] : $input['uid'],
                    'password' => bcrypt($password),
                    'facebook_id' => $input['uid'],
                    ]);
                        $to=isset($input['email']) && !empty($input['email']) ? $input['email'] : '';
                        $mail_template_id=get_option('registration_mail_facebook');
                        $mail_data=array('name'=>$input['name'],'email'=>$to,'password'=>$password);
                        @send_mail($to,$mail_template_id,$mail_data);

                        if($user){
                            $admin_to=get_option('from_email');
                            $admin_mail_template_id=get_option('registration_mail_admin');
                            $admin_mail_data=array('name'=>$post['name']);
                            @send_mail($admin_to,$admin_mail_template_id,$admin_mail_data);
                        }

            }
            $token = JWTAuth::fromUser($user);
            $user->jwt_token = $token;
            $user->save();
            \Api::success(['token'=>$token, 'user'=> $user]);
        }else if($input['provider'] == "google"){
            $user = User::with('UserEducation')->where('google_id', '=', $input['uid'])->first();

            if(!$user && isset($input['email']) && !empty($input['email'])){
                $user = User::where('email', '=', $input['email'])->first();
            }

            if(!$user){
                $password=$input['uid'].random_int( 100, 999 );
                $user =  User::create([
                    'name' => $input['name'],
                    'email' => isset($input['email']) && !empty($input['email'])  ? $input['email'] : $input['uid'],
                    'password' => bcrypt($password),
                    'google_id' => $input['uid'],
                    ]);
                    $to=isset($input['email']) && !empty($input['email']) ? $input['email'] : '';
                        $mail_template_id=get_option('registration_mail_gmail');
                        $mail_data=array('name'=>$input['name'],'email'=>$to,'password'=>$password);
                        @send_mail($to,$mail_template_id,$mail_data);

                        if($user){
                            $admin_to=get_option('from_email');
                            $admin_mail_template_id=get_option('registration_mail_admin');
                            $admin_mail_data=array('name'=>$post['name']);
                            @send_mail($admin_to,$admin_mail_template_id,$admin_mail_data);
                        }
            }
            $token = JWTAuth::fromUser($user);
            $user->jwt_token = $token;
            $user->save();
            \Api::success(['token'=>$token, 'user'=> $user]);
        }else{
            \Api::error(['message'=>'Error while login.', 'data' => $input]);  
        }
        
    }
}