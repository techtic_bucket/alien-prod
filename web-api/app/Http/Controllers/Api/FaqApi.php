<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq;

class FaqApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){

    	$data=Faq::where('status','Active')->get();
        
    	\Api::success(['data'=>$data]);	
    }
    public function askquestion(){
        $post=\Request::all();

        if(empty($post['name']))
            \Api::error("Please enter name.");
        if(empty($post['email']))
            \Api::error("Please enter email.");
        if(empty($post['question']))
            \Api::error("Please enter question.");

        
        $email=$post['email'];
        $name=$post['name'];
        $question=$post['question'];
        
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            \Api::error("Please enter valid email address.");
        }


        $to=$email;
        $mail_template_id=get_option('ask_question_mail');
        $mail_data=array('name'=>$name,'email'=>$email,'question'=>$question);
        @send_mail($to,$mail_template_id,$mail_data);

        if($to){
            $admin_to=get_option('ask_question_from_mail');
            $admin_mail_template_id=get_option('ask_question_admin_mail');
            $admin_mail_data=array('name'=>$name,'email'=>$email,'question'=>$question);
            @send_mail($admin_to,$admin_mail_template_id,$admin_mail_data);
            
            $data['message']="Your question has been successfully submitted.";
            \Api::success($data);
        }else{
            \Api::error('Error while submitting question.');
        }
    }
   
}