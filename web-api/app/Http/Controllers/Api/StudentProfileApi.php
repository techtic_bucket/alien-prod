<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User,
    App\Models\UniversityRate,
    App\Models\Activity,
    App\Models\UserEducation;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Storage;
class StudentProfileApi extends Controller
{

    use AuthenticatesUsers;
    
    function __constrctor(){
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function Index(){

    }

    function get(Request $request){
        try{
            if($user = JWTAuth::parseToken()->authenticate()){
                \Api::success(['data' => $user]);
            }else{
                \Api::error("User is not found.");
            }
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
    }
    function update(Request $request){
        $post = $request->all();
        try{
            if($user = JWTAuth::parseToken()->authenticate()){
                
                if(isset($post["education_update"]) && $post["education_update"] == 1){

                    $number = [
                        'last_education_percentage' => 'Last Education Percentage',
                        'markes_ietls' => 'IETLS Markes ',
                        'markes_toefl' => 'TOEFL Markes',
                        'markes_pte' => 'PTE Markes',
                        'markes_sat' => 'SAT Markes',
                        'markes_gre' => 'GRE Markes'
                    ];
                    foreach ($number as $key => $value) {
                        if(isset($post[$key]) && !empty($post[$key]) && !is_numeric($post[$key])){
                            \Api::error("{$value} must be number.");
                        }
                    }
                    if($user->user_education){
                        $education = UserEducation::find($user->user_education->id);
                    }else{
                        $education = new UserEducation();
                        $education->user_id = $user->id;
                    }
                    $education->last_education = $post['last_education'];
                    $education->last_education_percentage = $post['last_education_percentage'];
                    $education->interested_degree = $post['interested_degree'];
                    $education->interested_course = $post['interested_course'];
                    $education->interested_country = $post['interested_country'];
                    $education->markes_ietls = $post['markes_ietls'];
                    $education->markes_toefl = $post['markes_toefl'];
                    $education->markes_pte = $post['markes_pte'];
                    $education->markes_sat = $post['markes_sat'];
                    $education->markes_gre = $post['markes_gre'];
                    $education->save();

                    $edu = $education->toArray();
                    $edu['interested_degree_data'] = $education->course;
                    $edu['interested_course_data'] = $education->degree;


                    $user->user_education = $edu;

                    \Api::success(['data' => $user]);
                }else{

                    $fields = array(
                        'name' => 'Name',
                        'email' => 'Email',
                        'contact_number' => 'Contact number',
                        'address' => 'Address',
                        'state' => 'State',
                        'country' => 'Country',
                        'zip_code' => 'Zip code',
                        'gender' => 'Gender',
                        'dob' => 'Date of Birth',
                        'qualification' => 'Qualification',
                    );
                    
                    foreach ($fields as $key => $value) {
                        if(!isset($post[$key]) || empty($post[$key])){
                            \Api::error("{$value} is require.");
                        }
                    }
                    $email = $post["email"];
                        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                           \Api::error("Please enter valid email address.");
                        }

                    $user_data=User::where('email',$post['email'])->where('id', '!=', $user->id)->first();
                    if($user_data){
                        \Api::error("The email id has already been used. Please use another email id");
                    }

                    if(isset($post['dob']) && isset($post['dob']['day'])){
                        $post['dob']['day'] = str_pad($post['dob']['day'], 2, '0', STR_PAD_LEFT);
                        $post['dob']['month'] = str_pad($post['dob']['month'], 2, '0', STR_PAD_LEFT);
                        $post['dob']['year'] = str_pad($post['dob']['year'], 4, '0', STR_PAD_LEFT);
                        $post['dob'] = date('Y-m-d',strtotime($post['dob']['day']."-".$post['dob']['month']."-".$post['dob']['year']));
                    }
                    $today=date('Y-m-d');
                    if($post['dob']>=$today)
                        \Api::error("Date of Birth must be less than today.");

                    foreach ($fields as $key => $value) {
                        if(isset($post[$key]) && !empty($post[$key])){
                            $user->$key = $post[$key];
                        }
                    }
                    $user->save();

                    $edu = $user->user_education->toArray();
                    $edu['interested_degree_data'] = $user->user_education->course;
                    $edu['interested_course_data'] = $user->user_education->degree;

                    $user->user_education = $edu;
                    \Api::success(['data' => $user]);
                }
            }else{
                \Api::error("User is not found.");
            }
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
        
    }
    public function student_rating(Request $request){
        $input = $request->all();
        $user = JWTAuth::toUser($input['user_id']);
        $user_id=$user->id;
        $university_rate=UniversityRate::with('university')->where('user_id',$user_id)->get();
        \Api::success(['data'=>$university_rate]);
    }

    
    public function image_upload(Request $request){
        $input = $request->all();
        try{
            if($user = JWTAuth::parseToken()->authenticate()){
                $user->image = $input['profile_image'];
                $user->save(); 

                $edu = $user->user_education->toArray();
                $edu['interested_degree_data'] = $user->user_education->course;
                $edu['interested_course_data'] = $user->user_education->degree;

                $user->user_education = $edu;
                \Api::success(['data' => $user]);
            }else{
                \Api::error("User is not found.");
            }
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){
            \Api::error(['type'=> 'invalid','message' => $e->getMessage()]);
            exit();
        }
    }

    public function student_activity(Request $request){
        $input = $request->all();
        $user = JWTAuth::parseToken()->authenticate();
        $limit=$input['limit'];
        $limit = isset($limit) ? $limit : 9;
        
        $user_id=$user->id;
        $activity=Activity::with('university','post')->where('user_id',$user_id)->orderBy('created_at','desc')->paginate($limit)->toArray();
        \Api::success(['data'=>$activity]);
    }
    
}
