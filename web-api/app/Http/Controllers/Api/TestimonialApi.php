<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Testimonial;

class TestimonialApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
    	$limit = \Request::get('limit');
    	$limit = isset($limit) ? $limit : 9;
    	$data = Testimonial::orderBy('created_at','desc')->paginate($limit)->toArray();
    	$data['status'] = "success";
    	$data['status_code'] = "200";
    	\Api::json($data);
    }

}