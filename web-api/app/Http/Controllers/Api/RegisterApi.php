<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;


class RegisterApi extends Controller
{

    
    function __constrctor(){
        
    }

    public function Index(){
        $post=\Request::all();

       if(empty($post['name']))
            \Api::error("Please enter name.");
        if(empty($post['email']))
            \Api::error("Please enter email.");
        if(empty($post['password']))
            \Api::error("Please enter password.");
        if(empty($post['confirm_password']))
            \Api::error("Please enter confirm password.");
        if(empty($post['contact_number']))
            \Api::error("Please enter contact number.");
        if(empty($post['address']))
            \Api::error("Please enter address.");
        if(empty($post['state']))
            \Api::error("Please enter state.");
        if(empty($post['country']))
            \Api::error("Please enter country.");
        if(empty($post['zip_code']))
            \Api::error("Please enter zip code.");
        if(empty($post['qualification']))
            \Api::error("Please enter qualification.");

        


        
        $email = $post["email"];
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            \Api::error("Please enter valid email address.");
        }
        if(strlen($post['password']) <6 ){
            \Api::error("Please enter password at least 6 characters.");
        }
        if($post['password'] != $post['confirm_password'])
            \Api::error("Both passwords are not match.");

        $post['password']=bcrypt($post['password']);
        $user_data=User::where('email',$post['email'])->first();
        
        if($user_data){
            \Api::error("The email id has already been used. Please use another email id");
        }
        
        $user=User::create($post);
        $to=$post['email'];
        $mail_template_id=get_option('registration_mail');
        $mail_data=array('name'=>$post['name']);
        @send_mail($to,$mail_template_id,$mail_data);

        if($user){
            $admin_to=get_option('from_email');
            $admin_mail_template_id=get_option('registration_mail_admin');
            $admin_mail_data=array('name'=>$post['name']);
            @send_mail($admin_to,$admin_mail_template_id,$admin_mail_data);
            
            $data['message']="User successfully registered.";
            \Api::success($data);
        }else{
            \Api::error('Error while User registering.');
        }
    }
    public function forget_password(){
        $post=\Request::all();
        if(empty($post['email']))
            \Api::error("Please enter email.");

        $email=$post['email'];
        $user=User::where('email',$email)->first();
        if(empty($user))
            \Api::error("Incorrect Email or Email is not found.");

        $code = generate_random_password();
        $url=url('').'/';
        if(strpos($url,"web-api/"))
           $url=str_replace("web-api/","",$url);
        if(strpos($url,"public/"))
           $url=str_replace("public/","",$url);
        if(strpos($url,"index.php/"))
           $url=str_replace("index.php/","",$url);
       
        
        $reset_link = $url.'#/reset-password/'.$code;

        $user_detail=User::find($user['id']);

        $update_info=array('reset_code'=>$code,'reset_date'=>date('Y-m-d H:i:s'));
        $user_detail->update($update_info);

        $to=$post['email'];
        $name=$user_detail['name'];
        $mail_template_id=get_option('forget_password_mail');
        $mail_data=array('name'=>$name,'reset_link'=>$reset_link);
        @send_mail($to,$mail_template_id,$mail_data);

        if($user_detail){
            $data['message']="Reset password link sent to your email.";
            \Api::success($data);
        }else{
             \Api::error('there is some error occured.Please try later.');
        }

    }
    public function reset_password(){
      //  $post=\Request::all();
        $code =  \Request::get('code');

        if(empty($code))
            \Api::error("Incorrect code or Code is not found.");

        $user =$data= User::where('reset_code', '=', $code)->first();
       
        if(empty($user))
            \Api::error("Incorrect code or Code is not found.");

        $today=date('Y-m-d H:i:s');
        $user_date= date("Y-m-d H:i:s", strtotime('+24 hours', strtotime($user['reset_date'])));
        if($user_date>=$today){
           \Api::success(['data'=>$data]);
       }
       else{
           \Api::error('Reset password link is expired.please try again.');
       }
    }
    public function update_password(){
        $post=\Request::all();

        $password = $post['new_password'];
        $confirm_password = $post['confirm_password'];
        $code=$post['code'];
        $id=$post['id'];
        if(empty($post['new_password']))
            \Api::error("Please enter password.");

        if(empty($post['confirm_password']))
            \Api::error("Please enter confirm password.");

        if(strlen($post['new_password'])<6){
                    \Api::error("Please enter password at least 6 characters.");
                }

                
        if($password != $confirm_password){
            \Api::error("Password and Confirm Password does not match.");
        }
      
        $userdata = User::where('reset_code', '=', $code)
                  ->where('id', '=', $id)
                  ->first();

        if(!empty($userdata)){
            $user_val=User::find($id);
            $update_info=array('reset_date'=>null,'reset_code'=>null,'password'=>bcrypt($password));
            $user_val->update($update_info);
            \Api::success("Your password updated Successfully.");
        }else
            \Api::error("Error while updating password.plese try again.");
        
    }
}
