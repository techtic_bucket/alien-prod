<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article,App\Models\NewsComment;

class NewsApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
        $data=Article::where('status','PUBLISHED');
        $slug =  \Request::get('slug');
        if(isset($slug)){
            $data=$data->where('slug',$slug)->orderBy('date','desc')->first();
            if($data)
                \Api::success(['data'=>$data]);
            else
                \Api::error('No result found.');
        }else{

            $limit = \Request::get('limit');
            
            $limit = isset($limit) ? $limit : 9;

            $search = \Request::get('search');
            if(isset($search) && !empty(isset($search))){
                $data->where(function($q) use ($search){
                    $q->where('title', 'LIKE', "%{$search}%")
                    ->orWhere('content', 'LIKE', "%{$search}%");
                });
            }


            $data=$data->orderBy('date','desc')->paginate($limit)->toArray();
            $data['status'] = "success";
            $data['status_code'] = "200";
            \Api::json($data);  
        }
      
    }
   /* public function get_news(){
        $post=\Request::all();
        $id=$post['id'];

    	$data=Article::find($id);

        $news_comment=NewsComment::where('articles_id',$id)->orderBy('created_at','desc')->paginate('5')->toArray();
        $data['news_comment']=$news_comment;

    	if($data)
            \Api::success(['data'=>$data]);
        else
            \Api::error('No result found.');
    }
    public function news_comment(){
        $post=\Request::all();
        $user_token=$post['user_token'];
        $user=User::where('jwt_token',$user_token)->first();
        if(!$user)
             \Api::error("User not found.");

        $user_id=$user->id;
        $news_id=$post['news_id'];
        $comment=$post['comment'];
        $val=array('user_id'=>$user_id,
                   'articles_id'=>$news_id,
                   'comment'=>$comment,
                   'status'=>'inactive');

        $news_comment=NewsComment::create($val);

        if($news_comment)
            \Api::success("Comment successfully added.");
        else
            \Api::error("Unable to create comment, Please try again.");

    }*/

}
