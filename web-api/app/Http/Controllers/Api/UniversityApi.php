<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\University,
    App\Models\User,
    App\Models\UniversityRate,
    App\Models\Course,
    App\Models\Stream,
    App\Models\UniversityCourseMeta,
    App\Models\Component_filter,
    App\Models\UniversityCourse;
use JWTAuth; 
use Tymon\JWTAuth\Exceptions\JWTException;

class UniversityApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){

        $post=\Request::all();
        $limit = isset($post['limit']) ? $post['limit'] : 9;

        $university=University::select('universitys.*')->with('Courses','avgRatings');

       

        if(isset($post['keyword']) && !empty($post['keyword'])){
            $university->where('name', 'LIKE', "%{$post['keyword']}%");
        }
       
        if(isset($post['campus']) && !empty($post['campus'])){
            $university->where('campus', 'LIKE', "%{$post['campus']}%");
        }
       
       
        if(isset($post['country']) && !empty($post['country'])){
            $university->where('country', 'LIKE', "%{$post['country']}%");
        }
       
        if(!empty($post['course']) || !empty($post['stream']))
        {
              $university=$university->WhereHas('Courses',function($query) use($post){
                    $query->where(function ($query) use($post) {
                        if(!empty($post['course']))
                            $query->where('course_id',$post['course']);
                        if (!empty($post['stream']))
                            $query->where('stream_id',$post['stream']);

                    });
              });
        }

        $variable = [
            'duration',
            'intakes',
            'fees',
            'last_education',
            'last_education_percentage',
            'entry_criteria',
            'marks_ietls',
            'marks_toefl',
            'marks_pte',
            'marks_sat',
            'marks_gre'
        ];

        $number = [
            'last_education',
            'last_education_percentage',
            'marks_ietls',
            'marks_toefl',
            'marks_pte',
            'marks_sat',
            'marks_gre'
        ];
        $component = [];
        foreach ($variable as $value) {
            if(isset($post[$value]) && !empty($post[$value]))
                $component[$value] = $post[$value];
        }
        $component = array_filter($component);



       if(count($component)>0)
        {
            $university=$university->join('university_courses','university_courses.university_id','=','universitys.id');

                $new_component = [];
                foreach ($component as $key => $value) {
                    $key = str_replace('comp_', '', $key);
                    $new_component[$key] = $value;
                }
            
                foreach ($new_component as $key => $value) {
                    if(!empty($value)){
                        $university=$university->join('university_course_meta as meta_'.$key,'meta_'.$key.'.university_courses_id','=','university_courses.id')
                            ->where(function($query) use($key, $value, $number){
                                if(is_array($value)){
                                    $query->where(function($q) use($key, $value){
                                        $q->where('meta_'.$key.'.type','component');
                                        $q->where('meta_'.$key.'.meta_key',$key);
                                        
                                        $q->where(function($q) use($key, $value){
                                            $q->where('meta_'.$key.'.meta_value','>=', $value[0]);
                                            $q->orWhere('meta_'.$key.'.meta_value', null);
                                        });
                                    })->orWhere(function($q) use($key, $value){
                                        $q->where('meta_'.$key.'.type','component');
                                        $q->where('meta_'.$key.'.meta_key',$key);
                                       // $q->where('meta_'.$key.'.meta_value','>=', $value[1]);

                                        $q->where(function($q) use($key, $value){
                                            $q->where('meta_'.$key.'.meta_value','>=', $value[1]);
                                            $q->orWhere('meta_'.$key.'.meta_value', null);
                                        });
                                    });
                                }else{
                                    $query->where('meta_'.$key.'.type','component');
                                    $query->where('meta_'.$key.'.meta_key',$key);
                                    //$query->where('meta_'.$key.'.meta_value','like', '%'.$value.'%');
                                    $query->where(function($q) use($key, $value, $number){
                                        if(in_array($key, $number)){
                                           $q->whereRaw("CAST(`meta_{$key}`.`meta_value` as SIGNED) <= CAST('{$value}' as SIGNED)");
                                        }else{
                                           $q->where('meta_'.$key.'.meta_value','like', '%'.$value.'%'); 
                                        }
                                        $q->orWhere('meta_'.$key.'.meta_value', null);
                                        $q->orWhere('meta_'.$key.'.meta_value', 0);
                                        //$q->orWhere('meta_'.$key.'.meta_value', "");
                                    });
                                }
                            });
                    }

                }
        }

        $data=$university->orderBy('universitys.created_at','desc')->distinct('universitys.id')->paginate($limit)->toArray();
        
        $data['status'] = "success";
        $data['status_code'] = "200";
        //print_r($data);
        //return;

        \Api::json($data);

    }

    public function interactive_assessment($id){
        $university = University::with(['courses' => function($q){
            return $q->with(['course','stream', 'meta' => function($q){
                return $q->with('component');
            }]);
        }])->where('slug',$id)->first();

        \Api::success(['data'=>$university]);
    }
    

    public function show($id){
        $university = University::with(['courses' => function($q){
            return $q->with(['course','stream', 'meta' => function($q){
                return $q->with('component');
            }]);
        }])->where('slug',$id)->first();

        \Api::success(['data'=>$university]);
    }

    public function compare(){
        $universites_id = \Request::get('universites_id');
        
        if(empty($universites_id))
            \Api::error("Compare university is not found.");

        $ids = array_map(function($item){ return $item['id']; }, $universites_id);

        if(!is_array($universites_id)){
            $universites_id = explode(',', $universites_id);
        }
    	$university = University::with('avgRatings')
        ->with(['courses' => function($q){
    		return $q->with(['course','stream', 'meta' => function($q){
    			return $q->with('component');
    		}]);
    	}])
        ->whereIn('id', $ids)
        ->get();

        $course = [];
        $stream = [];
        if(isset($universites_id)){
            foreach ($universites_id as $key => $value) {
                $course[$value['id']]['course'] = (isset($value['course']) && !empty($value['course'])) ? $value['course'] : "";
                $stream[$value['id']]['stream'] = (isset($value['stream']) && !empty($value['stream'])) ? $value['stream'] : "";
            }
        }
        $universities = [];
        foreach ($university as $key => $value) {
            $q = UniversityCourse::query();

            if(isset($course[$value->id]['course']) && !empty($course[$value->id]['course']) && isset($stream[$value->id]['stream']) && !empty($stream[$value->id]['stream'])){
                $q->where('course_id', $course[$value->id]['course']);
                $q->where('stream_id', $stream[$value->id]['stream']);
            }else{
                $q->where('course_id', '');
                $q->where('stream_id', '');
            }
            $q->where('university_id',$value->id);
            $q->with(['course','stream', 'meta' => function($q){
                return $q->with('component');
            }]);
            $v = $value->toArray();
            $v['selected_course_data'] = $q->first();

            if(isset($course[$value->id]['course']) && !empty($course[$value->id]['course']))
                $v['selected_course'] = $course[$value->id]['course'];

            if(isset($stream[$value->id]['stream']) && !empty($stream[$value->id]['stream']))
                $v['selected_stream'] = $stream[$value->id]['stream'];

            $universities[] = $v;
        }

        $components = [
            ['id'=>'duration', 'name'=>'Duration'],
            ['id'=>'intakes', 'name'=>'Intakes'],
            ['id'=>'fees', 'name'=>'Fees'],
            ['id'=>'last_education', 'name'=>'Education'],
            ['id'=>'last_education_percentage', 'name'=>'Education percentage'],
            ['id'=>'entry_criteria', 'name'=>'Entry Criteria'],
            ['id'=>'marks_ietls', 'name'=>'Marks IETLS'],
            ['id'=>'marks_toefl', 'name'=>'Marks TOEFL'],
            ['id'=>'marks_pte', 'name'=>'Marks PTE'],
            ['id'=>'marks_sat', 'name'=>'Marks SAT'],
            ['id'=>'marks_gre', 'name'=>'Marks GRE'],
        ];


    	\Api::success([
            'data'=> [
                'courses' => Course::all('id','name'),
                'stream' => Stream::all('id','name'),
                'components' => $components,
                'university' => $universities,
            ]
        ]);
    }
    /*public function rating(){
        $post=\Request::all();

        if(!isset($post['user_token']))
            \Api::error('You need to login to post your rating.');

        $user_token=$post['user_token'];
        $user=User::where('jwt_token',$user_token)->first();
        if(!$user)
             \Api::error("User not found.");

        $user_id=$user->id;
        $news_id=$post['rating'];
        $universitys_id=$post['universitys_id'];
        $val=array('user_id'=>$user_id,
                   'rating'=>$news_id,
                   'universitys_id'=>$universitys_id,
                   'comment'=>$post['comment']);
        $blog=UniversityRate::where('universitys_id',$universitys_id)->where('user_id',$user_id)->first();
        if($blog)
        {
            $error="Unable to update rating, Please try again.";
            $blog_comment=UniversityRate::find($blog['id']);
            $blog_comment->update($val);
            $success="Rating successfully updated.";
        }else{
            $error="Unable to add rating, Please try again.";
            $blog_comment=UniversityRate::create($val);
            $success="Rating successfully added.";

        }

        if($blog_comment)
            \Api::success($success);
        else
            \Api::error($error);
    }*/
    
  
    /*public function filter(){
        $post=\Request::all();
        $limit = isset($post['limit']) ? $post['limit'] : 9;

        $university=University::with('Courses');

        if(isset($post['keyword']) && !empty($post['keyword'])){
            $university->where('name', 'LIKE', "%{$post['keyword']}%");
        }
       
        if(!empty($post['courses']) || !empty($post['stream']) )
        {
            $courses=$post['courses'];
            $stream=$post['stream'];
              $university=$university->WhereHas('Courses',function($query) use($courses,$stream){
                    $query->where(function ($query) use($courses,$stream) {
                        if(!empty($courses))
                            $query->orWhere('course_id',$courses);
                        if (!empty($stream))
                            $query->orWhere('stream_id',$stream);

                    });
              });
        }
        $component=(isset($post['component'])) ? $post['component'] : [];
      
       if(count($component)>0)
        {
            $university=$university->join('university_courses','university_courses.university_id','=','universitys.id');
            
                foreach ($component as $key => $value) {
                    if(!empty($value)){
                        $university=$university->join('university_course_meta as meta_'.$key,'meta_'.$key.'.university_courses_id','=','university_courses.id')
                            ->where(function($query) use($key,$value){

                                    $query->where('meta_'.$key.'.type','component');
                                    $query->where('meta_'.$key.'.meta_key',$key);
                                    $query->where('meta_'.$key.'.meta_value','like', '%'.$value.'%');
                        });
                    }

                }
        }

        $data=$university->orderBy('universitys.created_at','desc')->paginate($limit)->toArray();
        
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);


    }*/


    function components(){
        $components = [];

        $q = UniversityCourseMeta::where('meta_key','fees')->where('meta_value', 'REGEXP', "^[0-9]+\\.?[0-9]*$");
        $min = $q->min('meta_value');
        $max = $q->max('meta_value');
        /*$min_round = 1;
        for($i=1; $i <= strlen($min)-1; $i++){
            $min_round = $min_round * 10;
        }
        $max_round = 1;
        for($i=1; $i <= strlen($max)-1; $i++){
            $max_round = $max_round * 10;
        }*/
        if($min == $max){
            $min = 0;
        }
        $value['min'] = $min;//floor($min / $min_round) * $min_round;
        $value['max'] = $max;//ceil($max / $max_round) * $max_round;
    
        $components['fees'] = $value;
      
        \Api::success(['data' => $components]);
    }

    function courses(){
        $Courses = \App\Models\Courses::get()->toArray();
        \Api::success(['data' => $Courses]);
    }
    function streams(){
        $Stream = \App\Models\Stream::get()->toArray();
        \Api::success(['data' => $Stream]);
    }
}