<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post,App\Models\BlogComment,App\Models\User,App\Models\Activity;
use JWTAuth; 
use Tymon\JWTAuth\Exceptions\JWTException;
class BlogApi extends Controller
{
    function __constrctor(){
    	
    }

    public function Index(){
        
        $limit = \Request::get('limit');
        $limit = isset($limit) ? $limit : 9;
    	
        $data = Post::orderBy('created_at','desc')
            ->with('postcategory','posttag','users','BlogComment')
            ->where('status','active');

        $search = \Request::get('search');
        if(isset($search) && !empty(isset($search))){
            $data->where(function($q) use ($search){
                $q->where('description', 'LIKE', "%{$search}%")
                ->orWhere('title', 'LIKE', "%{$search}%");
            });
        }

        $data = $data->paginate($limit)->toArray();
        $data['status'] = "success";
        $data['status_code'] = "200";
        \Api::json($data);
    }


    public function Show($id){ 
        if(is_numeric($id))
    	   $data=Post::with('postcategory','posttag','users','BlogComment')->find($id);
        else
           $data=Post::with('postcategory','posttag','users','BlogComment')->where('slug', $id)->first();

        if($data)
            \Api::success(['data'=>$data]);
        else
            \Api::error('No result found.');
       	
    }
    public function getblogcomment(){
        $slug = \Request::get('slug');
        if(empty($slug))
             \Api::error("Comment not found.");

        $post=Post::where('slug',$slug)->first();

        $post_comment=$data=BlogComment::with('users')->where('posts_id',$post['id'])->where('status','active')->get()->toArray();
        
        \Api::success(['data'=>$data]); 
    }
   /* public function blogcomment(){
        $post=\Request::all();
        $user_token=$post['user_token'];
        $user=User::where('jwt_token',$user_token)->first();
        if(!$user)
             \Api::error("User not found.");

        $user_id=$user->id;
        $news_id=$post['posts_id'];
        $comment=$post['comment'];
        $val=array('user_id'=>$user_id,
                   'posts_id'=>$news_id,
                   'comment'=>$comment,
                   'status'=>'inactive');

        $blog_comment=BlogComment::create($val);

        if($blog_comment)
            \Api::success("Comment successfully added.");
        else
            \Api::error("Unable to create comment, Please try again.");

    }*/
    public function blogcomment(Request $request){
         try{ 
            if($user = JWTAuth::parseToken()->authenticate()){ 
                $post = $request->all(); 
                
                if(!isset($post['comment']) || empty($post['comment']))
                    \Api::error("Please enter comment.");

                
                
                if(isset($post['comment_id'])){
                    $comment = BlogComment::find($post['comment_id']);
                    if($comment->user_id != $user->id){
                        \Api::error("You have not permission to edit comment. You can edit only own comments.");
                    }
                    $comment->comment = $post['comment'];
                    $comment->save();

                    if($comment)
                        $msg="Comment successfully updated.";
                    else
                        $msg="Unable to update comment, Please try again.";

                    $type='edit_blog_comment';
                }else{
                    
                    if(!isset($post['captcha']) || empty($post['captcha']))
                        \Api::error("Please enter captcha.");
                    
                    $comment = new BlogComment();
                    $comment->user_id = $user->id;
                    $comment->posts_id = $post['posts_id'];
                    $comment->comment = $post['comment'];
                    $comment->status = 'active';
                    $comment->save();

                    if($comment)
                        $msg="Comment successfully added.";
                    else
                        $msg="Unable to create comment, Please try again.";

                    $type='add_blog_comment';
                }
                
                $update_val=array('type'=>$type,'data'=>$comment['posts_id'],'user_id'=>$user->id);
                Activity::create($update_val);   

                 if($comment)
                        \Api::success($msg);
                    else
                        \Api::error($msg);

            }else{ 
                \Api::error("User is not found."); 
            } 
        } 
        catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){ 
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']); 
            exit(); 
        } 
        catch(\Tymon\JWTAuth\Exceptions\TokenInvalidException $e){ 
            \Api::error(['type'=> 'invalid','message' => 'Token is invalid']); 
            exit(); 
        } 
        catch(\Tymon\JWTAuth\Exceptions\JWTException $e){ 
            \Api::error(['type'=> 'expired','message' => 'Your login has been expired. Please login and try again.']); 
            exit(); 
        } 
    }

 
}
