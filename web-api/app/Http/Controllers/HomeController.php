<?php 
namespace App\Http\Controllers;

Use Illuminate\Http\Request;

Class HomeController Extends Controller{

	Public function _construct(){
		$this->middleware('auth');
	}
	public function index(){
		return view('home');
	}
}