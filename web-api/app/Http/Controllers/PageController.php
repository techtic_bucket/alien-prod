<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Banner;

class PageController extends Controller
{ 
    public function index($slug)
    {
        return redirect(getenv('Home_url').'#/page/'.$slug);
    }

    public function getSinglePage($slug){
        if($slug == 'undefined') $slug = 'home';
        $result  = [];
        $obj_menu = Page::where('slug',$slug)->with('Menu')->first();
        $obj_banner = $obj_menu->menu->first()->banner; 
        $result['page_banner'] = $this->splitbanner($obj_menu->menu->first()->banner);
        $result['page_content'] = $obj_menu;
        if(isset($result['page_banner']->image))
        {
            $image=$result['page_banner']->image;
            if(strpos($image, 'uploads') === false)
            {
                unset($result['page_banner']->image);
            }
         }
        return json_encode($result);
    }

    public function splitbanner($banner_content ='')
    {
        if($banner_content == '') return $banner_content;
        $banner_content = json_decode(trim($banner_content));

        if($banner_content->heading === NULL) return json_encode($banner_content);
        $heading_string = trim($banner_content->heading);
        $first_space = strpos($heading_string, ' ');
        $banner_content->heading  = [substr($heading_string, 0 , $first_space), trim(substr($heading_string, $first_space))];
        $banner_content->image    = get_uploaded_path($banner_content->image);
        return $banner_content;
    }
}