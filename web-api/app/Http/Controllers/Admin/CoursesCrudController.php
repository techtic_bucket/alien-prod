<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CoursesRequest as StoreRequest;
use App\Http\Requests\CoursesRequest as UpdateRequest;

class CoursesCrudController extends CrudController
{

    public function setUp()
    {
        $this->crud->setModel("App\Models\Courses");
        $this->crud->setRoute("admin/courses");
        $this->crud->setEntityNameStrings('Courses', 'Courses');

        $this->crud->setFromDb();
        
    }

	public function store(StoreRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}
}
