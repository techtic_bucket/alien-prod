<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;

class PageCrudController extends \Backpack\PageManager\app\Http\Controllers\Admin\PageCrudController
{
    public function __construct()
    {

        parent::__construct();

        /*$user=Auth::user();
        $user->hasRole('admin');*/
        $this->crud->setEntityNameStrings('Page', 'Pages');
        $this->crud->removeButton('open');
        $this->crud->removeColumn('template');
        $this->crud->removeField('template', 'update/create/both');


      //  $this->crud->addButtonFromModelFunction('line', 'open', 'setOpenButton', 'beginning');
    }
}