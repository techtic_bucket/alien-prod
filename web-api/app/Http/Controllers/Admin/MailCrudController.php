<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Request;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MailRequest as StoreRequest;
use App\Http\Requests\MailRequest as UpdateRequest;
use App\Models\Settings;

class MailCrudController extends CrudController
{

    public function setUp()
    {
       
        $this->crud->show_list = 0;
        $this->crud->setModel("App\Models\Settings");
        $this->crud->setRoute("admin/mail");
        $this->crud->setEntityNameStrings('mail', 'Mail Settings');

        $this->crud->setFromDb();
        $this->crud->denyAccess(['create', 'update', 'reorder', 'delete']);
    }

	public function store(StoreRequest $request)
	{
        $redirect_location = parent::storeCrud();
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
        /*$redirect_location = parent::updateCrud();
        return $redirect_location;*/
        $post=Request::all();
        unset($post['_token']);
        foreach ($post as $key => $value) {
            $available_val=Settings::where('name',$key)->first();
            if($available_val){
                $update=Settings::find($available_val->id);
                $update_info=array('field'=>$value);
                $update->update($update_info);
                \Alert::success(trans('backpack::crud.update_success'))->flash();
            }else{
                $update_info=array('key'=>'mail','name'=>$key,'active'=>'1','field'=>$value);
                $update=Settings::insert($update_info);
                \Alert::success(trans('backpack::crud.insert_success'))->flash();
            }
        }
        return \Redirect::intended('admin/mail');
	}
}
