<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
/*use App\Http\Requests\StreamRequest as StoreRequest;
use App\Http\Requests\StreamRequest as UpdateRequest;*/
use App\Http\Requests\CoursesRequest as StoreRequest;
use App\Http\Requests\CoursesRequest as UpdateRequest;
use App\Models\Stream;

class StreamCrudController extends CrudController
{

    public function setUp()
    {
        $this->crud->setModel("App\Models\Stream");
        $this->crud->setRoute("admin/stream");
        $this->crud->setEntityNameStrings('Streams', 'Streams');

        $this->crud->setFromDb();
    }

	public function store(StoreRequest $request)
	{
        $redirect_location = parent::storeCrud();
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
        $redirect_location = parent::updateCrud();
        return $redirect_location;
	}
}
