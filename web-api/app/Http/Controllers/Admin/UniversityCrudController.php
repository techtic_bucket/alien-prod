<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Request;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UniversityRequest as StoreRequest;
use App\Http\Requests\UniversityRequest as UpdateRequest;
use App\Models\Component_filter,App\Models\UniversityMeta,App\Models\University;
use App\Http\Controllers\Admin\DB;

use App\Models\UniversityCourse;

class UniversityCrudController extends CrudController {

	public function setUp() {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\University");
        $this->crud->setRoute("admin/university");
        $this->crud->setEntityNameStrings('University', 'Universities');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		$this->crud->setFromDb();

        $this->crud->removeColumns(['slug','image','campus','description','cover_image','map_embed_link','apply_now_link']);
        $this->crud->removeFields(['slug','map_embed_link','apply_now_link'], 'update/create/both');
        $component_filter=Component_filter::where('status','active')->get();
        $val=array();
        $field_val=array(
            [
                'label' => "Image",
                'name' => "image",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 0,
            ],[
                'label' => "Cover Image",
                'name' => "cover_image",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 0,
            ],[
                'name' => 'map_embed_link',
                'label' => "Map Embed Link",
                'type' => 'text',
            
            ],[
                'name' => 'apply_now_link',
                'label' => "Apply Now Link",
                'type' => 'text',
            
            ],[
                'name'=>'meta_data',
                'type' => 'view',
                'view' => 'partial/university_courses'
            ]
        );

      
        $this->crud->addFields($field_val,'update/create/both');
        $this->crud->addColumn([
                                'label' => "Rating", // Table column heading
                               'type' => "model_function",
                               'function_name' => 'getrating', // the method in your Model
                            ]);
        
    }

	public function store(StoreRequest $request)
	{
        
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        $post=Request::all();

      
        $val=$this->crud->entry;
        $id=$val->id;
        if(isset($post['metadata']) && (is_array($post['metadata']) || is_object($post['metadata']))){
            foreach ($post['metadata'] as $key => $value) {
                $update_val=array();
                foreach ($value as $k => $v) {
                
                
                    $update_val=array('value'=>$v,'key'=>$key,'meta_key'=>$k,'university_id'=>$id);
                    UniversityMeta::create($update_val);
               
                   
                }
            }
        }
            
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
        
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        $post=Request::all();
        
       /* $universitys=University::find($post['id']);
        foreach ($post['metadata'] as $key => $value) {
            $update_val=array();
            foreach ($value as $k => $v) {
                $update_val=array('value'=>$v,'key'=>$key,'meta_key'=>$k,'university_id'=>$id);
               
               UniversityMeta::create($update_val);
               
            }
        }
        */
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}


    function get_course_data($uid){
        $university = University::find($uid);

        return \Datatable::collection($university->courses)
             ->addColumn('Id',function($model){
                    return $model->id;
                })
             ->addColumn('Course Name',function($model){
                    return $model->course ? $model->course->name : "-" ;
                })
             ->addColumn('Stream Name',function($model){
                    return $model->stream ? $model->stream->name : "-" ;
                })
            ->addColumn('', function($model){
                    ob_start();
                    ?>
                        <table>
                            <?php foreach($model->meta as $meta){ 
                            ?>
                            <tr>
                                <th><?php 
                                    echo $meta->getMetaKeyName();
                                ?>
                                </th> 
                                <td><?php echo !empty($meta->meta_value) ? $meta->meta_value : "-" ?></td>
                            <tr>
                            <?php } ?>
                        </table>
                    <?php
                    return ob_get_clean();
                })
            ->addColumn('Action', function($model){
                    ob_start();
                    ?>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a herf="javascript:" data-id="<?php echo $model->id ?>" onClick="open_add_dialog(this, 'edit')" class="btn btn-xs btn-primary">Edit</a>
                        <a herf="javascript:" data-id="<?php echo $model->id ?>" onClick="delete_course(this)" class="btn btn-xs btn-danger">Delete</a>
                    </div>
                    <?php
                    return ob_get_clean();
                })
        //->showColumns('id', 'name')
        ->searchColumns('Id','Course Name', 'Stream Name', "","Action")
        ->orderColumns('Id','Course Name', 'Stream Name', "","Action")
        //->orderColumns('id','name')
        ->make();
    }


    function get_course(){
        $cid=\Request::input('cid');

        $courses = UniversityCourse::find($cid);
        $custom = [];
        $component = [];
        if($courses){
            foreach($courses->meta as $meta){
                if($meta->type == 'custom'){
                    
                    $custom[] = array(
                        'key' => $meta->meta_key,
                        'value' => $meta->meta_value
                        );
 
                }else{
                    $component[] = array(
                        'key' => $meta->meta_key,
                        'value' => $meta->meta_value
                        );
                }
            }
        }

        echo json_encode(['course' => $courses->course_id, 'stream' => $courses->stream_id, 'custom' => $custom, 'components' => $component]);
        exit();
        
    }

    public function delete_course(Request $request){
        $request = $request::all();
        $request = json_decode(json_encode($request));
        if(isset($request->cid) && !empty($request->cid)){
            UniversityCourse::find($request->cid)->delete();
            \Api::success("Course successfully deleted.");
        }else{
            \Api::error("Unable to delete courses. Please reload page and try again.");
        }
    }
    public function update_course(Request $request){
        //print_r($request::all());
        $request = $request::all();
        $request = json_decode(json_encode($request));
        $create = false;

        if(isset($request->cid) && !empty($request->cid)){
            $course = UniversityCourse::find($request->cid);
            $course->course_id = $request->course;
            $course->stream_id = $request->stream;
            $course->save();
        }else{
            $course = new UniversityCourse();
            $course->university_id = $request->uid;
            $course->course_id = $request->course;
            $course->stream_id = $request->stream;
            $course->save();
            $create = true;
        }
        if($course){
            foreach ($request->custom_component as $key => $value) {
                $find = [
                    'meta_key' => $key,
                    'type' => 'component',
                    'university_courses_id' => $course->id,
                ];
                $meta = $course->meta()->firstOrNew($find);
                $meta->meta_value = $value;
                $meta->save();
            }

            $course->meta()->where('type','custom')->delete();
            if(!empty($request->custom))
            {
                foreach ($request->custom as $value) {
                if(empty($value->title) && empty($value->value))
                {
                    continue;
                }
                $find = [
                    'meta_key' => $value->title,
                    'type' => 'custom',
                    'meta_value' => $value->value,
                    'university_courses_id' => $course->id
                ];
                $course->meta()->create($find);
            }
            }
            

            if($create)
                \Api::success("Course successfully created.");
            else
                \Api::success("Course successfully updated.");
        }else{
            if($create)
                \Api::error("Unable to add courses. Please reload page and try again.");
            else
                \Api::error("Unable to update courses. Please reload page and try again.");
        }


    }
}
