<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;

class ArticleCrudController extends \Backpack\NewsCRUD\app\Http\Controllers\Admin\ArticleCrudController
{
    public function __construct()
    {
        parent::__construct();
        $this->crud->setModel("App\Models\Article");
        $this->crud->setEntityNameStrings('Article', 'Articles');
        $this->crud->addFields(array([    // Image
                                'name' => 'image',
                                'label' => 'Image',
                                'type' => 'image',
				                'upload' => true,
				                'crop' => true,
				                'aspect_ratio' => 0,
                            ],['name' => 'date',
				                'type' => 'date_picker',
				                'label' => 'Date',
				               // optional:
				                'date_picker_options' => [
				                  'todayBtn' => true,
				                  'format' => 'dd-mm-yyyy',
				                  'language' => 'en',
				                   'startDate'=> 'd']]));

        /*$this->crud->setModel("\App\Models\Articles");
       // $this->crud->addButton($stack, , 'model_function', , );
        $this->crud->addButtonFromModelFunction('line','view', 'getlinkforcomment', 'beginning');*/

    }
}