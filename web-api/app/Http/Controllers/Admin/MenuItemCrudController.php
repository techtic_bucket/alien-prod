<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Requests\CrudRequest as StoreRequest;
use Backpack\CRUD\app\Http\Requests\CrudRequest as UpdateRequest;
use App\Models\Banner;
class MenuItemCrudController extends \Backpack\MenuCRUD\app\Http\Controllers\Admin\MenuItemCrudController
{
    public function __construct()
    {
        parent::__construct();

         $this->crud->setModel("App\Models\MenuItem");
         $this->crud->setEntityNameStrings('Menu Item', 'Menu Items');
         //$this->crud->removeButton('Delete');
         
        $this->crud->addField([
                                'label' => 'Category',
                                'type' => 'select',
                                'name' => 'category_id',
                                'entity' => 'category',
                                'attribute' => 'title',
                                'model' => "App\Models\MenuCategory",
                                'allows_null' => false
                            ]);
         $this->crud->removeColumn('parent_id');
          $this->crud->addColumn([
                                'label' => 'Category',
                                'type' => 'select',
                                'name' => 'category_id',
                                'entity' => 'category',
                                'attribute' => 'title',
                                'model' => "App\Models\MenuCategory",
                            ]);
        $this->crud->denyAccess(['reorder','delete']);
        $this->crud->addButtonFromModelFunction('line','Delete', 'getlinkfordelete','end');
    }
    public function delete($id)
    {
       
            $banner=Banner::where('menu_id',$id)->delete();
            /*$update_val=array('menu_id'=>null);
            if($banner->count()>0){
                foreach ($banner as $key => $value) {
                $banner_detail=Banner::find($value->id);
                $banner_detail->update($update_val);
                }
            }*/
            return $this->crud->delete($id);
       
    }
}