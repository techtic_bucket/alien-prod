<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BannerRequest as StoreRequest;
use App\Http\Requests\BannerRequest as UpdateRequest;
use Illuminate\Http\Request;

class MailController extends CrudController
{

    public function setUp()
    {
        $this->crud->show_list = 0;
        $this->crud->setModel("App\Models\Settings");
        $this->crud->setRoute("admin/banner");
        $this->crud->setEntityNameStrings('banner', 'banners');
        $menuitem = new MenuItem();
        $menu_dropdown = $menuitem->menuDDArray(['select' => ['id','name']]);
        
        $this->crud->setFromDb();
        $this->crud->addFields(array(
            [
                'label' => "Banner Image",
                'name' => "image",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 0,
            ],
            [   // select_from_array
                'name' => 'menu_id',
                'label' => "Select Menu",
                'type' => 'select_from_array',
                'options' => $menu_dropdown,
                'allows_null' => false
            ],
            [   // URL
                'name'  => 'link',
                'label' => 'Link',
                'type'  => 'text'
            ]

        ),'update/create/both');
        $this->crud->removeColumns(['menu_id','image','Menu']); // remove a column from the stack
        $this->crud->addColumn([
           'name' => 'image', // The db column name
           'label' => "Banner Image", // Table column heading
           'type' => 'image_link'
        ]);
      
    }

	public function store(StoreRequest $request)
	{
        $redirect_location = parent::storeCrud();
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
        $redirect_location = parent::updateCrud();
        return $redirect_location;
	}
}
