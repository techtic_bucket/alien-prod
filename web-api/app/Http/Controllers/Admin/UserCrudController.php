<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
//use App\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use Backpack\CRUD\CrudTrait; 
// <------------------------------- this one
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;



class UserCrudController extends CrudController
{
    use HasRoles;
    //use Authenticatable;
    public function setUp()
    {
       /* $user=Auth::user();
        $user->hasRole('admin');*/

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\User");
        $this->crud->setRoute("admin/user");
        $this->crud->setEntityNameStrings('User', 'Users');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setFromDb();
        $this->crud->removeColumns(['password','remember_token','jwt_token','reset_code','reset_date','facebook_id','google_id','image','address','country','state','zip_code','gender']);
        $this->crud->removeFields(['jwt_token','contact_number','address','country','state','zip_code','qualification','reset_code','reset_date','facebook_id','google_id','image','gender', 'dob'], 'update/create/both');
        $this->crud->addFields(array([   // Password
                        'name' => 'password_confirmation',
                        'label' => 'Confirm Password',
                        'type' => 'password',
                        
                    ],[   // select_from_array
                        'name' => 'gender',
                        'label' => "Gender",
                        'type' => 'radio',
                        'options' => array('male'=>'Male','female'=>'Female'),
                        'inline'      => true,
                    ],[
                        'name' => 'dob',
                        'type' => 'date_picker',
                        'label' => 'Date of Birth',
                       // optional:
                        'date_picker_options' => [
                          'todayBtn' => true,
                          'format' => 'dd-mm-yyyy',
                          'language' => 'en',
                           'endDate'=> 'd',
                    ]],[   // Password
                        'name' => 'contact_number',
                        'label' => 'Contact Number',
                        'type' => 'text'
                    ], 
                    [   // Password
                        'name' => 'address',
                        'label' => 'Address',
                        'type' => 'text'
                    ],
                    [   // Password
                        'name' => 'country',
                        'label' => 'Country',
                        'type' => 'text'
                    ],
                    [   // Password
                        'name' => 'state',
                        'label' => 'State',
                        'type' => 'text'
                    ],[   // Password
                        'name' => 'zip_code',
                        'label' => 'Zip Code',
                        'type' => 'text'
                    ],[   // Password
                        'name' => 'qualification',
                        'label' => 'Qualification',
                        'type' => 'text'
                    ],[    // Image
                                'name' => 'image',
                                'label' => 'Profile Picture',
                                'type' => 'image',
                                'upload' => true,
                                'crop' => true,
                                'aspect_ratio' => 0,
                    ],[ 
                           'label' => 'Roles',
                           'type' => 'checklist',
                           'name' => 'userrole',
                           'entity' => 'role',
                           'attribute' => 'name',
                           'model' => "Backpack\PermissionManager\app\Models\Role",
                            'pivot' => true, // <-------------------------------------- don't forget this line
                        ],
                        [ 
                           'label' => 'Permissions',
                           'type' => 'checklist',
                           'name' => 'userpermission',
                           'entity' => 'permission',
                           'attribute' => 'name',
                           'model' => "Backpack\PermissionManager\app\Models\Permission",
                            'pivot' => true, // <-------------------------------------- don't forget this line
                        ]),'update/create/both');
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

	/*public function store(StoreRequest $request)
	{
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6',
        ]);
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the login form
                ->withInput();

            $input = input::all();

        }
        $post=Request::all();

        $password = bcrypt($post['password']);
        $remember_token = $post['_token'];

        $request->request->set('password', $password);
        $request->request->set('remember_token', $remember_token);

		// your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email'=>'required|email',
            'password'=>'min:6',
        ]);
       
         if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator) // send back all errors to the login form
                ->withInput();

            $input = input::all();

        }
        $post=Request::all();
        $user_data=User::where('email',$post['email'])->where('id','!=',$post['id'])->get()->first();
        if($user_data){
            $validator=array('messages'=>'The email has already been taken.');
             return Redirect::back()
                ->withErrors($validator) // send back all errors to the login form
                ->withInput();

            $input = input::all();
        }
        if(empty($post['password'])){
            $users_data=User::find($post['id']);
            $password=$users_data->password;
        }else{
           $password = bcrypt($post['password']);
        }
        $request->request->set('password', $password);

		// your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}*/
    public function store(StoreRequest $request)
    {
        $this->crud->hasAccessOrFail('create');

        // insert item in the db
        if ($request->input('password')) {
            $item = $this->crud->create(\Request::except(['redirect_after_save']));

            // now bcrypt the password
            $item->password = bcrypt($request->input('password'));
            $item->save();
        } else {
            $item = $this->crud->create(\Request::except(['redirect_after_save', 'password']));
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // redirect the user where he chose to be redirected
        switch (\Request::input('redirect_after_save')) {
            case 'current_item_edit':
                return \Redirect::to($this->crud->route.'/'.$item->id.'/edit');

            default:
                return \Redirect::to(\Request::input('redirect_after_save'));
        }
    }
     public function update(UpdateRequest $request)
    {
       
        //encrypt password and set it to request
        $this->crud->hasAccessOrFail('update');

        $dataToUpdate = \Request::except(['redirect_after_save', 'password']);

        //encrypt password
        if ($request->input('password')) {
            $dataToUpdate['password'] = bcrypt($request->input('password'));
        }

        // update the row in the db
        $this->crud->update(\Request::get('id'), $dataToUpdate);

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return \Redirect::to($this->crud->route);
    }
}
