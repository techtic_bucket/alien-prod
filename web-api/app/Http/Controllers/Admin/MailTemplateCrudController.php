<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MailTemplateRequest as StoreRequest;
use App\Http\Requests\MailTemplateRequest as UpdateRequest;

class MailTemplateCrudController extends CrudController
{

    public function setUp()
    {
       // $this->crud->show_list = 0;
        $this->crud->setModel("App\Models\Settings");
        $this->crud->setRoute("admin/mail_template");
        $this->crud->setEntityNameStrings('Mail Template', 'Mail Templates');
        $this->crud->addClause('where', 'key', '=','mail_template');
        $this->crud->setFromDb();

        $this->crud->removeColumns(['key','name','description','value','active','field']);
        $this->crud->addColumn([
                   'name' => 'name', // The db column name
                   'label' => "Title"
                ]);
        $this->crud->addColumn([
                   'name' => 'description', // The db column name
                   'label' => "Subject"
                ]);
        $this->crud->addColumn([
                   'name' => 'field', // The db column name
                   'label' => "Content"
                ]);
         $this->crud->addColumn([
                    'label' => 'Status',
                    'type' => 'boolean',
                    'name' =>'active',
                    'options' => [1 => 'Active', 0 => 'Inactive']
                ]);

        $this->crud->removeFields(['key','name','description','value','active','field'],'update/create/both');
        $this->crud->addField([
                'name' => 'name', 
                'label' => 'Title', 
                'type' => 'text',
              ], 'update/create/both');
        $this->crud->addField([
                'name' => 'description', 
                'label' => 'Subject', 
                'type' => 'text',
              ], 'update/create/both');
        $this->crud->addField([
                'name' => 'field', 
                'label' => 'Content', 
                'type' => 'textarea', 
                'attributes'=>array('class'=>'faqckedit form-control'),
              ], 'update/create/both');
        $this->crud->addField([
                'name' => 'active',
                'label' => "Status",
                'type' => 'select_from_array',
                'options' => ["1" => "Active", "0" => "Inactive"],
                'allows_null' => false,
              ], 'update/create/both');
         $this->crud->addField([
                'name' => 'key',
                'type' => 'hidden',
                'value'=>'mail_template',
              ], 'update/create/both');
        //$this->crud->denyAccess(['create', 'update', 'reorder', 'delete']);
    }

	public function store(StoreRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}
}
