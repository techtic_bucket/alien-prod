<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MenuCategoryRequest as StoreRequest;
use App\Http\Requests\MenuCategoryRequest as UpdateRequest;
use App\Models\MenuItem;
use Illuminate\Http\Request;
class MenuCategoryCrudController extends CrudController
{

    public function setUp()
    {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Models\MenuCategory");
        $this->crud->setRoute("admin/menu-category");
        $this->crud->setEntityNameStrings('Menu Category', 'Menu Categories');

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setFromDb();
        
        // run a function on the CRUD model and show its return value
        $this->crud->addColumn([
                   'label' => "Set Order", // Table column heading
                   'type' => "model_function",
                   'function_name' => 'order_menu', // the method in your Model
                ]);
    }

	public function store(StoreRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}

	public function update(UpdateRequest $request)
	{
		// your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
	}
    public function set_order($id)
    {
        $menu=MenuItem::where('category_id',$id)->orderBy('lft','asc')->get();
        $this->data['cat_id']=$id;
        $this->data['entries'] = $menu;
        $this->data['crud'] = $this->crud;
        //$this->data['title'] = trans('backpack::crud.reorder').' '.$this->crud->name;
        $this->data['menu_list']=$menu;
        return view('menu/reorder', $this->data);
    }
    public function saveReorderMenu($id)
    {
    

       $all_entries = \Request::input('tree');
      
    
        if (count($all_entries)) {
            
            $count=count($all_entries);
            unset($all_entries[0]);
            foreach ($all_entries as $key => $value) {
               $id=$value['item_id'];
               $menu_item='';
               $update_val=array('parent_id'=>(empty($value['parent_id'])) ? null: $value['parent_id'] ,
                                 'lft'=>(empty($value['left'])) ? null: $value['left'],
                                 'rgt'=>(empty($value['right'])) ? null: $value['right'],
                                 'depth'=>(empty($value['depth'])) ? null: $value['depth']);

               $menu_item=MenuItem::find($id);
               $menu_item->update($update_val);
            }
        } else {
            return false;
        }

        return 'success for '.$count.' items';
    }
}
