<?php

use \App\Models\Footer;
use \App\Models\Settings;

function get_constant_value($key_value ='', $default=''){
	if($key_value=='') return '';
    if(Footer::select('meta_value')->where('meta_key', $key_value)->count()==0) return '';
	return Footer::select('meta_value')->where('meta_key', $key_value)->first()->meta_value;
}

function update_constant_value($key ='', $value=''){
	if($key=='') return '';	
    $constant = Footer::where('meta_key', $key)->first();
    $constant->meta_value = $value;
    return $constant->save();
}
function get_option($key){
  if($key=='') return '';
  $constant = Settings::where('name', $key)->first();
  if($constant){
    return $constant->field;
  }else{
    return null;
  }
}
function check_and_return_image_upload($rawfile, $default){
    if (starts_with($rawfile, 'data:image'))
    {
        return uplaod_raw_image($rawfile);
    } else return $default;
}

function get_uploaded_path($value){
    return str_replace('/public/index.php','',URL::to($value));
}

function get_storage_link($value = '', $type=0){

    if($type = 1) $value = 'storage/app/public/'.str_replace('storage/app/public/', '', $value);
    return str_replace('/public/index.php','',URL::to($value));
}

function get_storage_link_api($value, $type=0){
	if($type = 1) $value = 'storage/app/public/'.$value;
	$value = str_replace('/public/index.php','',URL::to($value));
    return str_replace('storage/app/public//storage/app/public','storage/app/public',$value);
}

function uplaod_raw_image($value, $destination_path = '', $disk='')
{ 
    $attribute_name = "image";
    if($disk == '') $disk = "public";
    if($destination_path == '') $destination_path =  'uploads';

    // if the image was erased
    if ($value==null) {
        // delete the image from disk
        // \Storage::disk($disk)->delete($this->image);

        // set null in the database column
        return  null;
    }

    // if a base64 was sent, store it in the db
    if (starts_with($value, 'data:image'))
    {
        $pos  = strpos($value, ';');
        $type  = explode(':', substr($value, 0, $pos))[1];
        $type  = str_replace("image/", "", $type);
        // 0. Make the image
        $image = \Image::make($value);
        // 1. Generate a filename.
        $filename = md5($value.time()).'.'.$type;
        // 2. Store the image on disk.
        $destination_path.'/'.$filename;
        \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
        // 3. Save the path to the database
        $attribute_name = $destination_path.'/'.$filename;
    }
    return "storage/app/public/".$attribute_name;
}


function maybe_unserialize( $original ) {
      if (is_serialized( $original ) ) // don't attempt to unserialize data that wasn't serialized going in
        return @unserialize( $original );
      return $original;
    }

    function maybe_serialize( $data ) {
      if ( is_array( $data ) || is_object( $data ) )
          return serialize( $data );
      if (is_serialized($data))
          return serialize( $data );
      return $data;
    }

    function is_serialized($value, &$result = null)
    {
    // Bit of a give away this one
        if (!is_string($value))
        {
          return false;
        }
        // Serialized false, return true. unserialize() returns false on an
        // invalid string or it could return false if the string is serialized
        // false, eliminate that possibility.
        if ($value === 'b:0;')
        {
          $result = false;
          return true;
        }
        $length = strlen($value);
        $end  = '';
        switch ($value[0])
        {
          case 's':
            if ($value[$length - 2] !== '"')
            {
              return false;
            }
          case 'b':
          case 'i':
          case 'd':
            // This looks odd but it is quicker than isset()ing
            $end .= ';';
          case 'a':
          case 'O':
            $end .= '}';
            if ($value[1] !== ':')
            {
              return false;
            }
            switch ($value[2])
            {
              case 0:
              case 1:
              case 2:
              case 3:
              case 4:
              case 5:
              case 6:
              case 7:
              case 8:
              case 9:
              break;
              default:
                return false;
            }
          case 'N':
            $end .= ';';
            if ($value[$length - 1] !== $end[0])
            {
              return false;
            }
          break;
          default:
            return false;
        }
        if (($result = @unserialize($value)) === false)
        {
          $result = null;
          return false;
        }
        return true;
    }
    function getSql(\Illuminate\Database\Eloquent\Builder $model){ 
    function replace ($sql, $bindings) 
    { 
        $needle = '?'; 
        foreach ($bindings as $replace){ 
            $pos = strpos($sql, $needle); 
            if ($pos !== false) { 
                $sql = substr_replace($sql, $replace, $pos, strlen($needle)); 
            } 
        } 
        return $sql; 
    } 
    $sql = replace($model->toSql(), $model->getBindings()); 
    
    return $sql; 
}

//mail function

function get_mail_template($args=""){
  
  if(empty($args))
    return false;

  $results=Settings::find($args);
  if(!empty($results)){
        return json_decode(json_encode($results));
    }else{
      return false;
    }
}
function send_mail($to, $mail_template_id, $data=array(), $from = "", $debug = false){

  
  if(is_array($from)){
    $from_email = $from['email'];
    $from_name = $from['name'];
  }else{
    if(strpos($from, '@') !== false){
      $from_email = $from;
      $from_name = get_option('from_name');
    }else{
      $from_email = get_option('from_email');
      $from_name = $from != "" ? $from : get_option('from_name');
    }
  }

  $default_data = array(
    'header' => filter_content(get_option('mail_header'),$data, 'both', true),
    'footer' => filter_content(get_option('mail_footer'),$data, 'both', true),
  );

  $data = array_merge($default_data, $data);

  if(is_numeric($mail_template_id)){
    $template = get_mail_template($mail_template_id);


    $subject = filter_content($template->description, $data, 'both', true);
    $message = filter_content($template->field, $data, 'both', true);
  }else{
    $subject = "Test";
    $message = $mail_template_id;
  }

  /*$CI->email->from($from_email,$from_name);
  $CI->email->to($to);
  $CI->email->subject($subject);
  $CI->email->message($message);  
  $CI->email->set_mailtype("html");

  if($debug){
    echo "<pre>"; 
    echo "<br> subject = $subject";
    echo "<br> to = ";print_r($to);
    echo "<br> from =";print_r(array($from_email,$from_name));
    echo "<br> message = $message";
    echo "<br> mail_template_id = $mail_template_id";
    echo "</pre>";
    echo "<pre>"; echo "<br> template =";  print_r($template);echo "</pre>";
    echo "<pre>"; echo "<br> data =";  print_r($data);echo "</pre>";
  }

  return $CI->email->send();*/
  $to=$to;
  $subject=$subject;
  $message=$message;
  $reply_to  = $from_email;
  $from_mail = "".$from_name." <".$from_email.">";
  
  $header = "MIME-Version: 1.0\r\n";
  $header .= "Content-type: text/html; charset=iso-8859-2\r\nContent-Transfer-Encoding: 8bit\r\nX-Priority: 1\r\nX-MSMail-Priority: High\r\n";
  $header .= "From: ".$from_mail."\r\n" . "Reply-To: ".$reply_to."\r\n";
  if (preg_match('/gmail/',$from_mail)){
      $header  = str_replace("\r\n","\n",$header);
  }

  return mail($to,$subject,$message,$header);
}
function filter_content($message, $data, $var_prefix = false, $sub = false){
    

    if(!is_array($data)){
      if(empty($data))
        $data = array();
      else
        $data = (array)$data;
    }

    return filter_msg($message,$data, $var_prefix, $sub);
  }
  function filter_msg($msg,$values, $var_prefix = false, $sub = false){
    $values = (array)$values;
    
    if($var_prefix === true){
      preg_match_all("/\{\\$([a-zA-Z_\x7f-\xff][a-z-A-Z0-9_\x7f-\xff]*)\}/",$msg,$matches);
    }else{
      preg_match_all("/\{([a-z\$A-Z_\x7f-\xff][a-z-A-Z0-9_\x7f-\xff]*)\}/",$msg,$matches);
      //preg_match_all("/\{([a-zA-Z_\x7f-\xff][a-z-A-Z0-9_\x7f-\xff]*)\}/",$msg,$matches);
    }
    foreach ($matches[0] as $key => $var_name) {
      $k = $matches[1][$key];
      $k = trim($k, '$');
      if($sub != false && !isset($values[$k])){
          $sub = $sub === true ? "-" : $sub;
          $sub_var_args = explode($sub, $k);

          $val = $values;
          foreach ($sub_var_args as $value) {
            if(is_object($val)){
              $val = (array)$val;
            }
            if(isset($val[$value])){
              $val = $val[$value];
            }else{
              $val = "";
            }
          }

        }else{
          $val = $values[$k];
        }


        $msg = str_replace($var_name, $val, $msg);
    }
    return $msg;
  }
function generate_random_password()
{
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $password = substr( str_shuffle( $chars ), 0, 12 );
    return $password;
}


//end mail function
