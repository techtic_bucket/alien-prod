@extends('backpack::layout')

@section('after_styles')
    <link href="{{ asset('vendor/backpack/nestedSortable/nestedSortable.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('header')
  <section class="content-header">
    <h1>
      <span class="text-capitalize">{{ $crud->entity_name_plural }}</span>
      <small>{{ trans('backpack::crud.all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span> {{ trans('backpack::crud.in_the_database') }}.</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
      <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
      <li class="active">{{ trans('backpack::crud.reorder') }}</li>
    </ol>
  </section>
@endsection

@section('content')
<?php
  function tree_element($entry, $key, $all_entries, $crud)
  {
    if (!isset($entry->tree_element_shown)) {
      // mark the element as shown
      $all_entries[$key]->tree_element_shown = true;
      $entry->tree_element_shown = true;

      // show the tree element
      echo '<li id="list_'.$entry->getKey().'">';
      echo '<div><span class="disclose"><span></span></span>'.$entry->name.'</div>';

      // see if this element has any children
      $children = [];
      foreach ($all_entries as $key => $subentry) {
        if ($subentry->parent_id == $entry->getKey()) {
          $children[] = $subentry;
        }
      }

      $children = collect($children)->sortBy('lft');

      // if it does have children, show them
      if (count($children)) {
        echo '<ol>';
        foreach ($children as $key => $child) {
          $children[$key] = tree_element($child, $child->getKey(), $all_entries, $crud);
        }
        echo '</ol>';
      }
      echo '</li>';
    }

    return $entry;
  }

 ?>
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    @if ($crud->hasAccess('list'))
      <a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
    @endif

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ trans('backpack::crud.reorder').' '.$crud->entity_name_plural }}</h3>
        </div>
        <div class="box-body">

          <p>{{ trans('backpack::crud.reorder_text') }}</p>
           <ol class="sortable">
            <?php
              $all_entries = collect($menu_list->all())->sortBy('lft')->keyBy($crud->getModel()->getKeyName());
              $root_entries = $all_entries->filter(function($item) {
                return $item->parent_id == 0;
              });
             ?>
            @foreach ($root_entries as $key => $entry)
              <?php
                $root_entries[$key] = tree_element($entry, $key, $all_entries, $crud);
              ?>
            @endforeach
          </ol>
          <input type="hidden" name="cat_id" id='cat_id' value='{{$cat_id}}'>
          {{ csrf_field() }}
          <button id="toArray" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('backpack::crud.save') }}</span></button>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
  </div>
</div>
@endsection

@section('after_scripts')
  <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
  <script src="http://45.79.111.106/alien/web-api/public/vendor/backpack/nestedSortable/jquery.mjs.nestedSortable2.js" type="text/javascript"></script>

  <script type="text/javascript">
    jQuery(document).ready(function($) {

      // initialize the nested sortable plugin
      $('.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
           // toleranceElement: '> div',
            maxLevels: {{ $crud->reorder_max_level or 3 }},

            isTree: false,
            expandOnHover: 700,
            startCollapsed: false
        });

      $('.disclose').on('click', function() {
        $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
      });

      $('#toArray').click(function(e){
        // get the current tree order
        arraied = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
        var token = $('input[name="_token"]').val();
        console.log(token);
        // log it
        console.log(arraied);
        var cat_id=$('#cat_id').val();
        // send it with POST
          $.ajax({
            url: '{{ url('admin/menu-category/saveReorderMenu') }}'+'/'+cat_id,
            type: 'post',
            data: { tree: arraied,token:token },
          })
          .done(function() {
            console.log("success");
            new PNotify({
                        title: "{{ trans('backpack::crud.reorder_success_title') }}",
                        text: "{{ trans('backpack::crud.reorder_success_message') }}",
                        type: "success"
                    });
          })
          .fail(function() {
            console.log("error");
            new PNotify({
                        title: "{{ trans('backpack::crud.reorder_error_title') }}",
                        text: "{{ trans('backpack::crud.reorder_error_message') }}",
                        type: "danger"
                    });
          })
          .always(function() {
            console.log("complete");
          });

      });

      $.ajaxPrefilter(function(options, originalOptions, xhr) {
          var token = $('meta[name="csrf_token"]').attr('content');

          if (token) {
                return xhr.setRequestHeader('X-XSRF-TOKEN', token);
          }
      });

    });
  </script>
@endsection