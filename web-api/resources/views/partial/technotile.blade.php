@extends('base')

@section('content')

    <?php
      // print_r($all->toArray());
      $master_data = $all->toArray(); 
      $total_count = $all->count();
      $parts = array_chunk($master_data, 3);
      $current_column = 1;
    ?> 
            @foreach($all as $key=>$value)
              
              @if($key == 0)  <div class="tech_col4"> @endif
              @if($current_column != $value->column_no) </div><div class="tech_col4"> @endif
                <?php $current_column = $value->column_no; list($width, $height) = getimagesize(str_replace('public/index.php','',URL::to('/')).$value['image']); ?>
                <div class="{{ ($value['type'] == 'S') ? 'row' : 'tech_big_img' }}">
                  <div class="flip-container">
                    <div class="flipper">
                      <div class="front {{ ($value['type'] == 'S') ? 'small_thumb' : '' }} {{ ($value['ori'] == 'R') ? 'right' : '' }}">
                        <img src="{{ get_uploaded_path(URL::to('/').$value['image'])}}" alt="">
                      </div>
                      <div class="back {{ ($value['type'] == 'S') ? 'small_thumb' : '' }} {{ ($value['ori'] == 'R') ? 'right' : '' }}">
                        <h2><a href="{{ $value['link'] }}" >{{ ($value['type'] == 'S') ? '' : $value['heading']}}<span>{{ $value['description']}}</span></a></h2>
                      </div>
                    </div>
                  </div>
                </div>
              @if($total_count == ($key+1)) </div> @endif
            @endforeach

  <ul class="technoslide_resp" style="display: none;" >
    @foreach($all as $key=>$value)
      @php if($value['type'] == 'S') continue; @endphp
      <li>
        <img src="{{ get_uploaded_path(URL::to('/').$value['image'])}}" alt="">
        <a href="{{ $value['link'] }}" ><h4> {{$value['heading'] }}</h4></a>
        <p>{{ $value['description']}}</p>
      </li>
    @endforeach
  </ul>
@stop
