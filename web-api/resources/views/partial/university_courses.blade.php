@if(isset($entry) && !empty($entry))
    <hr>
    <a herf="javascript:" onClick="open_add_dialog(this)" class="btn btn-primary">Add Course</a><br><br>
    <div class='row'>
        <div class='col-sm-12'>
            <?php
            $table = Datatable::table()
                ->setUrl(route('university.course.data', $entry->id))
                ->addColumn('Id','Course Name', 'Stream Name', "", 'Action')
                ->setOptions([
                    'responsive' => true,
                    'bProcessing' => true,
                    'language' => [
                         'processing' => '<i class="fa fa-spin fa-circle-o-notch" aria-hidden="true"></i> loading...'
                        ],
                    'columnDefs' => [
                            [
                                'responsivePriority' => 1, 
                                'targets' => 0 
                            ],
                            [
                                'responsivePriority' => 2, 
                                'targets' => 2 
                            ],
                            [ 
                                'responsivePriority' => 3, 
                                'targets' => -1 
                            ]
                        ]
                    ])
                ->noScript();

            ?>
            {!! $table->render() !!}

        </div>
    </div>


    <div id="add-course-form" style="display:none">
        <div class="row">
            <form>
                <div class="form-group col-sm-12">
                    <label>Courses</label>
                    <select class="form-control" name="course">
                        @foreach(App\Models\Courses::get() as $course)
                            <option value="{{$course->id}}">{{$course->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-12">
                    <label>Streams</label>
                    <select class="form-control" name="stream">
                        @foreach(App\Models\Stream::get() as $stream)
                            <option value="{{$stream->id}}">{{$stream->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <label>Duration :</label>
                    <input type="text" name='custom_component[duration]' class="form-control" value="">
                </div>
                

                <div class="form-group col-sm-12">
                    <label>Intakes :</label>
                    <input type="text" name='custom_component[intakes]' class="form-control" value="">
                </div>
                
                <div class="form-group col-sm-12"> 
                    <label>Fees in AUD $ :</label>
                    <input type="text" name='custom_component[fees]' class="form-control" value="">
                </div>
                


                <h4 class="col-sm-12">Education</h4>
                <div class="form-group col-sm-12">
                    <label>Last Education</label>
                    <input type="text" name='custom_component[last_education]' class="form-control" value="">
                </div>
                <div class="form-group col-sm-12">
                    <label>Last Education percentage</label>
                    <input type="text" name='custom_component[last_education_percentage]' class="form-control" value="">
                </div>

                <h4 class="col-sm-12">Marks</h4>

                <div class="form-group col-sm-12">
                    <label>Entry Criteria Description:</label>
                    <textarea name='custom_component[entry_criteria]' class="form-control"  value="" required></textarea>
                </div>

                <div class="form-group col-sm-12">
                    <label>IETLS</label>
                    <input type="text" name='custom_component[marks_ietls]' class="form-control" value="">
                </div>
                <div class="form-group col-sm-12">
                    <label>TOEFL</label>
                    <input type="text" name='custom_component[marks_toefl]' class="form-control" value="">
                </div>
                <div class="form-group col-sm-12">
                    <label>PTE</label>
                    <input type="text" name='custom_component[marks_pte]' class="form-control" value="">
                </div>
                <div class="form-group col-sm-12">
                    <label>SAT</label>
                    <input type="text" name='custom_component[marks_sat]' class="form-control" value="">
                </div>
                <div class="form-group col-sm-12">
                    <label>GRE</label>
                    <input type="text" name='custom_component[marks_gre]' class="form-control" value="">
                </div>

                <div class="col-sm-12">
                    <h4>Custom Fields</h4>
                    <div class="custom-fields-contianer">
                        <div class="form-group custom-field">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label>Title</label>
                                    <input type="text" name='component_title' class="form-control" value="" required>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Value</label>
                                    <input type="text" name='component_value' class="form-control" value="" required>
                                </div>
                                <div class="form-group col-sm-12">
                                    <a herf="javascript:" onclick="$(this).closest('.custom-field').remove()" class="btn btn-xs right btn-danger">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <a herf="javascript:" class="btn add-custome-field right btn-success">Add</a>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        var oTable;
    </script>
    {!! $table->script() !!}
    <script type="text/javascript">
        var field_html = $('#add-course-form').find('.custom-fields-contianer').html();
        
        $('#add-course-form').find('.custom-fields-contianer').html("");
        var add_from = $('#add-course-form').html();
        $('#add-course-form').remove();

        var field_key = 0;



        function add_custom_field(ele, data){
            var contaner = $(ele).find('.custom-fields-contianer');
            
            

            $.each(data, function(i,value){
                var field = $(field_html);
                field.find("input[name='component_title']").val(value.key);
                field.find("input[name='component_value']").val(value.value);
                field.find("input[name='component_title']").attr('name', "custom["+field_key+"][title]");
                field.find("input[name='component_value']").attr('name',"custom["+field_key+"][value]");
                contaner.append(field);
                field_key++;
            });
            
        }


        function delete_course(ele) {
            var cid = $(ele).data('id');
             $.ajax({
                url : '{{ Route("university.course.delete") }}',
                type : 'post',
                dataType: 'json',
                data : {cid : cid},
                beforeSend: function(request) {
                    return request.setRequestHeader('X-CSRF-Token', '{{ csrf_token() }}');
                },
                success : function(data){
                    oTable.fnDraw();
                }
            })
        }
        function open_add_dialog(ele, type) {
            var dialog, cid = $(ele).data('id');
            if(type == undefined){
                type = "add";
            }
            if(type == "add"){
                title = "Add Course";
                html = add_from;
            }else{
                title = "Edit Course";
                html = '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>'

                $.ajax({
                    url : '{{ Route("university.course.get") }}',
                    type : 'post',
                    dataType: 'json',
                    data : {
                        cid : cid,
                    },
                    beforeSend: function(request) {
                        return request.setRequestHeader('X-CSRF-Token', '{{ csrf_token() }}');
                    },
                    success : function(data){
                        var form_data = $(add_from);
                        $.each(data.components, function(i,component){
                             form_data.find("input[name='custom_component["+component.key+"]']").val(component.value)
                             form_data.find("textarea[name='custom_component["+component.key+"]']").val(component.value)

                        });

                        if(data.course != undefined){
                            console.log(form_data.find("[name='course']"));
                            form_data.find("[name='course']").val(data.course)
                        }
                        if(data.stream != undefined){
                            console.log(form_data.find("[name='stream']"));
                            form_data.find("[name='stream']").val(data.stream)
                        }

                        var custom_html = "";
                        $(form_data).find('.custom-fields-contianer').html("");
                        add_custom_field(form_data, data.custom);

                        form_data.find('.add-custome-field').on('click', function(){
                            add_custom_field(form_data)
                        });

                        dialog.find('.bootbox-body').html("").append(form_data[0]);
                    }
                })
            }
            dialog = bootbox.dialog({
                title: title,
                message: html,
                buttons : {
                    cancel : {
                        label : "Cancel",
                    },
                    submit : {
                        label : "Submit",
                        callback: function (result) {
                            if(cid == undefined)
                                cid = "";
                            thtml = dialog.find('.bootbox-body')
                                .append("<input type='hidden' name='_method' value='POST'>")
                                .append("<input type='hidden' name='_token' value='{{ csrf_token() }}'>")
                                .append("<input type='hidden' name='cid' value='"+cid+"'>")
                                .append("<input type='hidden' name='uid' value='{{$entry->id}}'>");

                            from_data = dialog.find('.bootbox-body :input').serialize();
                            $.ajax({
                                url : '{{ Route("university.course.update") }}',
                                type : 'post',
                                dataType : 'json',
                                beforeSend: function(request) {
                                    return request.setRequestHeader('X-CSRF-Token', '{{ csrf_token() }}');
                                },
                                data : from_data,
                                success : function(data){
                                    oTable.fnDraw();
                                    /*if(data.status == 'success'){
                                        
                                    }else{

                                    }*/
                                }
                            });
                        }
                    }
                }
            });
            if(type == "add"){
                add_custom_field(dialog.find('.bootbox-body'));
                dialog.find('.bootbox-body').find('.add-custome-field').on('click', function(){
                    add_custom_field(dialog.find('.bootbox-body'))
                });
            }
        }
    </script>
@endif