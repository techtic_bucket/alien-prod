<?php
$component_filter=App\Models\Component_filter::where('status','active')->get();
$stream_data=App\Models\Courses::with('courses_name')->Where('parent_id', '!=', '')->get();

?>
<div class='row'>
                        <div class='ref-add-one'>
                       
                        <?php
                        	$key=1;
                         for($key=1;$key<=3;$key++) { ?>
                        <div class='ref-add'>
                            <div class='col-sm-12'>Component #<?php echo $key; ?>:</div>
                            <?php foreach ($component_filter as $k => $v) { ?>
                                <div class="form-group col-sm-6">
                                    <label><?php echo $v->name.' :'; ?></label>
                                    <?php $name_stream=strtolower($v->name); ?>
                                    @if($name_stream=='stream')
                                        <select name='metadata[{{$key}}][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control">
                                            @foreach($stream_data as $val)
                                               <option value='{{$val->id}}'>{{$val->name}}({{$val->courses_name->name}})</option>
                                            @endforeach
                                        </select>
                                    @elseif($name_stream=='eligibility criteria')
                                        <textarea name='metadata[{{$key}}][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control" ></textarea>
                                    @else
                                        <input type="text" name='metadata[{{$key}}][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control" value="" required>
                                    @endif
                                </div>
                            <?php } ?>
                            
                            <div class='col-sm-12 m-t-sm'>
                                <a herf="javascript:" class="remove_note btn btn-xs right btn-danger">Remove</a>
                            </div>
                            <div class='col-sm-12'><hr></div>
                       	</div>
                        <?php } ?>
                    </div>
                            <div id='ref-add'>
                                <div class='ref-add'>
                                    <div class='col-sm-12'>Componanent #$key:</div>
                                    <?php foreach ($component_filter as $k => $v) { ?>
                                        <div class="form-group col-sm-6">
                                            <label><?php echo $v->name.' :'; ?></label>
                                            <?php echo $name_stream=strtolower($v->name); ?>
                                            @if($name_stream=='stream')
                                                <select name='metadata[{{$key}}][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control">
                                                   
                                                </select>
                                            @elseif($name_stream=='eligibility criteria')
                                                <textarea name='metadata[$key][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control" ></textarea>
                                            @else
                                                <input type="text" name='metadata[{{$key}}][{{strtolower($v->name)}}]' id='{{strtolower($v->name)}}' class="form-control" value="" required>
                                    @endif
                                        </div>
                                    <?php } ?>
                                    <div class='col-sm-12 m-t-sm'>
                                        <a herf="javascript:" class="remove_note btn btn-xs right btn-danger">Remove</a>
                                    </div>
                                    <div class='col-sm-12'><hr></div>
                                </div>
                            </div>
                             @push('custom_after_scripts')

                                    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js" type="text/javascript"></script>
                                    <script type="text/javascript">
                                          //$ = jQuery;
                                        var note_container = document.getElementById('ref-add').innerHTML;
                                        document.getElementById('ref-add').innerHTML = ""; 
                                        $(document).ready(function(){
                                            var key = <?php echo $key > 0 ? $key : 0; ?>;
                                            $(document).on('click','.remove_note',function(){
                                                var numItems = $('.remove_note').length;
                                                
                                                    $(this).parents('.ref-add').remove();
                                               
                                                
                                            });
                                            $('.add_note').on('click',function(){
                                                var numItems = $('.remove_note').length;
                                                
                                               
                                                    html = replaceAll(note_container,'$key',key);
                                                    $('.ref-add-one').append(html);
                                                    key++;
                                            });
                                        });
                                        function replaceAll(string, find, replace) {
                                            return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
                                        }
                                        function escapeRegExp(string) {
                                            return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
                                        }
                                    </script>
                                     @endpush
                                <div class='clearfix'></div>
        
                                <div class="col-sm-10 add-note">
                                    <button class='add_note btn btn-success btn-xs' type='button'>Add</button>
                                </div>
                            </div> 
