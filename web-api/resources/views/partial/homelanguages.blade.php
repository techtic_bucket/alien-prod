@extends('base')

@section('content')

  @php
    $background = get_storage_link(get_constant_value('home_languages_bg'));
  @endphp
  <section class="start_project_block" style="background: #1f242b  url('<?php echo $background; ?>') no-repeat center center;">
    <div class="fixed-wrapper">
      <h3>{{ get_constant_value('home_languages_title_1') }} <span>{{ get_constant_value('home_languages_title_2') }}</span></h3>
      <div class="services">
        <ul>
          @foreach($all as $single_row)
            <li><a href="{{ $single_row->link }}" title="{{ $single_row->title }}"><img src="{{ get_uploaded_path(url($single_row->image)) }}" alt="{{ $single_row->title }}"></a></li>
          @endforeach
        </ul>
      </div>
    </div>    
  </section>
@stop
