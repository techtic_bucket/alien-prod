@extends('base')

@section('content')
  <section class="company_info_block">
    <div class="fixed-wrapper">
      <div class="team_left">
        @foreach($left as $content)
          <div class="row_content"> <i><img src="{{ get_uploaded_path($content->image)}}" alt=""></i>
            <h4>{{ $content->title }}</h4>
            <div class="row_content_col">
              <h5><font class="count">{{ $content->count}} </font>
              @if(($content->count)>1000000) + @endif</h5>
            </div>
          </div>
        @endforeach
      </div>
    @php $right_upper = unserialize($right_upper); $count=0;  @endphp
    
    @if(!empty($right_upper))
      <div class="concept_right">
       
              
         
            <div class="concept_block ref"> <img style="max-width: 190px; max-height: 300px;" src="{{ $right_upper[0]['image_name']}}" alt="">
             <h4 >{{ $right_upper[0]['title'] }}</h4>
             <p>{{ $right_upper[0]['description'] }}</p>
            </div>
         
            <div class="concept_block">
              <ul>
               @php unset($right_upper[0]);
                foreach ($right_upper as $key => $value) { @endphp
                <li class="trigger_h4" >
                  <a href="javascript:void(0)">
                    <h4>{{ $value['title']}}</h4>
                  </a>
                  <p>{{ $value['description'] }}</p>
                  <img src="{{ $value['image_name']}}" alt="">
                </li>  
              @php  } @endphp         
              </ul>
            </div>
       
      </div>
    @endif
    </div>
  </section>
@stop
<style>
  .trigger_h4 p {display: none;}
  .trigger_h4 img {display: none;}
</style>
<script>
jQuery(document).ready(function () {

  $(document.body).on('click','.trigger_h4',function(){
    $c = $(this);
    $r = $('.ref');
    $newval = $c.find('h4').html();
    $oldval = $r.find('h4').html();
    $newval1 = $c.find('p').html();
    $oldval1 = $r.find('p').html();
    $newval2 = $c.find('img').attr('src');
    $oldval2 = $r.find('img').attr('src');

    $c.find('h4').fadeOut(300,function(){
      $(this).html($oldval).fadeIn(300);
    }) 
    
    $r.find('h4').fadeOut(300,function(){
      $(this).html($newval).fadeIn(300);
    });


    $c.find('p').fadeOut(300,function(){
      $(this).html($oldval1);//.fadeIn(300);
    }) 
    
    $r.find('p').fadeOut(300,function(){
      $(this).html($newval1).fadeIn(300);
    });

    $c.find('img').fadeOut(300,function(){
      $(this).attr('src', $oldval2);//.fadeIn(300);
    }) 

    $r.find('img').fadeOut(300,function(){
      $(this).attr('src', $newval2).fadeIn(300);
    }); 
  });
});

</script> 