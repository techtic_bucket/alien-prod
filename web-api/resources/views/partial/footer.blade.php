@extends('base')

@section('content')

  @php $footer_bar = json_decode(get_constant_value('home_footer_bar')); @endphp
  <section class="nda_block">
    @foreach($footer_bar as $foot)
      <div class="col3_block">
        <h4><a href="{{$foot->link}}">{{ $foot->name }}</a></h4>
        <p>{{ $foot->description }}</p>
      </div>
    @endforeach
  </section>
  <!--Footer section --> 
  <footer class="footer">
    <div class="fixed-wrapper">
      <div class="footer_col quick_links">
        @php $column_1 = unserialize(get_constant_value('footer_column_first')); @endphp

        <h6>{{ $column_1[0] }}<span>{{ $column_1[1]}}</span></h6>
        <ul class="footer_links">
          @php $menus = unserialize($column_1['2']); @endphp
          @foreach($menus as $foot => $value)
            @php $current_menu = \App\Models\MenuItem::with('page')->find($value); @endphp
              @if(isset($current_menu))
                <li>
                  <a href="{{ env('Home_url').'#/page/'.$current_menu->page->slug}}">{{ $current_menu->name}}</a>
                </li>
              @endif
          @endforeach
        </ul>
      </div>
      <div class="footer_col service_links">
        @php $column_2 = unserialize(get_constant_value('footer_column_second')); @endphp
        <h6>{{ $column_2[0] }}<span>{{ $column_2[1]}}</span></h6>
        <ul class="footer_links">
          @php $menus = unserialize($column_2['2']); @endphp
          @foreach($menus as $foot => $value)
              @php $current_menu = \App\Models\MenuItem::with('page')->find($value); @endphp
              @if(isset($current_menu))
              <li><a href="{{ env('Home_url').'#/page/'.$current_menu->page->slug}}">{{ $current_menu->name}}</a></li>
              @endif
          @endforeach
        </ul>
      </div>
      <div class="footer_col connect_with">
        @php $column_3 = unserialize(get_constant_value('footer_column_third')); $image_Section = unserialize($column_3[2]);@endphp
        <h6>{{ $column_3[0] }}<span>{{ $column_3[1]}}</span></h6>
        <ul class="social_media">
          <li><a href="{{ $image_Section[0][0]}}"><img src="{{ get_storage_link_api($image_Section[0][1], 1)}}" alt=""></a></li>
          <li><a href="{{ $image_Section[1][0]}}"><img src="{{ get_storage_link_api($image_Section[1][1], 1)}}" alt=""></a></li>
          <li><a href="{{ $image_Section[2][0]}}"><img src="{{ get_storage_link_api($image_Section[2][1], 1)}}" alt=""></a></li>
          <li><a href="{{ $image_Section[3][0]}}"><img src="{{ get_storage_link_api($image_Section[3][1], 1)}}" alt=""></a></li>
        </ul>
        <strong>Find us.</strong>
        <p>{{ get_constant_value('footer_address') }}</p>
        <strong>Call us.</strong>
        <p><a href="tel:+{{ get_constant_value('footer_contact')}}">{{ get_constant_value('footer_contact')}}</a></p>
      </div>
      @php $column_4 = unserialize(get_constant_value('footer_column_forth'));@endphp
      <div class="footer_col logo_footer"> <a href="#"><img src="{{get_storage_link_api($column_4[0], 1)}}" alt=""></a>
        <p>{{ $column_4[1] }}</p>
    </div>
  </footer>
  <!--Copyright section --> 
  <div class="copyright">
    <div class="fixed-wrapper">
      <p>{{ str_replace('CURRENT_YEAR', date('Y'), $column_4[3]) }}</p>
    </div>
  </div>
@stop
