@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="http://placehold.it/160x160/00a65a/ffffff/&text={{ Auth::user()->name[0] }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
            <li><a href="{{ url(config('backpack.base.route_prefix').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

          
          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <!-- <li><a href="{ url(config('backpack.base.route_prefix', 'admin') . '/elfinder') }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li> -->
          <li class="treeview">
            <a href="#"><i class="fa fa-list"></i> <span>Menu</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url('admin/menu-category') }}"><i class="fa fa-angle-double-right"></i> <span>Category</span></a></li>
              <li><a href="{{ url('admin/menu-item') }}"><i class="fa fa-angle-double-right"></i> <span>Menu List</span></a></li>
            </ul>
          </li>
          
          <!-- <li><a href="{{ url('admin/user') }}"><i class="fa fa-list"></i> <span>Users</span></a></li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix').'/page') }}"><i class="fa fa-file-o"></i> <span>Pages</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/banner') }}"><i class="fa fa-angle-double-right"></i>Banner</a></li>
          <li><a href="{{ url('admin/footer') }}"><i class="fa fa-minus"></i> <span>Footer Section</span></a></li>
          {{-- <li><a href="{{ url('admin/setting') }}"><i class="fa fa-cog"></i> <span>Settings</span></a></li> --}}
          {{-- <li><a href="{{ url('admin/TechnologySliderCrud') }}"><i class="fa fa-cog"></i> <span>Technology Slider</span></a></li> --}}
         <!--  <li class="treeview">
           <a href="#"><i class="fa fa-globe"></i> <span>Translations</span> <i class="fa fa-angle-left pull-right"></i></a>
           <ul class="treeview-menu">
             <li><a href="{ url(config('backpack.base.route_prefix', 'admin').'/language') }}"><i class="fa fa-flag-checkered"></i> Languages</a></li>
             <li><a href="{ url(config('backpack.base.route_prefix', 'admin').'/language/texts') }}"><i class="fa fa-language"></i> Site texts</a></li>
           </ul>
         </li>  -->
          <li class="treeview">
            <a href="#"><i class="fa fa-group"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url('admin/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
              <li><a href="{{ url('admin/role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
              <li><a href="{{ url('admin/permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
            </ul>
          </li>
          <li class="treeview">
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>News</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url('admin/article') }}"><i class="fa fa-newspaper-o"></i> <span>Articles</span></a></li>
                <li><a href="{{ url('admin/category') }}"><i class="fa fa-list"></i> <span>Categories</span></a></li>
                <li><a href="{{ url('admin/tag') }}"><i class="fa fa-tag"></i> <span>Tags</span></a></li>
               
              </ul>
          </li>
          <li class="treeview">
              <a href="#"><i class="fa fa-university"></i> <span>University</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url('admin/university') }}"><i class="fa fa-graduation-cap"></i> <span>University</span></a></li>
                <li><a href="{{ url('admin/courses') }}"><i class="fa fa-folder-open"></i> <span>Courses</span></a></li>
                <li><a href="{{ url('admin/stream') }}"><i class="fa fa-folder-open"></i> <span>Streams</span></a></li>
                <li><a href="{{url(config('backpack.base.route_prefix', 'admin').'/component_filter') }}"><i class="fa fa-angle-double-right"></i>Component Filter</a></li>
              </ul>
          </li>
          <!-- <li><a href="{url(config('backpack.base.route_prefix', 'admin').'/university') }}"><i class="fa fa-angle-double-right"></i>University</a></li> -->
          <!-- <li><a href="{url(config('backpack.base.route_prefix', 'admin').'/component_filter') }}"><i class="fa fa-angle-double-right"></i>Component Filter</a></li> -->

          <li class="treeview">
            <a href="#"><i class="fa fa-file-o"></i> <span>Blog Section</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/blog_tag') }}"><i class="fa fa-angle-double-right"></i> Tags</a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/blog_category') }}"><i class="fa fa-angle-double-right"></i> Category</a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/post') }}"><i class="fa fa-angle-double-right"></i> Post</a></li>
              <li><a href="{{ url('admin/blogcomment') }}"><i class="fa fa-comment"></i> <span>Blog Comment</span></a></li>
            </ul>
          </li>
          <li><a href="{{ url('admin/testimonial') }}"><i class="fa fa-list"></i> <span>Testimonial</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-envelope"></i> <span>Mail</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="{{ url('admin/mail') }}"><i class="fa fa-cog"></i> <span>Mail Settings</span></a></li>
                <li><a href="{{ url('admin/mail_template') }}"><i class="fa fa-folder-open"></i> <span>Mail Templates</span></a></li>
            </ul>
          </li>
         <li><a href="{{ url('admin/faq') }}"><i class="fa fa-comment "></i> <span>FAQ</span></a></li> 
         <li><a href="{{ url('admin/events') }}"><i class="fa fa-calendar"></i> <span>Events</span></a></li>
       <!--  <li><a href="{ url('admin/external_link') }}"><i class="fa fa-external-link"></i> <span>External Link</span></a></li> -->

          <!-- <li><a href="{{ url('admin/faq') }}"><i class="fa fa-question-circle-o fa-comment "></i> <span>FAQ</span></a></li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
