@extends('backpack::layout')
@section('header')
	<section class="content-header">
	  <h1>
	    {{ trans('backpack::crud.edit') }} <span class="text-lowercase">{{ $crud->entity_name }}</span>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
	    <li><a href="{{ url($crud->route) }}" class="text-capitalize">{{ $crud->entity_name_plural }}</a></li>
	    <li class="active">{{ trans('backpack::crud.edit') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<!-- Default box -->
		@if ($crud->hasAccess('list'))
			<a href="{{ url($crud->route) }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span class="text-lowercase">{{ $crud->entity_name_plural }}</span></a><br><br>
		@endif

		  {!! Form::open(array('url' => $crud->route.'/'.$entry->getKey(), 'method' => 'put', 'files'=>$crud->hasUploadFields('create'))) !!}
		  <div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">{{ trans('backpack::crud.edit') }}</h3>
		    </div>
		    <div class="box-body row" style="position: relative;">
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content')
		      @else
		      	@include('crud::form_content', ['fields' => $crud->getFields('update', $entry->getKey())])
		      @endif

				@if(View::exists('crud::custom.'.$crud->entity_name.'_edit'))
					@include('crud::custom.'.$crud->entity_name.'_edit')
				@endif
		
		    </div><!-- /.box-body -->
		    <div class="box-footer">

			  <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('backpack::crud.save') }}</span></button>
		      <a href="{{ url($crud->route) }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">{{ trans('backpack::crud.cancel') }}</span></a>
		    </div><!-- /.box-footer-->
		  </div><!-- /.box -->
		  {!! Form::close() !!}
	</div>
</div>
@endsection
@push('custom_after_scripts')
<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript"> 
  jQuery.validator.addMethod("noSpace", function(value, element) { 
      return value == '' || value.trim().length != 0;  
    }, "No space please and don't leave it empty");
jQuery.validator.addMethod("custom_email", function(value, element) {
               return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
            }, "Please enter a valid email address.");

$(document).ready(function(){
 
 $("form").validate({ 
     // ignore: [], 
      rules: { 
        	name:{required:true,noSpace: true},
        	title:{required:true,noSpace: true},
        	heading:{required:true,noSpace: true},
        	text:{required:true,noSpace: true},
        	author_name:{required:true,noSpace: true},
        	email:{required:true,custom_email: true},
        	password:{ minlength: 6},
        	question:{required:true,noSpace: true},
      		answer:{required:true,noSpace: true},
      		password_confirmation:{ equalTo: '[name="password"]'},
      		//statement:{maxlength:300},
      }, 
      errorPlacement: function (error, element) { 
          error.appendTo($(element).closest('.form-group')); 
      } 
});  

    jQuery( '.faqckedit' ).ckeditor({width:'98%', height: '350px', toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        [ 'Cut', 'Copy', 'Paste', 'Image','Link','Iframe','Maximize','NumberedList','BulletedList','PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],     // Defines toolbar group without name.
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline','texttransform','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'   ] },'/',
        { name: 'Fonts', items : ['FontSize','TextColor','BGColor','Font','Styles','Format','RemoveFormat','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Outdent','Indent']}
      ]});

});

</script>
@endpush
