
      @php $oldcontent = unserialize(get_constant_value('footer_column_fifth')); @endphp
      {!! Form::open(['url' => 'admin/footer/update',  'enctype' => 'multipart/form-data']) !!}
        <div class="box">
          <div class="box-header btn-primary ">Fifth Column</div>
          <div class="box-body row">
            <input type="hidden" name="key[]" value="footer_column_fifth" />
            
            @php $data = ['name' => 'copyright', 'label' => 'Copyright Content', 'type' => 'text', 'value' => @$oldcontent['copyright']]; @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}