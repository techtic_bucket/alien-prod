<div class="row">
    <div class="col-md-12">
      <!-- Default box -->
      {!! Form::open(['url' => 'admin/constant/update',  'enctype' => 'multipart/form-data']) !!}
        <div class="box">
          <div class="box-body row">
            
            @php
            $data = ['name' =>'home_statics_bg', 'crop'=>1, 'aspect_ratio' => 0,  'label' => 'Section Background Image', 'type'=>'image', 'upload' => 1, 'other_module' => 1, 'default' => get_storage_link('uploads/about_banner.jpg'), 'value' => get_storage_link(get_constant_value('home_statics_bg')) ];
            @endphp
            <input type="hidden" name="key[]" value="home_statics_bg" /> 

            @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <!-- Default box -->
      @php $oldcontent = unserialize(get_constant_value('home_statics_right_content')); @endphp
      {!! Form::open(['url' => 'admin/constant/update_home_right',  'enctype' => 'multipart/form-data']) !!}
        <div class="box">
          <div class="box-header btn-primary ">Right Content Section</div>
          <div class="box-body row">
            <input type="hidden" name="key[]" value="home_statics_right_content" />
            
            @php $key=1; @endphp
            <div class='section-right-content'>
                @php foreach ($oldcontent as $key1 => $value) {  @endphp
                <div class='section-field'>
                    @php $data = ['name' => 'title_'.$key, 'label' => 'Title', 'type' => 'text', 'value' =>@$value['title']]; @endphp
                    @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

                    @php $data = ['name' => 'description_'.$key, 'label' => 'Content','value' =>@$value['description'] ]; @endphp
                    @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])

                    @php
                    $data = ['name' =>'image_'.$key, 'crop'=>1, 'aspect_ratio' => 0,  'label' => 'Background Image', 'type'=>'image', 'upload' => 1, 'other_module' => 1, 'default' => get_storage_link('uploads/about_banner.jpg'), 'value' =>@$value['image_name'] ];
                    @endphp
                    @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])
                    <div class='col-md-12'>
                        <button type='button' class='btn btn-info new-section-delete'>Delete Section</button>
                    </div>
                </div>
                @php $key++; } @endphp
            </div>
            <input type="hidden" name="right_content_count" value="<?php echo --$key; ?>" id='right_content_count' />
            <div id='section-right-content1' style="display: none;">
                <div class='section-field'>
                    @php $data = ['name' => 'title_$key', 'label' => 'Title', 'type' => 'text', 'value' =>'']; @endphp
                    @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

                    @php $data = ['name' => 'description_$key', 'label' => 'Content','value' =>'']; @endphp
                    @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])

                    @php
                    $data = ['name' =>'image_$key', 'crop'=>1, 'aspect_ratio' => 0,  'label' => 'Background Image', 'type'=>'image', 'upload' => 1, 'other_module' => 1, 'default' => get_storage_link('uploads/about_banner.jpg'), 'value' => '' ];
                    @endphp
                    @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])
                    <div class='col-md-12'>
                        <button type='button' class='btn btn-info new-section-delete'>Delete Section</button>
                    </div>
                </div>
            </div>

            <div class='col-md-12'></div>
           

          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
            <button type='button' class='btn btn-info new-section'>Add New Section</button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}
    </div>
  </div>
    <link href="{{ asset('vendor/backpack/cropper/dist/cropper.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .hide {display: none; }
        .btn-group {margin-top: 10px; }
        img {max-width: 100%; /* This rule is very important, please do not ignore this! */ }
        .img-container, .img-preview {width: 100%; text-align: center; } 
        .img-preview {float: left; margin-right: 10px; margin-bottom: 10px; overflow: hidden; }
        .preview-lg {width: 263px; height: 148px; } 
        .btn-file {position: relative; overflow: hidden; } 
        .btn-file input[type=file] {position: absolute; top: 0; right: 0; min-width: 100%; min-height: 100%; font-size: 100px; text-align: right; filter: alpha(opacity=0); opacity: 0; outline: none; background: white; cursor: inherit; display: block; }
    </style>

  @push('custom_after_scripts')

    <script src="{{ asset('vendor/backpack/cropper/dist/cropper.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-sortable/0.14.3/sortable.min.js"></script>
        <script>
        var note_container = document.getElementById('section-right-content1').innerHTML;
        document.getElementById('section-right-content1').innerHTML = ""; 

            $(document).ready(function($) { 
               
                 var key = <?php echo $key > 0 ? $key : 0; ?>;
                    $('.new-section').on('click',function(){
                        
                        key ++;
                        html = replaceAll(note_container, '$key', key);
                        $('.section-right-content').append(html);
                         $('.form-group.image').each(function(index){
                    // Find DOM elements under this form-group element
                    var $mainImage = $(this).find('#mainImage');
                    var $uploadImage = $(this).find("#uploadImage");
                    var $hiddenImage = $(this).find("#hiddenImage");
                    var $rotateLeft = $(this).find("#rotateLeft")
                    var $rotateRight = $(this).find("#rotateRight")
                    var $zoomIn = $(this).find("#zoomIn")
                    var $zoomOut = $(this).find("#zoomOut")
                    var $reset = $(this).find("#reset")
                    var $remove = $(this).find("#remove")
                    // Options either global for all image type fields, or use 'data-*' elements for options passed in via the CRUD controller
                    var options = {
                        viewMode: 2,
                        checkOrientation: false,
                        autoCropArea: 1,
                        responsive: true,
                        preview : $(this).attr('data-preview'),
                        aspectRatio : $(this).attr('data-aspectRatio')
                    };
                    var crop = $(this).attr('data-crop');

                    // Hide 'Remove' button if there is no image saved
                    if (!$mainImage.attr('src')){
                        $remove.hide();
                    }
                    // Initialise hidden form input in case we submit with no change
                    $hiddenImage.val($mainImage.attr('src'));


                    // Only initialize cropper plugin if crop is set to true
                    if(crop){

                        $remove.click(function() {
                            $mainImage.cropper("destroy");
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $rotateLeft.hide();
                            $rotateRight.hide();
                            $zoomIn.hide();
                            $zoomOut.hide();
                            $reset.hide();
                            $remove.hide();
                        });
                    } else {

                        $(this).find("#remove").click(function() {
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $remove.hide();
                        });
                    }
                    

                    /*$('.section-right-content').on('click',function(){
                        
                    });    */    

                    $uploadImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }
                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $uploadImage.val("");
                                if(crop){
                                    $mainImage.cropper(options).cropper("reset", true).cropper("replace", this.result);
                                    // Override form submit to copy canvas to hidden input before submitting
                                    $('form').submit(function() {
                                        var imageURL = $mainImage.cropper('getCroppedCanvas').toDataURL();
                                        $hiddenImage.val(imageURL);
                                        return true; // return false to cancel form action
                                    });
                                    $rotateLeft.click(function() {
                                        $mainImage.cropper("rotate", 90);
                                    });
                                    $rotateRight.click(function() {
                                        $mainImage.cropper("rotate", -90);
                                    });
                                    $zoomIn.click(function() {
                                        $mainImage.cropper("zoom", 0.1);
                                    });
                                    $zoomOut.click(function() {
                                        $mainImage.cropper("zoom", -0.1);
                                    });
                                    $reset.click(function() {
                                        $mainImage.cropper("reset");
                                    });
                                    $rotateLeft.show();
                                    $rotateRight.show();
                                    $zoomIn.show();
                                    $zoomOut.show();
                                    $reset.show();
                                    $remove.show();

                                } else {
                                    $mainImage.attr('src',this.result);
                                    $hiddenImage.val(this.result);
                                    $remove.show();
                                }
                            };
                        } else {
                            alert("Please choose an image file.");
                        }
                    });

                });
                        $('#right_content_count').val(key);
                    });

                    
                    $(document).on('click','.new-section-delete',function(){
                        var count=$('#right_content_count').val();
                        $(this).parents('.section-field').remove();
                    });
                // Loop through all instances of the image field
                $('.form-group.image').each(function(index){
                    // Find DOM elements under this form-group element
                    var $mainImage = $(this).find('#mainImage');
                    var $uploadImage = $(this).find("#uploadImage");
                    var $hiddenImage = $(this).find("#hiddenImage");
                    var $rotateLeft = $(this).find("#rotateLeft")
                    var $rotateRight = $(this).find("#rotateRight")
                    var $zoomIn = $(this).find("#zoomIn")
                    var $zoomOut = $(this).find("#zoomOut")
                    var $reset = $(this).find("#reset")
                    var $remove = $(this).find("#remove")
                    // Options either global for all image type fields, or use 'data-*' elements for options passed in via the CRUD controller
                    var options = {
                        viewMode: 2,
                        checkOrientation: false,
                        autoCropArea: 1,
                        responsive: true,
                        preview : $(this).attr('data-preview'),
                        aspectRatio : $(this).attr('data-aspectRatio')
                    };
                    var crop = $(this).attr('data-crop');

                    // Hide 'Remove' button if there is no image saved
                    if (!$mainImage.attr('src')){
                        $remove.hide();
                    }
                    // Initialise hidden form input in case we submit with no change
                    $hiddenImage.val($mainImage.attr('src'));


                    // Only initialize cropper plugin if crop is set to true
                    if(crop){

                        $remove.click(function() {
                            $mainImage.cropper("destroy");
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $rotateLeft.hide();
                            $rotateRight.hide();
                            $zoomIn.hide();
                            $zoomOut.hide();
                            $reset.hide();
                            $remove.hide();
                        });
                    } else {

                        $(this).find("#remove").click(function() {
                            $mainImage.attr('src','');
                            $hiddenImage.val('');
                            $remove.hide();
                        });
                    }
                    

                    /*$('.section-right-content').on('click',function(){
                        
                    });    */    

                    $uploadImage.change(function() {
                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        if (!files.length) {
                            return;
                        }
                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {
                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {
                                $uploadImage.val("");
                                if(crop){
                                    $mainImage.cropper(options).cropper("reset", true).cropper("replace", this.result);
                                    // Override form submit to copy canvas to hidden input before submitting
                                    $('form').submit(function() {
                                        var imageURL = $mainImage.cropper('getCroppedCanvas').toDataURL();
                                        $hiddenImage.val(imageURL);
                                        return true; // return false to cancel form action
                                    });
                                    $rotateLeft.click(function() {
                                        $mainImage.cropper("rotate", 90);
                                    });
                                    $rotateRight.click(function() {
                                        $mainImage.cropper("rotate", -90);
                                    });
                                    $zoomIn.click(function() {
                                        $mainImage.cropper("zoom", 0.1);
                                    });
                                    $zoomOut.click(function() {
                                        $mainImage.cropper("zoom", -0.1);
                                    });
                                    $reset.click(function() {
                                        $mainImage.cropper("reset");
                                    });
                                    $rotateLeft.show();
                                    $rotateRight.show();
                                    $zoomIn.show();
                                    $zoomOut.show();
                                    $reset.show();
                                    $remove.show();

                                } else {
                                    $mainImage.attr('src',this.result);
                                    $hiddenImage.val(this.result);
                                    $remove.show();
                                }
                            };
                        } else {
                            alert("Please choose an image file.");
                        }
                    });

                });
            });

            window.angularApp = window.angularApp || angular.module('backPackTableApp', ['ui.sortable'], function($interpolateProvider){
                $interpolateProvider.startSymbol('<%');
                $interpolateProvider.endSymbol('%>');
            });

            window.angularApp.controller('tableController', function($scope){

                $scope.sortableOptions = {
                    handle: '.sort-handle'
                };

                $scope.addItem = function(){

                    if( $scope.max > -1 ){
                        if( $scope.items.length < $scope.max ){
                            var item = {};
                            $scope.items.push(item);
                        } else {
                            new PNotify({
                                title: $scope.maxErrorTitle,
                                text: $scope.maxErrorMessage,
                                type: 'error'
                            });
                        }
                    }
                    else {
                        var item = {};
                        $scope.items.push(item);
                    }
                }

                $scope.removeItem = function(item){
                    var index = $scope.items.indexOf(item);
                    $scope.items.splice(index, 1);
                }

                $scope.$watch('items', function(a, b){

                    if( $scope.min > -1 ){
                        while($scope.items.length < $scope.min){
                            $scope.addItem();
                        }
                    }

                    if( typeof $scope.items != 'undefined' && $scope.items.length ){

                        if( typeof $scope.field != 'undefined'){
                            if( typeof $scope.field == 'string' ){
                                $scope.field = $($scope.field);
                            }
                            $scope.field.val( angular.toJson($scope.items) );
                        }
                    }
                }, true);

                if( $scope.min > -1 ){
                    for(var i = 0; i < $scope.min; i++){
                        $scope.addItem();
                    }
                }
            });

            function replaceAll(string, find, replace){
                return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
            }
            function escapeRegExp(string){
                return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
            }
            angular.element(document).ready(function(){
                angular.forEach(angular.element('[ng-app]'), function(ctrl){
                    var ctrlDom = angular.element(ctrl);
                    if( !ctrlDom.hasClass('ng-scope') ){
                        angular.bootstrap(ctrl, [ctrlDom.attr('ng-app')]);
                    }
                });
            })

        </script>
    @endpush