@php $oldcontent = unserialize(get_constant_value('footer_column_first')); @endphp
      {!! Form::open(['url' => 'admin/footer/update',  'enctype' => 'multipart/form-data','id'=>'column_first']) !!}
        <div class="box">
          <div class="box-header btn-primary ">First Column</div>
          <div class="box-body row">
            <input type="hidden" name="key[]" value="footer_column_first" /> 
            
            @php $data = [
                'name' => 'title', 
                'label' => 'Title', 
                'type' => 'text', 
                'value' => isset($oldcontent['title']) ? $oldcontent['title']: ""
              ];
             @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])
            
            @php 
            $data = [
              'name' => 'description',
              'label' => 'Description',
              'type' => 'textarea',
              'value' => isset($oldcontent['description']) ? $oldcontent['description']: ""
            ]; @endphp
            @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])

           <div class="col-md-6">
              
              @php 
                $image_section = isset($oldcontent['links']) ? $oldcontent['links'] : []; 

                $data = [
                  'name' => 'link_1',
                  'label' => 'First Image Link',
                  'type' => 'url',
                  'value' => isset($image_section[0]['url']) ? $image_section[0]['url'] : ""
                ];
              @endphp
              @include('vendor.backpack.crud.fields.url', ['field' => $data,'fields' => ''])

              @php 
                $data = [
                  'name' =>'image_1',
                  'crop'=>1,
                  'aspect_ratio' => 0,
                  'label' => 'First Image',
                  'type'=>'image',
                  'upload' => 1,
                  'other_module' => 1,
                  'default' => 'http://localhost/aur_new/web-api/uploads/fb.png',
                  'value' => isset($image_section[0]['image']) ? get_storage_link($image_section[0]['image'])  : ""
                ];  
              @endphp
              @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])

            </div>
            <div class="col-md-6">

              @php 
              $data = [
                'name' => 'link_2', 
                'label' => 'Second Image Link', 
                'type' => 'url', 
                'value' => isset($image_section[1]['url']) ? $image_section[1]['url'] : "" 
                ]; 
              @endphp
              @include('vendor.backpack.crud.fields.url', ['field' => $data,'fields' => ''])

              @php 
                $data = [
                'name' =>'image_2', 
                'crop'=>1, 
                'aspect_ratio' => 0,  
                'label' => 'Second Image', 
                'type'=>'image', 
                'upload' => 1, 
                'other_module' => 1, 
                'default' => get_uploaded_path('uploads/tw.png'), 
                'value' => isset($image_section[1]['image']) ? get_storage_link($image_section[1]['image']) : ""
              ];  
              @endphp
              @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])

            </div>
            <div class="col-md-6">

              @php 
                $data = [
                  'name' => 'link_3', 
                  'label' => 'Third Image Link', 
                  'type' => 'url', 
                  'value' => isset($image_section[2]['url']) ? $image_section[2]['url'] : ""
                ]; 
              @endphp
              @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

              @php 
                $data = [
                  'name' =>'image_3', 
                  'crop'=>1, 
                  'aspect_ratio' => 0,  
                  'label' => 
                  'Third Image', 
                  'type'=>'image', 
                  'upload' => 1, 
                  'other_module' => 1, 
                  'default' => get_uploaded_path('uploads/instagram.png'), 
                  'value' => isset($image_section[2]['image']) ? get_storage_link($image_section[2]['image']) : ""
                ];  
              @endphp
              @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])

            </div>
            <div class="col-md-6">

              @php 
                $data = [
                  'name' => 'link_4', 
                  'label' => 'Forth Image Link', 
                  'type' => 'url', 
                  'value' => isset($image_section[3]['url']) ? $image_section[3]['url'] : ""
                ];
              @endphp
              @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

              @php 
                $data = [
                  'name' =>'image_4', 
                  'crop'=>1, 'aspect_ratio' => 0,  
                  'label' => 'Forth Image', 
                  'type'=>'image', 
                  'upload' => 1, 
                  'other_module' => 1, 
                  'default' => get_uploaded_path('uploads/linkdin.png'), 
                  'value' => isset($image_section[3]['image']) ? get_storage_link($image_section[3]['image']) : ""
                ];  
              @endphp
              @include('vendor.backpack.crud.fields.image', ['field' => $data,'fields' => ''])
      
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}
