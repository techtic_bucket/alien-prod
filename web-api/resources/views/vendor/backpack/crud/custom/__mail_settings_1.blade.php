
    <div class="box-header btn-primary ">Mail Settings</div>
        <div class="box-body row">
            @php $data = [
                'name' => 'mail_header', 
                'label' => 'Mail Header', 
                'type' => 'textarea', 
                'value' => get_option('mail_header'),
                'attributes'=>array('class'=>'faqckedit form-control'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])
       
            @php $data = [
                'name' => 'mail_footer', 
                'label' => 'Mail Footer', 
                'type' => 'textarea', 
                'value' => get_option('mail_footer'),
                'attributes'=>array('class'=>'faqckedit form-control'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])
        
            @php $data = [
                'name' => 'from_name', 
                'label' => 'From Name', 
                'type' => 'text', 
                'value' => get_option('from_name'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])
        
            @php $data = [
                'name' => 'from_email', 
                'label' => 'From Email', 
                'type' => 'text', 
                'value' => get_option('from_email'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

            @php $data = [
                'name' => 'contact_from_email', 
                'label' => 'Contact From Email', 
                'type' => 'text', 
                'value' => get_option('contact_from_email'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])
            @php $data = [
                'name' => 'ask_question_from_mail', 
                'label' => 'Ask Question From Email', 
                'type' => 'text', 
                'value' => get_option('ask_question_from_mail'),
              ];
            @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])
        
</div>