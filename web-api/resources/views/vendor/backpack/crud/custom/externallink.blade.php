<?php
use App\Models\ExternalLink;
$data_external_link=ExternalLink::get();

?>
<div class="row">
    <div class="col-md-12">
    	<div class="box">
	        {!! Form::open(['url' => 'admin/external_link/update_val',  'enctype' => 'multipart/form-data']) !!}
	        	<div class="box-header btn-primary ">External Links</div>
			        <div class="box-body row">
			            <div class='ref-add-one'>
							<div class="col-sm-12">
							<table class="table table-bordered table-striped display dataTable">
								<tr>
									<td>Title</td>
									<td>Link</td>
									<td>Action</td>

								</tr>
								<?php $key=1;
								if($data_external_link->count()>0){
                        foreach ($data_external_link as $key => $value) { ?>
								<tr class='ref-add'>
									<td><input type="text" name='external_link[{{$key}}][title]' id='title{{$key}}' class="form-control" value="{{$value->title}}" ></td>
									<td><input type="text" name='external_link[{{$key}}][link]' id='link{{$key}}' class="form-control" value="{{$value->link}}" ></td>
									<td><a herf="javascript:" class="remove_note btn right btn-danger"><i class="fa fa-trash" role="presentation" aria-hidden="true"></i></a></td>

								</tr>
						<?php $key++; } } ?>
							</table>
							</div>
                       	<hr>

                        

                    	</div>
                    	<table id='ref-add'>
                    		<tr class='ref-add'>
                    			<td><input type="text" name='external_link[$key][title]' id='title$key' class="form-control" value=""></td>
                    			<td><input type="text" name='external_link[$key][link]' id='link$key' class="form-control" value=""></td>
                    			<td><a herf="javascript:" class="remove_note btn right btn-danger"><i class="fa fa-trash" role="presentation" aria-hidden="true"></i></a></td>
                    		</tr>
                    	</table>
                    	

			        	<div class="col-sm-12 add-note">
							<button class='add_note btn btn-success' type='button'><i class="fa fa-plus"></i> Add External Link</button>
						</div>
			        </div>


			    <div class="box-footer">
	                <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Save</span></button>
	            </div>
	        </form>
      	</div>  
    </div>
</div>
@push('after_styles')
<style>
.head_table{
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    font-weight: 400;
    font-size: 14px;
    color: #333;
    text-align: center;
}
</style>
@endpush

@push('custom_after_scripts')
<script type="text/javascript">
$ = jQuery;
var note_container = document.getElementById('ref-add').innerHTML;
document.getElementById('ref-add').innerHTML = ""; 
$(document).ready(function(){
	var key = <?php echo $key > 0 ? $key : 0; ?>;
	 $(document).on('click','.remove_note',function(){
	 	var numItems = $('.remove_note').length;
	 		
	 			$(this).parents('.ref-add').remove();
	 		
	});

    $('.add_note').on('click',function(){
		var numItems = $('.remove_note').length;
			
				html = replaceAll(note_container,'$key',key);
				html = replaceAll(html,'<tbody>','');
				console.log(html);
				$('.ref-add-one table tbody').append(html);
				key++;
			
	});
});

function replaceAll(string, find, replace) { return string.replace(new RegExp(escapeRegExp(find), 'g'), replace); }
function escapeRegExp(string) { return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");}
</script>
@endpush