<div class="row">
    <div class="col-md-12">
      <!-- Default box -->

      {!! Form::open(['url' => 'admin/technology/title']) !!}
        <div class="box">
          <div class="box-body row">
            <div class="form-group col-md-12">

              <label>Title</label><input name="title" class="form-control" type="text" value="<?php echo \App\Models\Constant::select('meta_value')->where('meta_key', 'technology_title')->first()->meta_value; ?>">
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}

      {!! Form::open(['url' => 'admin/constant/update']) !!}
        <div class="box">
          <div class="box-body row">
            <div class="form-group col-md-12">
              <label>Contact Us text</label>
              <input type="hidden" name="key[]" value="technology_cctitle" /> 
              <input name="technology_cctitle" class="form-control" type="text" value="<?php echo \App\Models\Constant::select('meta_value')->where('meta_key', 'technology_cctitle')->first()->meta_value; ?>">
            </div>
            <div class="form-group col-md-12">
              <label>Contact Us Link</label>
              <input type="hidden" name="key[]" value="technology_cclink" /> 
              <input name="technology_cclink" class="form-control" type="text" value="<?php echo \App\Models\Constant::select('meta_value')->where('meta_key', 'technology_cclink')->first()->meta_value; ?>">
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}
    </div>
  </div>