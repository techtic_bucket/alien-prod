<div class="row">
    <div class="col-md-12">
      <div class="box">
         {!! Form::open(['url' => 'admin/mail/update',  'enctype' => 'multipart/form-data']) !!}
          @include('vendor.backpack.crud.custom.__mail_settings_1')
          @include('vendor.backpack.crud.custom.__mail_settings_2') 
          <div class="box-footer">
                <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div>
          </form>
      </div>  
    </div>
  </div>
    <link href="{{ asset('vendor/backpack/cropper/dist/cropper.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .hide {display: none; }
        .btn-group {margin-top: 10px; }
        img {max-width: 100%; /* This rule is very important, please do not ignore this! */ }
        .img-container, .img-preview {width: 100%; text-align: center; } 
        .img-preview {float: left; margin-right: 10px; margin-bottom: 10px; overflow: hidden; }
        .preview-lg {width: 263px; height: 148px; } 
        .btn-file {position: relative; overflow: hidden; } 
        .btn-file input[type=file] {position: absolute; top: 0; right: 0; min-width: 100%; min-height: 100%; font-size: 100px; text-align: right; filter: alpha(opacity=0); opacity: 0; outline: none; background: white; cursor: inherit; display: block; }
    </style>

@push('custom_after_scripts')
<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/jquery.js') }}"></script>
<script>

jQuery(document).ready(function(){        

    jQuery( '.faqckedit' ).ckeditor({width:'98%', height: '350px', toolbar: [
        { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
        [ 'Cut', 'Copy', 'Paste', 'Image','Link','Iframe','Maximize','NumberedList','BulletedList','PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],     // Defines toolbar group without name.
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline','texttransform','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'   ] },'/',
        { name: 'Fonts', items : ['FontSize','TextColor','BGColor','Font','Styles','Format','RemoveFormat','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Outdent','Indent']}
      ]});

});

</script>
@endpush