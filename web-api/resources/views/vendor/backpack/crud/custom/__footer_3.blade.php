@php $oldcontent = unserialize(get_constant_value('footer_column_third')); @endphp
      {!! Form::open(['url' => 'admin/footer/update',  'enctype' => 'multipart/form-data']) !!}
        <div class="box">
          <div class="box-header btn-primary ">Third Column</div>
          <div class="box-body row">
            <input type="hidden" name="key[]" value="footer_column_third" />
            
            @php
                  $menu_cat=App\Models\MenuCategory::get();
                  $menu_val=array();
                  foreach ($menu_cat as $k => $v) {
                    $menu_val[$v->id]=$v->title;
                  }
                $data = ['label' => "Select Menu", 'type' => 'select_from_array', 'name' => 'menu_item','value' =>$oldcontent['menu_item'],'options' => $menu_val];

                $text_data = ['label' => "Menu Title", 'type' => 'text', 'name' => 'title','value' => isset($oldcontent) ? $oldcontent['title'] : ""]; 
          @endphp

          
          @include('vendor.backpack.crud.fields.text', ['field' => $text_data, 'fields' => ''])


          @include('vendor.backpack.crud.fields.select_from_array', ['field' => $data,'fields' => ''])

          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}