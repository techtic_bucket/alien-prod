
    <div class="box-header btn-primary ">Mail Templates Settings</div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Register Mail",
                       'type' => 'select_option_same_model',
                       'name' => 'registration_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('registration_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Register Mail For Admin",
                       'type' => 'select_option_same_model',
                       'name' => 'registration_mail_admin',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('registration_mail_admin'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Register Mail With facebook",
                       'type' => 'select_option_same_model',
                       'name' => 'registration_mail_facebook',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('registration_mail_facebook'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Register Mail With gmail",
                       'type' => 'select_option_same_model',
                       'name' => 'registration_mail_gmail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('registration_mail_gmail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Forget Password Mail",
                       'type' => 'select_option_same_model',
                       'name' => 'forget_password_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('forget_password_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Apply Now Mail",
                       'type' => 'select_option_same_model',
                       'name' => 'apply_now_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('apply_now_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Contact Us Mail",
                       'type' => 'select_option_same_model',
                       'name' => 'contact_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('contact_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Ask Question Email",
                       'type' => 'select_option_same_model',
                       'name' => 'ask_question_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('ask_question_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>
    <div class="box-body row">
            @php $data = [  // Select
                       'label' => "Ask Question Admin Email",
                       'type' => 'select_option_same_model',
                       'name' => 'ask_question_admin_mail',
                       'model' => "App\Models\settings",
                       'attribute'=>'name',
                       'value' =>get_option('ask_question_admin_mail'),
                       'opt'=>array('key'=>'mail_template') // foreign key model
                    ];
             
            @endphp
            @include('vendor.backpack.crud.fields.select_option_same_model', ['field' => $data,'fields' => ''])
    </div>