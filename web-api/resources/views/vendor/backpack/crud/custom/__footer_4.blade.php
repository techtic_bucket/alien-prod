
      @php $oldcontent = unserialize(get_constant_value('footer_column_forth')); @endphp
      {!! Form::open(['url' => 'admin/footer/update',  'enctype' => 'multipart/form-data','id'=>'column_forth']) !!}
        <div class="box">
          <div class="box-header btn-primary ">Forth Column</div>
          <div class="box-body row">
            <input type="hidden" name="key[]" value="footer_column_forth" />
            
            @php $data = ['name' => 'title', 'label' => 'Title', 'type' => 'text', 'value' => $oldcontent['title']]; @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])
            
            @php $data = ['name' => 'address', 'label' => 'Address', 'type' => 'textarea', 'value' => $oldcontent['address']]; @endphp
            @include('vendor.backpack.crud.fields.textarea', ['field' => $data,'fields' => ''])

            @php $data = ['name' => 'phone', 'label' => 'Phone', 'type' => 'text', 'value' => $oldcontent['phone']]; @endphp
            @include('vendor.backpack.crud.fields.text', ['field' => $data,'fields' => ''])

            @php $data = ['name' => 'email', 'label' => 'Email Address', 'type' => 'email', 'value' => $oldcontent['email']]; @endphp
            @include('vendor.backpack.crud.fields.email', ['field' => $data,'fields' => ''])

            @php $data = ['name' => 'website', 'label' => 'Website', 'type' => 'url', 'value' => $oldcontent['website']]; @endphp
            @include('vendor.backpack.crud.fields.url', ['field' => $data,'fields' => ''])

          </div><!-- /.box-body -->
          <div class="box-footer">
            <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Update</span></button>
          </div><!-- /.box-footer-->
        </div>
      {!! Form::close() !!}