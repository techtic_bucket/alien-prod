
<td>
		<select name="{{ $column['name'] }}" id='{{$column['id']}}' class='{{ $column['name'] }}'>
			@if (count($column['options']))
	    		@foreach ($column['options'] as $key => $value)
	    			<option value="{{ $key }}"
                        @if ((isset($column['value']) && $key==$column['value']) || ( ! is_null( old($column['name']) ) && old($column['name']) == $key) )
							 selected
						@endif
	    			>{{ $value }}</option>
	    		@endforeach
	    	@endif
	    </select>
</td>	    
<script type="text/javascript"></script>	