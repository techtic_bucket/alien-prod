<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Allintech</title>
        <style>
            body{ background: #F0F3F6;}
        </style>
    </head>
    <body>
        <table style="width:100%;">
            <tr>
                <td style="font-family: arial;">
                    <h2 style="font-family: arial; font-size:18px;">Hello</h2>

                    <p style="font-family: arial; font-weight: bold;">You have new contact enquiry:</p>
                    <table>
                        <tr>
                            <td style="font-family: arial; font-weight: bold;">Name</td><td>{{$name}}</td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-weight: bold;">Email</td><td>{{$email}}</td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-weight: bold;">Company Name</td><td>{{$company_name}}</td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-weight: bold;">Subject</td><td>{{$subject}}</td>
                        </tr>
                        <tr>
                            <td style="font-family: arial; font-weight: bold;">Message</td><td>{{$mess}}</td>
                        </tr>
                    </table>
                    <p><a style="color: #222222; font-size: 15px; text-decoration: none;" class="button" href="">Allintech</a></p>
                </td>
            </tr>
        </table>
    </body>
</html>
