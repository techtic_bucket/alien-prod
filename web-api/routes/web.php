<?php

use \Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin');
});

Route::get('/login', function () {
    return redirect('admin/login');
});

Route::get('admin/login','LoginController@login');
Route::get('/timthumb', 'TimthumbController@index');


Route::group(['prefix' => 'admin', 'middleware' => ['web','auth','role:admin,access_backend'], 'namespace' => 'Admin'], function () {
      


        
        
        CRUD::resource('article', 'ArticleCrudController');
        CRUD::resource('banner', 'BannerCrudController');
        CRUD::resource('blog_category', 'BlogCategoryCrudController');
        CRUD::resource('blog_tag', 'BlogTagCrudController');
        CRUD::resource('blogcomment', 'BlogCommentCrudController');
        CRUD::resource('component_filter', 'Component_filterCrudController');
        CRUD::resource('courses', 'CoursesCrudController');
        CRUD::resource('events', 'EventsCrudController');
        CRUD::resource('external_link', 'ExternalLinkCrudController');
        CRUD::resource('faq', 'FaqCrudController');
        CRUD::resource('footer', 'FooterCrudController');
        CRUD::resource('mail', 'MailCrudController');
        CRUD::resource('mail_template', 'MailTemplateCrudController');
        CRUD::resource('menu-category', 'MenuCategoryCrudController');
        CRUD::resource('menu-item', 'MenuItemCrudController');
        Route::get('menu-item/delete/{id}', 'MenuItemCrudController@delete');
        CRUD::resource('newscomment', 'NewsCommentCrudController');
        CRUD::resource('page', 'PageCrudController');
        CRUD::resource('post', 'PostCrudController');
        CRUD::resource('stream', 'StreamCrudController');
        CRUD::resource('testimonial', 'TestimonialCrudController');
        CRUD::resource('university', 'UniversityCrudController');
        CRUD::resource('user', 'UserCrudController');
        CRUD::resource('permission', 'PermissionCrudController');

        
        Route::get('menu-category/set-order/{id}','MenuCategoryCrudController@set_order');
      /*  Route::get('menu-category/set-order/{id}', [
   'middleware'=> 'can:set-orderMenuCategoryPage',
   'uses' => 'MenuCategoryCrudController@set_order',
]);*/
        Route::post('blogcomment/change_status/{id}','BlogCommentCrudController@change_status');
        Route::post('external_link/update_val','ExternalLinkCrudController@update_val');
        Route::post('footer/update','\App\Http\Controllers\FooterController@update');
        Route::post('mail/update','\App\Http\Controllers\Admin\MailCrudController@update');
        Route::post('menu-category/saveReorderMenu/{id}','MenuCategoryCrudController@saveReorderMenu');
        Route::post('newscomment/change_status/{id}','NewsCommentCrudController@change_status');
       /* Route::get('newscommentlist/{id}', 'NewsCommentCrudController@list');*/

        Route::post('university/course/get', [
            'as' => 'university.course.get',
            'uses' => 'UniversityCrudController@get_course'
            ]);

        Route::post('university/course/update', [
            'as' => 'university.course.update',
            'uses' => 'UniversityCrudController@update_course'
            ]);

        Route::post('university/course/delete', [
            'as' => 'university.course.delete',
            'uses' => 'UniversityCrudController@delete_course'
            ]);

        Route::get('university/course/data/{uid}', [
            'as' => 'university.course.data',
            'uses' => 'UniversityCrudController@get_course_data'
            ]);


});

Route::group(['prefix' => 'api',  'namespace' => 'Api'], function () {

    //Route::post('filter','UniversityApi@filter');
    Route::get('blog','BlogApi@index');
    Route::get('blog/{id}','BlogApi@show');
    Route::get('compare','UniversityApi@compare');
    Route::get('component_filter','GetComponentFilterApi@index');
    Route::get('components','UniversityApi@components');
    Route::get('courses','CourseApi@index');
    Route::get('courses','UniversityApi@courses');
    Route::get('csrf','CSRFApi@index');
    Route::get('events','EventApi@index');
    Route::get('externallink','ExternalLinkApi@index');
    Route::get('faq','FaqApi@index');
    Route::get('footer','FooterApi@index');
    Route::get('forget_password','RegisterApi@forget_password');
    Route::get('getblogcomment','BlogApi@getblogcomment');
    Route::get('menu','Menu@index');
    Route::get('menu/{any}','Menu@index');
    Route::get('news','NewsApi@index');
    Route::get('page','GetPageApi@index');
    Route::get('profile','StudentProfileApi@get');
    Route::get('ratinglist','StudentRatingApi@ratinglist');
    Route::get('reset_password','RegisterApi@reset_password');
    Route::get('streams','UniversityApi@streams');
    Route::get('student_rating','StudentRatingApi@student_rating');
    Route::get('student_activity','StudentProfileApi@student_activity');
    Route::get('studentrating','StudentRatingApi@index');
    Route::get('testimonial','TestimonialApi@index');
    Route::get('university','UniversityApi@index');
    Route::get('university/{id}','UniversityApi@show');
   
    Route::post('applynow','ApplyNowApi@index');
    Route::post('ask-question','FaqApi@askquestion');
    Route::post('blogcomment','BlogApi@blogcomment');
    Route::post('contact-us','ContactUsApi@index');
    Route::post('get_user_details','LoginApi@get_user_details');
    Route::post('image-upload','StudentProfileApi@image_upload');
    Route::post('login','LoginApi@index');
    Route::post('newsletter','NewsletterApi@index');
    Route::post('register','RegisterApi@index');
    Route::post('social-login','LoginApi@social_login');
    Route::post('subscribe',['as'=>'subscribe','uses'=>'MailChimpController@subscribe']);
    Route::post('university/rating','StudentRatingApi@rating');
    Route::post('update_password','RegisterApi@update_password');
    Route::put('change-password','LoginApi@change_password');
    Route::put('interactive-assessment','UniversityApi@interactive_assessment');
    Route::put('profile','StudentProfileApi@update');
   // Route::post('event_detail','EventApi@event_detail');
   // Route::post('get_news','NewsApi@get_news');
   // Route::post('news_comment','NewsApi@news_comment');
   /* Route::get('studentrating','StudentRatingApi@index');//home page list and list page
    Route::post('university/rating','StudentRatingApi@rating');//for submit rating
    Route::get('ratinglist','StudentRatingApi@index');
    Route::get('student_rating','StudentRatingApi@student_rating');*/
});
