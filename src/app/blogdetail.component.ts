import { Component, OnInit, OnDestroy  } from '@angular/core'; 
import { ActivatedRoute,Router } from '@angular/router';

//import { PageService,UserService,AuthenticationService,HeaderMenuService,BlogDetailService } from './service/index';
import { PageService, UserService, AuthenticationService, HeaderMenuService, BlogDetailService, LoadingService } from './service/index';

import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; 

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 		'blog-app',
  	templateUrl: 	'./view/bloginner.html',
})
 
export class BlogDetailComponent {
	blogdetail : any;
  blogcomment : any;
  private sub:any;
  loading = false;
  recentblog:any;
  model: any = {};
  captcha_code=false;
  //for local
  //public recaptchaSiteKey = '6LelxRMUAAAAAD1XdbwUUKPtiksFp03JzqTq7Mso';
   //for live
  public recaptchaSiteKey = '6LfiRxcUAAAAAAft2K9FNiql7I3rIarnhoqAkcx3';
  private onCaptchaComplete(response: any) {
      this.captcha_code=response.success;
      console.log('reCAPTCHA response recieved:');
      console.log(response.success);
      console.log(response.token);
  }

  searchForm : any;

	constructor (
    private _pageService: PageService, 
    private route: ActivatedRoute,           
    private router: Router,
    private _service: NotificationsService,
    private UserService :UserService,
    private authenticationService: AuthenticationService,
    private headerMenuService: HeaderMenuService,
    private loadingService: LoadingService,
    private blogdetailService:BlogDetailService
  ) {

    this.searchForm = new FormGroup({
        search: new FormControl(null),
    });

  }
  ngOnInit(){ 
    this.blogdetailService.blog.subscribe(
      data =>this.blogcomment = data, 
      error => console.log(error),
      () => console.log('str',this.blogcomment)
    );

    this.loadingService.setLoading(true);
    this.sub = this.route.params.subscribe(params => {
      let slug = params['slug'];
      this._pageService.getBlogdetails(slug).subscribe(
        data =>{
          this.loadingService.setLoading(false);
          this.blogdetail = data.data;
          this.blogdetailService.reLoadResult(slug);
        },
        error => this.loadingService.setLoading(false)
      ); 
    });

    this._pageService.getBlog({limit:5}).subscribe(
      data =>this.recentblog = data.data, 
      error => console.log(error),
      () => console.log(this.recentblog)
    );
	}

  saveEditable(value:any, comment_id:any){

    this.loadingService.setLoading(true);
    this.UserService.postcommentblog({comment : value, comment_id : comment_id}).subscribe(
      data => {
        this._pageService.displaySuccessMessage(data, "Comment successfully updated.")
        this.loadingService.setLoading(false);
      },
      error => {
        this._pageService.displayErrorMessage(error);
        this.loadingService.setLoading(false);
      }
    );
  }

  update_search(value:any){
      this.router.navigate(['/blog'], { queryParams: { search: value.search }});
  }

  userCanEdit(blog:any){
    let user=this.authenticationService.userdetail();
    if(user!= undefined && user.user != undefined && user.user.id!=undefined)
      return (blog.user_id == this.authenticationService.userdetail().user.id);
    else
      return false;
  }

  postComment(form:any){
   
    grecaptcha.reset();
    this.model.captcha=this.captcha_code;
    this.model.posts_id=this.blogdetail.id;
    //this.captcha.reset();
   // captcha.reset();
   // this.model.captcha=form.value.captcha.getResponse();
  /*  return false;
    if(this.model.captcha==null)
      this._pageService.displayErrorMessage("Please enter captcha.");*/
   // console.log(this.model);
   // return false;
    this.loadingService.setLoading(true);

    this.UserService.postcommentblog(this.model).subscribe(
      data => {
        this._pageService.displaySuccessMessage(data,"Comment successfully submitted.");
        this.captcha_code=false;
        form.reset();
        this.loadingService.setLoading(false);
        this.blogdetailService.reLoadResult(this.blogdetail.slug);
      },
      error => {
        this.captcha_code=false;
        this._pageService.displayErrorMessage(error);
        this.loadingService.setLoading(false);
      }
    ); 
  }
}