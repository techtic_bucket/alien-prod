import { Component  } from '@angular/core'; 
import { ActivatedRoute, Router  } from '@angular/router';
import { PageService , LoadingService} from './service/index';
import {NotificationsService} from 'angular2-notifications';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 		'eventlist-app',
  	templateUrl: 	'./view/events.html',
	providers: 		[PageService]
})

export class EventsComponent  {
	eventslist:any;
  recentevents:any;
	limit:number=2; 
  fetch_data : any;
  searchForm : any;
  constructor (
    private _pageService: PageService,
    private _service: NotificationsService,
    private loadingService: LoadingService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.fetch_data = {limit : 10, page : 1 };
  }

	ngOnInit(){ 
    let search:any = this.route.snapshot.queryParams['search'] || null;
    if(search != null){
      this.fetch_data.search = search;
    }

    this.getServerData(this.fetch_data); 

    this.searchForm = new FormGroup({
        search: new FormControl(search),
    });

		this.getServerData(this.fetch_data);

    this._pageService.getevents({limit:5, upcomming:true})
      .subscribe(data =>this.recentevents = data.data, 
              error => console.log(error),
              () => console.log(this.recentevents)); 

  }

  paginate(event:any) {
      this.fetch_data.limit = event.rows;
      this.fetch_data.page = (parseInt(event.page) + 1) ;
      this.getServerData(this.fetch_data); 
  }
  update_search(value:any){
    this.router.navigate(['/events'], { queryParams: { search: value.search }});

    if(value.search != null){
      this.fetch_data.search = value.search;
    }

    this.getServerData(this.fetch_data); 
  } 
    
  getServerData(event:any){  

    this.loadingService.setLoading(true);
    this._pageService.getevents(event).subscribe(
      response =>{
        if(response.error) { 
          this._service.error("Error","Server Error");
        } else {
          this.eventslist = response;
        }
        this.loadingService.setLoading(false);
      },
      error =>{
        this._service.error("Error","Server Error");
        this.loadingService.setLoading(false);
      }
    );
    return event;
  }
}