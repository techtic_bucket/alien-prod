import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from './service/page.service';
import { CompareService , LoadingService} from './service/index';
import {RatingModule} from "ng2-rating";
import { Router } from '@angular/router';
import {NotificationsService} from 'angular2-notifications';

@Component({
  selector: 'compare-app',
  templateUrl: './view/compare.html',
	providers: [PageService]
})


export class CompareComponent  {
    compare:any; 
  	constructor (private _pageService: PageService, 
      private compareService: CompareService, 
      private loadingService: LoadingService,
      private router: Router,
      private _service: NotificationsService,
      ) {}

  	ngOnInit(){
      let subscription = this.compareService.university.subscribe(
          value =>  this.getCompareData(value),
          error => true
      );
    }

    getCompareData(obj:any[]){
      let ids:any = [];

      obj.forEach((item, index) => {
        let o:any = { id : item.id };

        if(item.selected_course != undefined && item.selected_course != null && item.selected_course != ""){
          o.course = item.selected_course;
        }
        if(item.selected_stream != undefined && item.selected_stream != null && item.selected_stream != ""){
          o.stream = item.selected_stream;
        }
        ids.push(o);
      });
      this.loadingService.setLoading(true);
      this._pageService.getCompare({universites_id : ids})
        .subscribe(
          data =>this.compare = data.data, 
          error => {
             this._service.error("Error",'Compare university is not found.'); 
              this.router.navigate(['/top-university']);

          },
          ()=>this.loadingService.setLoading(false)
         );
    }

    change_course(id:any, event:any){
      var value = event.currentTarget.value;
      this.compareService.setCourse(id, value);  
    }
    change_stream(id:any, event:any){
      var value = event.currentTarget.value;
      this.compareService.setStream(id, value);
    }

    hasComponent(uni:any, comp:any){
      let meta:any[];
      if(uni.selected_course_data != undefined && uni.selected_course_data != null && uni.selected_course_data != ""){
        meta = uni.selected_course_data.meta;
        return meta.filter(item => item.meta_key !== comp.id).length > 0;
      }else{
        return false;
      }
    }

    valueComponent(uni:any, comp:any){
      let meta:any[];
      if(uni.selected_course_data != undefined && uni.selected_course_data != null && uni.selected_course_data != ""){

        meta = uni.selected_course_data.meta;
        let found:any = meta.filter(item => item.type == 'component' && comp.id == item.meta_key);


        if(found.length > 0){
          return found[0].meta_value;
        }
        return '-';
      }else{
        return '-';
      }
    }

    valueOtherMeta(uni:any){
      let meta:any[] = [];
      if(uni.selected_course_data != undefined && uni.selected_course_data != null && uni.selected_course_data != ""){
        
        if(uni.selected_course_data.meta != undefined){
          meta = uni.selected_course_data.meta;
          meta = meta.filter(item => item.type != 'component');
        }
      }
      return meta;
    }
    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];
}