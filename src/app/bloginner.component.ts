import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from './service/page.service';
@Component({
  selector: 'bloginner-app',
  templateUrl: './view/bloginner.html',
	providers: [PageService]
})


export class BloginnerComponent  {
 bloginner:any;
 recentblog:any;
  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getBlog({id:1})
  			.subscribe(data =>this.bloginner = data.data, 
                error => console.log(error),
                () => ''); 

     
  	}
}