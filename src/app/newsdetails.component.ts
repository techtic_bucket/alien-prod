import { Component  } from '@angular/core'; 
import { ActivatedRoute, Router } from '@angular/router';
import { PageService , LoadingService} from './service/index';
import {NotificationsService} from 'angular2-notifications';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 		'newsdetails-app',
  	templateUrl: 	'./view/newsdetails.html',
	providers: 		[PageService]
})

export class NewsdetailsComponent  {
	newslist:any;
  recentnews:any;
	limit:number=2; 
  searchForm : any;
	private sub:any;
    obj = {limit : this.limit, page : 1 };
  	constructor (
  		private _pageService: PageService,
  		private _service: NotificationsService,
  		private route: ActivatedRoute,
      private loadingService: LoadingService,
      private router: Router
  		) {

      this.searchForm = new FormGroup({
          search: new FormControl(null),
      });
    }

    update_search(value:any){
      this.router.navigate(['/latest-news'], { queryParams: { search: value.search }});
    }
  	ngOnInit(){

			this.sub = this.route.params.subscribe(
        params => {
          this.loadingService.setLoading(true);
          let slug = params['slug'];
          this._pageService.getNews({slug:slug}).subscribe(
            data =>{
              this.newslist = data.data; 
              this.loadingService.setLoading(false);
            }, 
            error => this.loadingService.setLoading(false)
          ); 
        }
      );

      this._pageService.getNews({limit:5}).subscribe(
          data =>this.recentnews = data.data
        );
    }
}
 