import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService, AuthenticationService, HeaderMenuService , LoadingService} from './service/index';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import {NotificationsService} from 'angular2-notifications';

@Component({
  selector: 'askquestion-app',
  templateUrl: './view/askquestion.html',
	providers: [PageService]
})


export class AskQuestionComponent  {
    askquestion:any;
  	constructor (private _pageService: PageService,
                 private loadingService: LoadingService,
                private _service: NotificationsService) {
       this.askquestion = new FormGroup({
            name: new FormControl(null),
            email: new FormControl(null),
            question: new FormControl(null)
          });
    }

  	ngOnInit(){ 
    }
    questionSubmit(askquestion:any){
    
      let value=askquestion.value;
      this.loadingService.setLoading(true);
       
      this._pageService.askQuestionSubmit(value).subscribe(
        data => {
          this.loadingService.setLoading(false);
          if(data.message !== undefined){
              this._service.success("Success", data.message);
          }else{
                this._service.success("Success","Password successfully reset.");
          }
           askquestion.reset();
        },
        error => {
          this.loadingService.setLoading(false);
          var error_data = error.json();
                    if(error_data.message != undefined){
                        if(error_data.message.type == undefined){
                          error_data.message = {
                            type  : null,
                            message : error_data.message
                          };
                        }
       
            this._service.error("Error",error_data.message.message);
          }else{
            this._service.error("Error", "Error while changing password, Please try again.");
          }
        }
      );
    }
}