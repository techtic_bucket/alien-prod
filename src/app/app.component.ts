import { Component, ViewEncapsulation } from '@angular/core'; 
import { PageService , LoadingService} from './service/index';
import {
    Router,
    Event as RouterEvent,
    NavigationStart,
    NavigationEnd,
    NavigationCancel,
    NavigationError
} from '@angular/router';

@Component({
	selector: 'my-app',
	templateUrl: './view/app.component.html',
	styleUrls:  [
        '../assets/css/aap.bootstrap.component.css', 
        '../assets/css/material.css', 
        '../assets/css/app.component.css', 
        '../assets/css/app.media.component.css',
        '../assets/css/glyphicon.css',
    ],
	encapsulation:ViewEncapsulation.None,
})
export class AppComponent  {
	page:any;

	loading: boolean = true;

    constructor(private router: Router, private loadingService:LoadingService) {
    	this.loadingService.setLoading(true);
        router.events.subscribe((event: RouterEvent) => {
            this.navigationInterceptor(event);
        });
    }

	onActivate(e:any, outlet:any){
	    window.scrollTo(0, 0);
	}

	navigationInterceptor(event: RouterEvent): void {
        if (event instanceof NavigationStart) {
            this.loadingService.setLoading(true);
        }
        if (event instanceof NavigationEnd) {
            this.loadingService.setLoading(false);
        }

        if (event instanceof NavigationCancel) {
            this.loadingService.setLoading(false);
        }
        if (event instanceof NavigationError) {
            this.loadingService.setLoading(false);
        }
    }
}

