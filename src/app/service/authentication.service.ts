import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Cons } from './cons'
@Injectable()
export class AuthenticationService {
	cons:any;
	user:any;
    constructor(private http: Http) {  this.cons = Cons.live(); }
 
    login(email: string, password: string) {
        return this.http.post(this.cons.login, { email: email, password: password })
            .map((response: Response) => {
                let user = response.json();

                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
    }

	socialSignIn(data: any) {
        return this.http.post(this.cons.social_login, { data: data })
            .map((response: Response) => {
                let user = response.json();
                if (user && user.token) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
    }

	isLogin(){
		let data = localStorage.getItem('currentUser');
		data = JSON.parse(data);
		if(data != undefined && data != null && data != "") {
            return true;
        }else{
            return false;
        }
	}
 
    logout() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('search_data');
    }

	userdetail(){
		let user:any;
		if(this.isLogin()) {
			user=localStorage.getItem('currentUser')
            user=JSON.parse(user);
            return user;
        }else{
            return false;
        }
	}
}
