import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
 
import { Search } from '../_models/index';
import { Cons } from './cons'
import { PageService } from './page.service';
import { LoadingService } from './loading.service';
 
@Injectable()
export class SearchService {
	cons:any;
	search_data:any;

	university: Observable<any>;
	search_slider_value: Observable<any>;

	private _university: BehaviorSubject<any[]>;
	private _search_slider_value: BehaviorSubject<any[]>;
	private dataStore: {
    	todos: any[],
    	search_slider_value:any,
  	};

    constructor(
		private http: Http, 
		private _pageService : PageService,
		private loadingService : LoadingService
	) { 
		this.search_data = {};

	    this.dataStore = { todos: [], search_slider_value : {} };
	    this._university = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this._search_slider_value = <BehaviorSubject<any>>new BehaviorSubject({});
	   
	    this.university = this._university.asObservable();
	    this.search_slider_value = this._search_slider_value.asObservable();
	}

	//public universities: Observable<List<Todo>> = this._university.asObservable();

	get todos() {
	    return this._university.asObservable();
	}

	loadAll(data:any) {
	    this.dataStore.todos = data;
	    this._university.next(Object.assign({}, this.dataStore).todos);
	}

	setSliderValue(data:any) {
	    this.dataStore.search_slider_value = data;
	    this._search_slider_value.next(Object.assign({}, this.dataStore.search_slider_value));
	}

	reLoadResult(obj?: any) {
		if(obj != undefined && obj != null){
			this.setSearchData(obj);
		}
		this.loadingService.setLoading(true);
		if(this.search_data == null || this.search_data == undefined){
			this.search_data = {};
		}
		let data = this._pageService.getUniversity(this.search_data).subscribe(
			response =>{ 
				this.loadAll(response);
			},
			error => error,
			() => this.loadingService.setLoading(false)
		);
    }

	setSearchData(obj: any) {
        this.search_data = obj;
		if(this.search_data == null || this.search_data == undefined){
			this.search_data = {};
		}
		localStorage.setItem('search_data', JSON.stringify(this.search_data));
    }
	getSearchData() {
		let data = localStorage.getItem('search_data');
		this.search_data = JSON.parse(data);
        return this.search_data;
    }
 
}