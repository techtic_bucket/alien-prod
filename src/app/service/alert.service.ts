import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import {NotificationsService, SimpleNotificationsComponent } from 'angular2-notifications';
@Injectable()
export class AlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;
 
    constructor(private router: Router, 
        private _service: NotificationsService
        ) {
        // clear alert message on route change
        /*router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                } else {
                    // clear alert
                    this.subject.next();
                }
            }
        });*/
    }
 
    success(message: string, keepAfterNavigationChange = false) {
        //this.keepAfterNavigationChange = keepAfterNavigationChange;
        //this.subject.next({ type: 'success', text: message });
        this.create('success', 'Success',message);
    }
 
    error(message: string, keepAfterNavigationChange = false) {
        //this.keepAfterNavigationChange = keepAfterNavigationChange;
        //this.subject.next({ type: 'error', text: message });
        this.create('error', 'Error',message);
    }
 
    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }


    public title: string = 'just a title';
    public content: string = 'just content';
    public type: string = 'success';

    public deleteId: string;

    public temp: boolean[] = [true, false];


    private html = `<p>Test</p><p>A nother test</p>`;


    create(type:any, title:string, content:string) {
        switch (type) {
            case 'success':
                let a = this._service.success(this.title, this.content, {id: 123});
                break;
            case 'alert':
                this._service.alert(this.title, this.content);
                break;
            case 'error':
                this._service.error(this.title, this.content);
                break;
            case 'info':
                this._service.info(this.title, this.content);
                break;
            case 'bare':
                this._service.bare(this.title, this.content);
                break;
        }
    }

    withOverride() { this._service.create('pero', 'peric', 'success', {timeOut: 0, clickToClose: false, maxLength: 3, showProgressBar: true, theClass: 'overrideTest'}) }

    withHtml() {this._service.html(this.html, 'success') }

    removeAll() { this._service.remove() }
}

