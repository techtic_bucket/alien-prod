import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PageService } from './page.service';

@Injectable()
export class UniversityDetailService {
	cons:any;
	search_data:any;
	university: Observable<any>;

	private _university: BehaviorSubject<any[]>;
	private dataStore: {
    	todos: any[]
  	};


    constructor(

		private _pageService : PageService
	) { 
		this.search_data = {};
	    this.dataStore = { todos: [] };
	    this._university = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this.university = this._university.asObservable();
	}

	//public universities: Observable<List<Todo>> = this._university.asObservable();

	get todos() {
	    return this._university.asObservable();
	}

	loadAll(data:any) {
	    this.dataStore.todos = data;
	    this._university.next(Object.assign({}, this.dataStore.todos));
	}

	reLoadResult(id:any) {
  		this._pageService.getUniversityrating({limit:5,slug:id})
        .subscribe(data =>this.loadAll(data), 
                error => console.log(error),
                () => ''); 
    }

	isComment(){
		
	}
}
