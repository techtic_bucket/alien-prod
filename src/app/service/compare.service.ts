import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class CompareService {
	universities:any[];

	university: Observable<any>;

	private _university: BehaviorSubject<any[]>;
	private dataStore: {
    	todos: any[]
  	};
	//public universities: Observable<List<Todo>> = this._university.asObservable();

	constructor(private http: Http) {
	    this.dataStore = { todos: [] };
	    this._university = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this.university = this._university.asObservable();
	}

	get todos() {
	    return this._university.asObservable();
	}

	loadAll() {
		let data:any;
	    let session = localStorage.getItem('compare_university');
		if(session != undefined && session != null && session != ""){
			data =  JSON.parse(session);
		}else{
			data = [];
		}

	    this.dataStore.todos = data;
	    this._university.next(Object.assign({}, this.dataStore).todos);
	}

	load(obj:any) {
	    let notFound = true;
	    this.dataStore.todos.forEach((item, index) => {
	        if (item.id === obj.id) {
	          this.dataStore.todos[index] = obj;
	          notFound = false;
	        }
	    });

	    if (notFound) {
	        this.dataStore.todos.push(obj);
	    }
	    this._university.next(Object.assign({}, this.dataStore).todos);
	}

	update(obj_id:any, updates:any[]) {
        this.dataStore.todos.forEach((t, i) => {

          if (t.id === obj_id) { 
          	updates.forEach((item, index) => {
		        t[item.key] = item.value;
		    });
          	this.dataStore.todos[i] = t; 
          }

        });
        this._university.next(Object.assign({}, this.dataStore).todos); 
        this.updateSession();
	}


	isFound(obj:any){
		let notFound = true;
	    this.dataStore.todos.forEach((item, index) => {
	        if (item.id === obj.id) {
	          notFound = false;
	        }
	    });
	    return !notFound
	}

	isLimit(){
		let count:number = 0;
	    this.dataStore.todos.forEach((item, index) => {
			count = count + 1;
	    });
	    return count >= 3;
	}

	remove(obj: any) {
	    this.dataStore.todos.forEach((t, i) => {
	        if (t.id === obj.id) { this.dataStore.todos.splice(i, 1); }
	    });
	    this._university.next(Object.assign({}, this.dataStore).todos);
	}



	loadFromSession(){
		this.loadAll();
	}

	updateSession(){
		localStorage.setItem('compare_university', JSON.stringify(this.dataStore.todos));
	}

	addToCompare(item:any) {
		this.load(item);
        this.updateSession();
    }
	removeFromCompare(item:any) {
		this.remove(item);
        this.updateSession();
    }

	getCompare() {
       return  this.dataStore.todos;
    }

    setCourse(university_id:any, id:any){
    	this.update(university_id, [{ key : 'selected_course', value : id}]);
    }
	setStream(university_id:any, id:any){
    	this.update(university_id, [{ key : 'selected_stream', value : id}]);
	}
}