import { Injectable }    from '@angular/core';
import { Jsonp, URLSearchParams,Http, Headers, RequestOptions,Response } from '@angular/http'; 

import { Cons } from './cons'
import 'rxjs/add/operator/map';
import { RatingNow, University,UniversityRating } from '../_models/index';

import { NotificationsService } from 'angular2-notifications';
import { ActivatedRoute,Router } from '@angular/router';

import { AuthenticationService, HeaderMenuService } from './index';

@Injectable()
export class PageService {
  htmlContent:string;
  cons:any;
  server_type:any;
  headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: Http,   
    private router: Router,
    private _service: NotificationsService,
    private authenticationService: AuthenticationService,
    private headerMenuService: HeaderMenuService,
    ) {
    this.cons = Cons.live();
    this.server_type = 'live';
  }
  
  getHeader(){ 
    return this.getUrl(this.cons.header_menu);
  }

  getUniversity(obj?:any){
    return this.getUrl(this.cons.university, obj); 
  }

  getComponent(obj?:any){
    return this.getUrl(this.cons.component, obj); 
  }

  getCourses(obj?:any){
    return this.getUrl(this.cons.courses, obj); 
  }

  getStreams(obj?:any){
    return this.getUrl(this.cons.streams, obj); 
  }

  getUniversitydetails(id?:any){ 
    var url:any;
    if(id != undefined){
      url =this.cons.university+"/"+id;
    }else{
      url =this.cons.university;
    }
    return this.getUrl(url); 
  }

  getCompare(ids?:any){ 
    return this.getUrl(this.cons.compare, ids);
  }

  getNews(obj?:any){ 
    return this.getUrl(this.cons.news, obj);
  }
  getevents(obj?:any){ 
    return this.getUrl(this.cons.events, obj);
  }

  getFAQ(obj?:any){ 
    return this.getUrl(this.cons.faq, obj);
  }

  getTestimonial(obj?:any){
    return this.getUrl(this.cons.testimonial, obj);
  }

  getBlog(obj?:any){
    return this.getUrl(this.cons.blog, obj);
  }

  getBlogdetails(id?:any){ 
    var url:any;
    if(id != undefined){
      url =this.cons.blog+"/"+id;
    }else{
      url =this.cons.blog;
    }
    return this.getUrl(url); 
  }
  getpage(obj?:any){
    return this.getUrl(this.cons.getpage, obj);
  }
  getBlogComment(obj?:any){
    return this.getUrl(this.cons.blogcomment, obj);
  }
  getStudentRating(obj?:any){
    return this.getUrl(this.cons.studentrating, obj);
  }
  getUniversityrating(obj?:any){
   // return this.http.post(this.cons.ratinglist, universityRating).map((response: Response) => response.json());
   /*var url:any;
   url=this.cons.ratinglist+"/"+universityRating;
*/    return this.getUrl(this.cons.ratinglist,obj);
  }
 
  /*getUniversityrating(universityRating:UniversityRating){
    return this.http.post(this.cons.ratinglist, universityRating).map((response: Response) => response.json());
  }
*/
  getFooter(){    
    return this.getUrl(this.cons.footer);
  }

  private clone(object: any){
    return JSON.parse(JSON.stringify(object));
  }

 applyNow(university: University){
   return this.http.post(this.cons.applynow, university).map((response: Response) => response.json());
  }
 forgetPassword(obj?:any){
   return this.getUrl(this.cons.forgotpassword, obj);
 }
 resetPassword(obj?:any){
   
   return this.getUrl(this.cons.resetpassword, obj);
 }
 submitContactUs(obj: any){
   return this.http.post(this.cons.contact_us, obj).map((response: Response) => response.json());
 }
 reset_password(user: any){
   return this.http.post(this.cons.updatepassword, user).map((response: Response) => response.json());
 }
 askQuestionSubmit(question:any){
   return this.http.post(this.cons.askquestion, question).map((response: Response) => response.json());
 }
  /*ratingnow(ratingNow: RatingNow){
   
   return this.http.post(this.cons.ratingnow, ratingNow).map((response: Response) => response.json());
  }*/
  getUrl(url:any, data?:any) {
    let params = new URLSearchParams();
    /*if(data != undefined){
      for(let key in data) {
          params.set(key, data[key]);
      }
    }*/
    let options = new RequestOptions({
        search: params
    });

    if(data != undefined && data != null && this.isJsObject(data))
      url = url +"?"+ this.objectToParams(data);

    return this.http.get(url, options)
      .map(response => response.json());
  }

  objectToParams(object:any):string{
      return Object.keys(object).map((key) => 
      {
        if(this.isJsObject(object[key])){
          return this.subObjectToParams(encodeURIComponent(key), object[key]) 
        }else{
          if(object[key] != null)
            return `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`;
          else
            return `${encodeURIComponent(key)}=`;
        }
      }
      ).join('&');
  }

  subObjectToParams(key:any,object:any) :string {
    return Object.keys(object).map((childKey) => {
        if(this.isJsObject(object[childKey]))
          return this.subObjectToParams(`${key}[${encodeURIComponent(childKey)}]`, object[childKey]);
        else
          return `${key}[${encodeURIComponent(childKey)}]=${encodeURIComponent(object[childKey])}`;
      }
    ).join('&');
  }

  isJsObject(object:any){
      return (typeof object == 'object' || typeof object == 'Array') && object != null;
  }

  displaySuccessMessage(data:any, message?:any){
    if(data.message !== undefined){
        this._service.success("Success", data.message);
    }else{
        this._service.success("Success",message);
    }
  }
  displayErrorMessage(error:any, message?:any){
    var error_data = error.json();
    if(error_data.message != undefined){
      if(error_data.message.type == undefined){
        error_data.message = {
          type  : null,
          message : error_data.message
        };
      }
      switch (error_data.message.type) {
        case "expired":
          this.headerMenuService.setIsLogin(false);
          this.authenticationService.logout();
          this._service.error("Error",error_data.message.message);
          this.router.navigate(['/login']);
          break;

        case "invalid":
          this._service.error("Error",error_data.message.message);
          break;
        
        default:
          this._service.error("Error",error_data.message.message);
          break;
      }
    }else{
      this._service.error("Error", message);
    }
  }
}
