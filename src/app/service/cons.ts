export class Cons {
  public static live_baseUrl = '../web-api/public/index.php/api/';
  public static local_baseUrl = '/app/service/json/';
  //public static local_baseUrl = 'http://localhost/alien/web-api/public/index.php/api/';
  
  static test(){
  	return 234234234;
  }

  static live(){
  	return { 
      'applynow':this.live_baseUrl+'applynow',
      'askquestion':this.live_baseUrl+'ask-question',
      
      'blog':this.live_baseUrl+'blog',
      'blogcomment':this.live_baseUrl+'getblogcomment',
      'bloginner':this.live_baseUrl+'blog',
      
      'change_password':this.live_baseUrl+'change-password',
      'compare':this.live_baseUrl+'compare',
      'component':this.live_baseUrl+'components',
      'contact_us':this.live_baseUrl+'contact-us',
      'courses':this.live_baseUrl+'courses',
      
      'events':this.live_baseUrl+'events',
      'faq':this.live_baseUrl+'faq',
      'footer':this.live_baseUrl+'footer',
      'forgotpassword':this.live_baseUrl+'forget_password',
      
      'getpage':this.live_baseUrl+'page',
      'getstudent_rating':this.live_baseUrl+'student_rating',
      'getstudent_activity':this.live_baseUrl+'student_activity',
      'header_menu':this.live_baseUrl+'menu/header',
      
      'image_upload':this.live_baseUrl+'image-upload',
      'login':this.live_baseUrl+'login',
      'news':this.live_baseUrl+'news',
      
      'postblogcomment':this.live_baseUrl+'blogcomment',
      'profile':this.live_baseUrl+'profile',
      
      'ratinglist':this.live_baseUrl+'ratinglist',
      'ratingnow':this.live_baseUrl+'university/rating',
      'register':this.live_baseUrl+'register',
      'resetpassword':this.live_baseUrl+'reset_password',
      
      'social_login':this.live_baseUrl+'social-login',
      'streams':this.live_baseUrl+'streams',
      'studentrating':this.live_baseUrl+'studentrating',
      'subscribe':this.live_baseUrl+'subscribe',
      'interactive_assessment':this.live_baseUrl+'interactive-assessment',
      
      'testimonial':this.live_baseUrl+'testimonial',
      'university':this.live_baseUrl+'university',
      'universitydetails':this.live_baseUrl+'universitydetails',
      'updatepassword':this.live_baseUrl+'update_password',
  	};
  }

}
