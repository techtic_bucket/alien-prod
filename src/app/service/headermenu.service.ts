import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class HeaderMenuService {
	header: Observable<any>;
	
	private _header: BehaviorSubject<any>;
	private data: {
    	is_login: boolean
  	};

	constructor(private http: Http) {
	    this.data = { is_login: false };
	    this._header = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this.header = this._header.asObservable();
	}

	get is_login() {
	    return this._header.asObservable();
	}

	setIsLogin(is_login:boolean) {
        this.data.is_login = is_login;
        this._header.next(Object.assign({}, this.data))
    }
	
}