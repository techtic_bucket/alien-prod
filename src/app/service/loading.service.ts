import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class LoadingService {
	loading: Observable<any>;
	
	private _loading: BehaviorSubject<any>;
	private data: {
    	is_loading: boolean,
  		count:number
  	};

	constructor(private http: Http) {
	    this.data = { is_loading: false, count:0 };
	    this._loading = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this.loading = this._loading.asObservable();
	}

	get is_loading() {
	    return this._loading.asObservable();
	}

	setLoading(is_loading:boolean) {
		if(is_loading == true){
			this.data.count = this.data.count + 1;
		}else{
			this.data.count = this.data.count - 1;
			if(this.data.count <= 0){
				this.data.count = 0;
			}
		}
		/*if(this.data.count == 0){
			this.data.is_loading = true;
		}else{
			this.data.is_loading = false;
		}*/
        this.data.is_loading = is_loading;
        this._loading.next(Object.assign({}, this.data))
    }
	
}