import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
//import { User,RatingNow,PostComment } from '../_models/index';
import { User,RatingNow,PostComment } from '../_models/index';

import { Cons } from './cons'
 
@Injectable()
export class UserService {
	cons:any;
    constructor(private http: Http) { this.cons = Cons.live(); }
 
    Profile() {
		let url = this.getTokenUrl(this.cons.profile);
        return this.http.get(url, this.jwt()).map((response: Response) => response.json());
    }
 
    getById(id: number) {
		let url = this.getTokenUrl(this.cons.profile+'/' + id);
        return this.http.get(url, this.jwt()).map((response: Response) => response.json());
    }
 
    create(user: User) {
		let url = this.getTokenUrl(this.cons.register);
        return this.http.post(url, user, this.jwt()).map((response: Response) => response.json());
    }
 
    update(user: any) {
		let url:string;
		//let header = {'Content-Type' : 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2)};
		//let header = {'Content-Type' : 'multipart/form-data', 'Accept': 'application/json'};

		//return this.http.put(this.getTokenUrl(this.cons.profile), user, this.jwt()).map((response: Response) => response.json());
		url = this.getTokenUrl(this.cons.profile);
        return this.http.put(url, user, this.jwt()).map((response: Response) => response.json());
    }
	updateProfile(user: any, image_only?:boolean) {
		let header:any;
		if(user != undefined){
			
			//let header = {'Content-Type' : 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2)};
			if(image_only){
				header = {'Content-Type' : 'multipart/form-data', 'Accept': 'application/json'};
			}
		}
		let url = this.getTokenUrl(this.cons.profile);
        return this.http.put(url, user, this.jwt(header)).map((response: Response) => response.json());
    }

	getInteractiveAssessment() {
		let url = this.getTokenUrl(this.cons.interactive_assessment);
        return this.http.get(url, this.jwt()).map((response: Response) => response.json());
    }
 
	updateEducation(data: any){
		let url = this.getTokenUrl(this.cons.profile+"?education_update=1");
        return this.http.put(url, data, this.jwt()).map((response: Response) => response.json());
    }
 
	chnage_password(user: any) {
		let url = this.getTokenUrl(this.cons.change_password);
		return this.http.put(url, user, this.jwt()).map((response: Response) => {
			response.json();
		});
    }
 
    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }
	submitRating(ratingNow: RatingNow){
		
		let url = this.getTokenUrl(this.cons.ratingnow);

		return this.http.post(url, ratingNow,this.jwt()).map((response: Response) => response.json());
	}
 	postcommentblog(postcomment:any){
		
		let url = this.getTokenUrl(this.cons.postblogcomment);

		return this.http.post(url, postcomment,this.jwt()).map((response: Response) => response.json());
	}
	getStudentRating(obj?:any){
   		
        //return this.http.get(this.cons.getstudent_rating, this.jwt()).map((response: Response) => response.json());
		return this.getUrl(this.cons.getstudent_rating, obj);
  	}
  	getStudentActivity(obj?:any){
  		return this.getUrl(this.cons.getstudent_activity, obj);
  	}
	updateUser(data:any) {
        if(data == undefined){

			let url = this.getTokenUrl(this.cons.profile);
			return this.http.get(url, this.jwt())
			.map((response: Response) => {
				let user = response.json();
				if (user && user.token) {
					localStorage.setItem('currentUser', JSON.stringify(user));
				}
			});
		}else{
			if (data && data.token) {
				localStorage.setItem('currentUser', JSON.stringify(data));
			}else{
				let new_data:any = localStorage.getItem('currentUser');
				new_data = JSON.parse(new_data);
				new_data.user = data;
				localStorage.setItem('currentUser', JSON.stringify(new_data));
			}
		}
    }
 
    // private helper methods
 
    private jwt(headers?:any) {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if(headers == undefined){
			headers = {};
		}
        if (currentUser && currentUser.token) {
            headers['Authorization'] = 'Bearer ' + currentUser.token;
        }

		let _headers = new Headers(headers);
        return new RequestOptions({ headers: _headers });

    }
	getUrl(url:any, data?:any) {
	    if(url.indexOf('?') >= 0){
			url = url + "&";
		}else{
			url = url + "?";
		}
	    
	    if(data != undefined && data != null)
	      url = url + this.objectToParams(data);

	  	url = this.getTokenUrl(url);
	    return this.http.get(url, this.jwt())
	      .map(response => response.json());
	  }

	  objectToParams(object:any):string{
	      return Object.keys(object).map((key) => 
	        this.isJsObject(object[key]) ? this.subObjectToParams(encodeURIComponent(key), object[key]) : `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`
	      ).join('&');
	  }

	  subObjectToParams(key:any,object:any) :string {
	    return Object.keys(object).map((childKey) => this.isJsObject(object[childKey]) ?
	        this.subObjectToParams(`${key}[${encodeURIComponent(childKey)}]`, object[childKey]) : `${key}[${encodeURIComponent(childKey)}]=${encodeURIComponent(object[childKey])}`
	    ).join('&');
	  }

	  isJsObject(object:any){
	      return typeof object == 'object' || typeof object == 'Array';
	  }

	getToken(){
	  	let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser && currentUser.token) {
           return currentUser.token;
        }else{
			return false;
		}
	}
	getTokenUrl(url:string){
		if(url.indexOf('?') >= 0){
			url = url + "&";
		}else{
			url = url + "?";
		}
		if(this.getToken()){
			return url + 'token='+ this.getToken();
		}else{
			return url;
		}
		
	}
}
