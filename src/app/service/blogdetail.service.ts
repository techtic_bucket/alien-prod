import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PageService } from './page.service';

@Injectable()
export class BlogDetailService {
	cons:any;
	search_data:any;
	blog: Observable<any>;

	private _blog: BehaviorSubject<any[]>;
	private dataStore: {
    	todos: any[]
  	};


    constructor(

		private _pageService : PageService
	) { 
		this.search_data = {};
	    this.dataStore = { todos: [] };
	    this._blog = <BehaviorSubject<any[]>>new BehaviorSubject([]);
	    this.blog = this._blog.asObservable();
	}

	//public universities: Observable<List<Todo>> = this._blog.asObservable();

	get todos() {
	    return this._blog.asObservable();
	}

	loadAll(data:any) {
		console.log(data);
	    this.dataStore.todos = data.data;
	    console.log('string');
	    this._blog.next(Object.assign({}, this.dataStore).todos);
	}

	reLoadResult(id:any) {
		console.log(id);
  		this._pageService.getBlogComment({slug:id})
        .subscribe(data =>this.loadAll(data), 
                error => console.log(error),
                () => ''); 
    }

}
