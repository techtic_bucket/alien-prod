import { Component  } from '@angular/core'; 
import { ActivatedRoute, Router } from '@angular/router';
import { PageService , LoadingService} from './service/index';
import {NotificationsService} from 'angular2-notifications';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 		'eventsdetail-app',
  	templateUrl: 	'./view/eventsdetail.html',
	providers: 		[PageService]
})

export class EventsdetailComponent  {
	eventsdetail:any;
  recentevents:any; 
  searchForm : any;
	private sub:any;
  	constructor (
  		private _pageService: PageService,
  		private _service: NotificationsService,
  		private route: ActivatedRoute,
      private router: Router,
      private loadingService: LoadingService
  		) {}

  	ngOnInit(){
        this.searchForm = new FormGroup({
            search: new FormControl(null),
        });

        this.loadingService.setLoading(true);
  			this.sub = this.route.params.subscribe(params => {
            let slug = params['slug'];
            this._pageService.getevents({slug:slug}).subscribe(
              data =>{
                this.eventsdetail = data.data;
                this.loadingService.setLoading(false);
              }, 
              error => console.log(error)
            ); 
        });

        this._pageService.getevents({limit:5,upcomming:true}).subscribe(
          data =>this.recentevents = data.data,
          error => console.log(error)
        ); 
  	}

    update_search(value:any){
      this.router.navigate(['/events'], { queryParams: { search: value.search }});
    }
}
 