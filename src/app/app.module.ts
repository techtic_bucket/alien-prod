import { NgModule, Compiler } from '@angular/core'; 

import { BrowserModule } from '@angular/platform-browser'; 
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { ModuleWithProviders } from '@angular/core'; 
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule}    from '@angular/forms';
import { AppComponent }  from './app.component';
import { HttpModule, JsonpModule, BaseRequestOptions } from '@angular/http';


import {MaterialModule} from '@angular/material';
import '../../node_modules/hammerjs/hammer.js';

import {FileUploadModule, PaginatorModule, SliderModule} from 'primeng/primeng';

import {InlineEditorModule} from './ng2-inline-editor/index';
import {MailChimpModule} from './mailchimp/index';

//import { DataTableModule } from 'angular-2-data-table';
import { DataTableModule } from './angular-2-data-table/src/index';

import { routing } from "./app.routes";
import { HomeComponent} from"./home.component"
import { LoadingComponent}  from './includes/loading.component';
import { HeaderComponent }  from './includes/header.component';
import { InnerbannerComponent}  from './includes/innerbanner.component';
import { MyAccountSidebarComponent }  from './includes/my-account-siderbar.component';
import { FooterComponent, CompareUniversitiesComponent }  from './includes/footer.component'; 
import { BannerComponent }  from './includes/home/banner.component';
import { TopuniversitiesComponent }  from './includes/home/topuniversities.component';
import { LatestnewsComponent}  from './includes/home/latestnews.component';
import { StudentratingComponent}  from './includes/home/studentrating.component';
import { RecentBlogComponent}  from './includes/home/recentblog.component';
import { AboutComponent } from "./about.component";
import { TestimonialsComponent } from "./includes/home/testimonials.component";
import { Coursescomponent } from"./courses-component"
import { PageComponent } from"./page.component";
import { BlogComponent } from "./blog.component"; 
import { BlogDetailComponent } from "./blogdetail.component"; 

/*import { PageNotFoundComponent }  from './not-found.component';
*/
import { TestimonialpageComponent} from "./testimonialpage.component";
import { LoginregisterComponent, LogoutComponent} from "./loginregister.component"; 
import { LatestnewslistComponent } from "./latestnewslist.component";
import { NewsdetailsComponent } from "./newsdetails.component";
import { EventsComponent } from "./events.component";
import { EventsdetailComponent } from "./eventsdetail.component";
import { UniversityComponent} from "./university.component"; 
import { CompareComponent} from "./compare.component"; 
import { StudentratingpageComponent} from "./studentratingpage.component"; 
import { BloginnerComponent} from "./bloginner.component";
import { AnswerquestionsComponent} from "./answerquestions.component";
import { UniversitydetailsComponent} from "./universitydetails.component";  
import { ForgotComponent, ResetPasswordComponent } from "./forgot.component";
import { AskQuestionComponent } from "./askquestion.component"; 
import {MyAccountComponent, MyAccountChanagePasswordComponent, MyAccountRatingComponent, MyAccountProfileEditComponent, MyAccountEditProfileComponent, ActivityComponent, EducationComponent, InteractiveAssessmentComponent} from "./my-account.component";  
import {SearchComponent} from "./search.component";  
import {ContactUsComponent} from "./contact-us-component"; 

import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService, HeaderMenuService, CompareService, SearchService, UniversityDetailService, BlogDetailService, PageService, LoadingService} from './service/index';


import { HtmlStripTagsPipe, TrimTextPipe, ImagePipe, CapitalizePipe, GenderLablePipe, DatetotimePipe, Nl2BrPipe,SafeUrlPipe, MetaNamePipe } from './pipe/common.pipe';

import {TestComponent} from "./test.component";
import {Ng2PaginationModule} from 'ng2-pagination';

import { Ng2GoogleRecaptchaModule }  from 'ng2-google-recaptcha';
/*import * as jQuery from 'jquery';*/
import {GlobalEventsManager} from "./includes/globals";
import {Ng2PageScrollModule} from 'ng2-page-scroll/ng2-page-scroll';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {Angular2SocialLoginModule } from "angular2-social-login";

import {RatingModule} from "ng2-rating";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



let providers_local = {
    "google": {
      "clientId": "818512482560-64njqrkas0v5j8pdod1fko2smtfjgoqf.apps.googleusercontent.com",
    },
    "facebook": {
      "clientId": "593294130868995",
      "apiVersion": "v2.4"
    }
  };

let providers_vps = {
    "google": {
      "clientId": "370131893303-mjf3u02di4nvfo65ug6l4o7niq1db5f5.apps.googleusercontent.com",
    },
    "facebook": {
      "clientId": "1854496681497442",
      "apiVersion": "v2.4"
    }
  };


let mailchimp =  {
  apiKey : '0a59cd086055786c8d444e13fa18dd21-us15'
};
 
let providers = providers_vps;
 /*'facebook' => [
        'client_id' => '480315068823632',
        'client_secret' => '3ff9b770ca20eaae7c733ecb48bd4f8a',
        'redirect' => 'http://new.lookingupli.com/social/fb_return',
    ],

    'twitter' => [
        'client_id' => 'ueYgNf65aItbXN1tiajOw0VQa',
        'client_secret' => 'KwUptAxAMOJEqng9pV6HfIyd0Ekg6GziRQ7oEfSi7sbD7uLfvn',
        'redirect' => 'http://new.lookingupli.com/social/twitter_return',
    ],

    'google' => [
        'client_id' => '119220010943-rcd764ac64juvh2ddsj7hm3u2b9tr3jl.apps.googleusercontent.com',
        'client_secret' => 'Yi0XRt2ksdD6zKpC2bZAlbG8',
        'redirect' => 'http://new.lookingupli.com/social/google_return',
    ],*/


@NgModule({
  imports:      [
      DataTableModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      JsonpModule,
      Ng2PaginationModule,
      routing,
      Ng2GoogleRecaptchaModule,
      Ng2PageScrollModule.forRoot(),
      SimpleNotificationsModule.forRoot(),
      MailChimpModule,
      NgbModule.forRoot(),
      RatingModule,
      FileUploadModule,
      PaginatorModule,
      SliderModule,
      InlineEditorModule,
      MaterialModule
], 
  declarations: [ 
      AppComponent,
      LoadingComponent,
      TestimonialpageComponent,
      LoginregisterComponent,
      HeaderComponent,
      FooterComponent,
      CompareUniversitiesComponent,
      BannerComponent,
      InnerbannerComponent,
      MyAccountSidebarComponent,
      TopuniversitiesComponent,
      LatestnewsComponent,
      StudentratingComponent,
      RecentBlogComponent,
      AboutComponent,
      Coursescomponent,
      HomeComponent,
      PageComponent,
      BlogComponent,BlogDetailComponent,
      TestimonialsComponent,
      UniversityComponent,
      CompareComponent,
      UniversitydetailsComponent,
      BloginnerComponent,
      AnswerquestionsComponent,
      StudentratingpageComponent,
      LatestnewslistComponent,
      NewsdetailsComponent,
      EventsComponent,
      EventsdetailComponent,

      MyAccountComponent,
      MyAccountChanagePasswordComponent, 
      MyAccountRatingComponent, 
      MyAccountProfileEditComponent,
      MyAccountEditProfileComponent,
      EducationComponent,
      InteractiveAssessmentComponent,
      ActivityComponent,

      TestComponent,
      LogoutComponent,
      ForgotComponent,
      ResetPasswordComponent,
      SearchComponent,
      AskQuestionComponent,
      ContactUsComponent,
      
      HtmlStripTagsPipe,
      TrimTextPipe,
      ImagePipe,
      CapitalizePipe,
      GenderLablePipe,
      DatetotimePipe,
      SafeUrlPipe,
      MetaNamePipe,
      Nl2BrPipe
  ], 
  bootstrap: [
      AppComponent,
  ],
  providers: [
      AlertService, 
      AuthenticationService, 
      AuthGuard, 
      UserService,
      HeaderMenuService,
      CompareService,
      BaseRequestOptions,
      GlobalEventsManager,
      SearchService,
      UniversityDetailService,
      BlogDetailService,
      PageService,
      LoadingService,
      {provide: LocationStrategy, useClass: HashLocationStrategy}
  ]
})
export class AppModule { 

  constructor (private _compiler: Compiler, private _headerMenuService: HeaderMenuService, private authenticationService: AuthenticationService, private _compareService:CompareService) {
    var is_login = this.authenticationService.isLogin();

    _compareService.loadFromSession();


    this._headerMenuService.setIsLogin(is_login);
    this._compiler.clearCache();
  }
  
} 
