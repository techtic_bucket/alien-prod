import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import {RatingModule} from "ng2-rating";
import { PageService , LoadingService} from './service/index';
import {NotificationsService} from 'angular2-notifications';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
    templateUrl:  './view/contact-us.html',
   
})
export class ContactUsComponent {
	ContactForm : any;
	data : any;
	constructor (
	    private _pageService: PageService,
	    private _service: NotificationsService,
	    private loadingService: LoadingService,
	) {
	  	this.ContactForm = new FormGroup({
            first_name: new FormControl(null),
            last_name: new FormControl(null),
            email: new FormControl(null),
            phone: new FormControl(null),
            message: new FormControl(null)
        });
	}

	ngOnInit(){ 
  		this._pageService.getFooter()
  			.subscribe(
            data => this.data =  data.data, 
            error => console.log(error)
        ); 

  	}

	send_mail(value:any, form?:any){
      	this.loadingService.setLoading(true);
      	this._pageService.submitContactUs(value).subscribe(
	        data => {
	          	this._service.success("Success", 'Your email successfully send.');
	          	this.loadingService.setLoading(false);
	          	if(form != undefined)
	         		form.reset();
	        },
	        error => {
	        	let er = error.json();
	          	this.loadingService.setLoading(false);
	          	if(er.message != undefined){
	          		this._service.error("Error", er.message);
	          	}else{
	          		this._service.error("Error", 'Error while sending email. Please try again.');
	          	}
	        }
	    )
	}
}