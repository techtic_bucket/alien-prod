export class User {
    id: number= null;
    name: string= null;
    email: string= null;
    password: string= null;
	confirm_password: string= null;
    address: string= null;
    country: string= null;
    state: string= null;
    zip_code: string= null;
    qualification: string= null;
    
    remember: string= null;
    login_email: string = null;
    login_password: string= null;
}