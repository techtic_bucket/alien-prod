export class University {
    name: string;
    email: string;
    mobile: string;
    comment: string;
    university_id: string;
    user_id:string;
}