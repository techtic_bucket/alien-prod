import { Component, OnInit, OnDestroy  } from '@angular/core'; 
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { CompareService , LoadingService, SearchService, PageService} from './service/index';

import {NotificationsService} from 'angular2-notifications';

import { Search } from './_models/index';
import { Router } from '@angular/router';

@Component({
  selector: 'university-app',
  templateUrl: './view/university.html',
	providers: [PageService]
})


export class UniversityComponent  {
    university:any; 
    compare_list:any; 
    totalItem:number;
    model: any = {};

  	constructor (
      private _pageService: PageService, 
      private compareService: CompareService,
      private loadingService: LoadingService,
      private _service: NotificationsService,
      private _searchService : SearchService,
      private router: Router,
      ) {
      this.getServerData({limit : 12, page : 1 }); 
      //this.compare_list = this.compareService.getCompare();

      let subscription = this.compareService.university.subscribe(
          value => this.compare_list = value,
          error => true
    
      );
    }

    private search() : void {
        this._searchService.setSearchData(this.model);
        this.router.navigate(['/search']);
    }

    paginate(event:any) {
        //event.first = Index of the first record
        //event.rows = Number of rows to display in new page
        //event.page = Index of the new page
        //event.pageCount = Total number of pages
        //this.obj.limit = event.rows;
        //this.obj.page = event.page;
        this.getServerData({limit : event.rows, page : (parseInt(event.page) + 1) }); 
    }

    isFound(obj:any){
      return this.compareService.isFound(obj);
    }

    isLimit(){
      return this.compareService.isLimit();
    }


    showLimitMessage(obj?:any){
      if(obj == undefined || obj == null){
        obj.id = 0;
      }
      if(this.isLimit() && !this.isFound(obj)){
        this._service.error("Error","You have already selected 3 universities. You can compare maximum 3 universities.");
      }
    }



    onChange(obj:any, event:any) {
      var isChecked = event.currentTarget.checked;
      if(isChecked){
        this.compareService.addToCompare(obj);
      }else{
        this.compareService.removeFromCompare(obj);
      }
    }
    getServerData(obj:any){  

      this.loadingService.setLoading(true);
      this._pageService.getUniversity(obj).subscribe(
        response =>{
          if(response.error) { 
            this._service.error("Error","Server Error");
          } else {
            this.university = response;
          }
        },
        error =>{
          this._service.error("Error","Server Error");
        },
        () => this.loadingService.setLoading(false)
      );
    return event;
  }

}