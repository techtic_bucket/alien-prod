import { NgModule, Component, ViewChild } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from './service/page.service';
import {AlertService, AuthenticationService, UniversityDetailService, UserService, HeaderMenuService, LoadingService, CompareService} from './service/index';
import { ActivatedRoute,Router } from '@angular/router';
import {NotificationsService} from 'angular2-notifications';
import {RatingModule} from "ng2-rating";
import { DataTable, DataTableTranslations, DataTableResource } from './angular-2-data-table/src/index';
const style = ` 
  :host /deep/ .index-column,
  :host /deep/ .index-column-header {
      text-align: right;
  }

  :host /deep/ .data-table .data-table-row.selected {
      background-color: #E4EDF9;
  }
  .data-table-header[_ngcontent-mjl-6] {
      min-height: 25px;
      margin-bottom: 10px;
  }
  .title[_ngcontent-mjl-6] {
      display: inline-block;
      margin: 5px 0 0 5px;
  }
  .button-panel[_ngcontent-mjl-6] {
      float: right;
  }
  .button-panel[_ngcontent-mjl-6]   button[_ngcontent-mjl-6] {
      outline: none !important;
  }

  .column-selector-wrapper[_ngcontent-mjl-6] {
      position: relative;
  }
  .column-selector-box[_ngcontent-mjl-6] {
      box-shadow: 0 0 10px lightgray;
      width: 150px;
      padding: 10px;
      position: absolute;
      right: 0;
      top: 1px;
      z-index: 1060;
  }
  .column-selector-box[_ngcontent-mjl-6]   .checkbox[_ngcontent-mjl-6] {
      margin-bottom: 4px;
  }
  .column-selector-fixed-column[_ngcontent-mjl-6] {
      font-style: italic;
  }
  `;
@Component({
  selector: 'universitydetails-app',
  templateUrl: './view/universitydetails.html',
  styles: [style],
	providers: [PageService,UniversityDetailService],
})



export class UniversitydetailsComponent  {
private overStar:number;
private ratingPercent:number;
 private maxRateValue:number = 5;
  starsCount: 5;
  Islogin:any;
  university:any;
  universityrating:any;
  model: any = {};
  user:any;
  loading = false;

  
  translations = <DataTableTranslations>{
      indexColumn: 'Index column',
      expandColumn: 'Expand column',
      selectColumn: 'Select column',
      paginationLimit: 'Max results',
      paginationRange: 'Result range'
  };
  	constructor (
      private _pageService: PageService, 
      private route: ActivatedRoute,
      private authenticationService: AuthenticationService,
      private UserService :UserService,
      private universitydetailService: UniversityDetailService,
      private router: Router,
      private headerMenuService: HeaderMenuService,
      private _service: NotificationsService,
      private loadingService: LoadingService,

      private compareService: CompareService,
    ){



    }

    @ViewChild(DataTable) corsesTable:any;
    private corsesResource = new DataTableResource([]);
    //private corsesTable:any = null;
    private sub:any;
    private corses_table:any[];
    private corses_count:any;
  	ngOnInit(){ 
       this.universitydetailService.university.subscribe(data =>this.universityrating = data, 
                error => console.log(error),
                () => console.log(this.universityrating)); 

        this.sub = this.route.params.subscribe(params => {
            let slug = params['slug'];
            this._pageService.getUniversitydetails(slug)
                .subscribe(data =>{
                     this.university = data.data;
                     this.universitydetailService.reLoadResult(slug); 
                    



                    this.corses_table = [];
                    this.corses_count = 0;
                    for(let course of this.university.courses){
                      this.corses_table.push({
                        course_name : course.course.name,
                        stream_name : course.stream.name,
                        meta : course.meta
                      });
                      this.corses_count++;
                    }

                    this.corsesResource = new DataTableResource(this.corses_table);
                    
                    /*filmResource = new DataTableResource(films);
                    films = [];
                    filmCount = 0;

                    @ViewChild(DataTable) filmsTable;*/

                  }, 
                error => console.log(error)
            ); 
        });

  	}

    reloadFilms(params:any) {

      /*this._items = this._items.filter(item => {
            let found = false;
            this.columns.toArray().forEach(column => {
                if(search_key.indexOf(item[column.property]) >= 0){
                    found = true;
                }
            });

            if(search_key == null || search_key == "" || search_key == undefined)
                return true;


            return found;
        })*/

        this.corsesResource.query(params).then(films => this.corses_table = films);
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }
    
    isComment(){
      let reatings:any = [];
      let flag:any=true;
      let user=this.authenticationService.userdetail();
     // console.log(user);
      reatings = this.universityrating.data;
      //console.log(reatings);
      if(reatings== undefined || reatings.length==0 || user==false){
        return true;
      }
      for (var i = 0; i < reatings.length; i++) {
        //console.log(reatings[i].id);
        if(reatings[i].user_id == user.user.id)
          flag=false;
      }
      return flag;
      /*reatings.filter((rating:any) => {
        console.log(rating.user_id);
        console.log(user.user.id);
        console.log(rating.user_id == user.user.id);
        if(rating.user_id == user.user.id)
          return false;
        else
          return true;
      }
      );*/
      //return reatings.filter((rating:any) => rating.user_id == user.user.id)>0;
    }
    private applyNow() : void {
        this.loading = true;
        let user=this.authenticationService.userdetail();
     
        if(user){
          this.model.user_id=user.user.id;
        }else{
          this.model.user_id=null;
        }
        this.model.university_id=this.university.id;
        this._pageService.applyNow(this.model)
        .subscribe(
               data => {
                  this.loadingService.setLoading(false);
                    if(data.message !== undefined){
                        this._service.success("Success", data.message);
                    }else{
                        this._service.success("Success","Your application submit to university.");
                    }
                     this.universitydetailService.reLoadResult(this.university.id);
                },
                error => {
                   this.loadingService.setLoading(false);
                    var error_data = error.json();
                    if(error_data.message != undefined){
                        if(error_data.message.type == undefined){
                          error_data.message = {
                            type  : null,
                            message : error_data.message
                          };
                        }
                        switch (error_data.message.type) {
                          case "expired":
                            this.headerMenuService.setIsLogin(false);
                            this.authenticationService.logout();
                            this._service.error("Error",error_data.message.message);
                            this.router.navigate(['/login']);
                            break;

                          case "invalid":
                            this._service.error("Error",error_data.message.message);
                            break;
                          
                          default:
                            this._service.error("Error",error_data.message.message);
                            break;
                        }
                        
                      }else{
                        this._service.error("Error", "Error while updating profile, Please try again.");
                      }
                }
            );
    }
     private ratingNow() : void {
        this.model.universitys_id=this.university.id;
        this.UserService.submitRating(this.model)
        .subscribe(
                data => {
                   this.loadingService.setLoading(false);
                    if(data.message !== undefined){
                        this._service.success("Success", data.message);
                    }else{
                        this._service.success("Success","Your application submit to university.");
                    }
                    this.loading = false;
                    this.universitydetailService.reLoadResult(this.university.slug);
                },
                error => {
                   this.loadingService.setLoading(false);
                   var error_data = error.json();
                    if(error_data.message != undefined){
                    if(error_data.message.type == undefined){
                      error_data.message = {
                        type  : null,
                        message : error_data.message
                      };
                    }
                    switch (error_data.message.type) {
                      case "expired":
                        this.headerMenuService.setIsLogin(false);
                        this.authenticationService.logout();
                        this._service.error("Error",error_data.message.message);
                        this.router.navigate(['/login']);
                        break;

                      case "invalid":
                        this._service.error("Error",error_data.message.message);
                        break;
                      
                      default:
                        this._service.error("Error",error_data.message.message);
                        break;
                    }
                    
                  }else{
                    this._service.error("Error", "Error while updating profile, Please try again.");
                  }
                }
            );
    }
    
    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];
     private resetRatingStar() {
        this.overStar = null;
    }
    //call this method when over a star
    private overStarDoSomething(value:number):void {
        this.overStar = value;
        this.ratingPercent = 100 * (value / this.maxRateValue);
    };


    isFound(obj:any){
      return this.compareService.isFound(obj);
    }

    isLimit(){
      return this.compareService.isLimit();
    }


    showLimitMessage(obj?:any){
      if(obj == undefined || obj == null){
        obj.id = 0;
      }
      if(this.isLimit() && !this.isFound(obj)){
        this._service.error("Error","You have already selected 3 universities. You can compare maximum 3 universities.");
      }
    }



    onChange(obj:any, event:any) {
      var isChecked = event.currentTarget.checked;
      if(isChecked){
        this.compareService.addToCompare(obj);
      }else{
        this.compareService.removeFromCompare(obj);
      }
    }
}
