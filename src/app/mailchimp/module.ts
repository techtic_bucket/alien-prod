import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MailChimpComponent} from './mail-chimp.component';

import { JsonpModule } from '@angular/http';

const INLINE_EDITOR_COMPONENTS = [MailChimpComponent];

@NgModule({
  imports: [CommonModule, FormsModule, JsonpModule],
  declarations: INLINE_EDITOR_COMPONENTS,
  exports: INLINE_EDITOR_COMPONENTS
})
export class MailChimpModule {
}
