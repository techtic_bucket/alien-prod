import { Component, forwardRef, Input, OnInit, Output, EventEmitter, ElementRef, ViewChild, Renderer } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { Http, Jsonp,  Headers, RequestOptions, RequestOptionsArgs, Request, RequestMethod, Response  } from '@angular/http';


import {Observable} from "rxjs/Rx";
import { Cons } from '../service/cons';


export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MailChimpComponent),
    multi: true
};

export interface InputConfig {
    apiKey: string;
    listId: string;
    dataCenter: string;
    version: string;
    placeholder: string;
    type: string;
    disabled: boolean;
    label: string;
    button: string;
}

const inputConfig: InputConfig = {
    apiKey: '',
    listId: '',
    dataCenter: '',
    version: '3.0',
    placeholder: 'Enter your email address...',
    type: 'text',
    disabled: false,
    label: 'SUBSCRIBE TO OUR NEWSLETTER',
    button: 'SUBSCRIBE',
};


const MAIL_CHIMP_TEMPLATE = `<div id="newsletter-edit-wrapper">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 newsletter">
        <h4 [innerHtml]="label"></h4>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 email">
        <input class="email_text" [(ngModel)]="value" placeholder="{{placeholder}}" type="text">
        <button class="subscribe" (click)="onClick(value)" type="button" [innerHtml]="button"></button>
      </div>
    </div>
</div>`;


const MAIL_CHIMP_CSS = ``;

@Component({
    selector: 'mail-chimp',
    template: MAIL_CHIMP_TEMPLATE,
    styles: [MAIL_CHIMP_CSS],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
})
export class MailChimpComponent implements OnInit, InputConfig { //ControlValueAccessor,
    cons:any;
    // inline edit form control
    //@ViewChild('inlineEditControl') inlineEditControl:any;
    @Output() public onSend: EventEmitter<any> = new EventEmitter();
    @Output() public onBeforeSend: EventEmitter<any> = new EventEmitter();
    @Output() public onSuccess: EventEmitter<any> = new EventEmitter();
    @Output() public onError: EventEmitter<any> = new EventEmitter();
  
    //Configuration attribute

    @Input() public apiKey: string;
    @Input() public version: string;
    @Input() public listId: string;
    @Input() public dataCenter: string;

    @Input() public placeholder: string;
    @Input() public type: string;
    @Input() public disabled: boolean;
    @Input() public label: string;
    @Input() public button: string;


    // select's attribute
    @Input()
    set options(options) {
        if (options['data'] === undefined) {
            this._options = {};
            this._options['data'] = options;
            this._options['value'] = 'value';
            this._options['text'] = 'text';
        } else {
            this._options = options;
        }
    }

    get options() { return this._options; }

    public onChange: any = Function.prototype;
    public onTouched: any = Function.prototype;

    private _value: string = '';
    private preValue: string = '';
    private editing: boolean = false;
    private isEmpty: boolean = false;
    private _options:any;

    get value(): any { return this._value; };

    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChange(v);
        }
    }

    constructor(
        element: ElementRef, 
        private jsonp:Jsonp, 
        private _renderer: Renderer,
        private _http: Http,
    ) {
         this.cons = Cons.live(); 
    }

    ngOnInit() {
        this.initProperty('apiKey');
        this.initProperty('version');
        this.initProperty('listId');
        this.initProperty('dataCenter');

        this.initProperty('placeholder');
        this.initProperty('type');
        this.initProperty('disabled');
        this.initProperty('label');
        this.initProperty('button');
    }

    public registerOnChange(fn: (_: any) => {}): void { this.onChange = fn; }

    public registerOnTouched(fn: () => {}): void { this.onTouched = fn; };



    onClick(value:any) {
        let data = {
            email : value
        }
        this.onBeforeSend.emit(data);
        this._http.post(this.cons.subscribe, data).map(response => response.json()).subscribe(
            res =>  this.onSuccess.emit(res),
            error => this.onError.emit(error.json())
        );
    }
  
    private initProperty(property: string): void {
        this[property] = typeof this[property] !== 'undefined'
            ? this[property]
            : inputConfig[property];
    }
}
