import 'rxjs/Rx';
import { Component, OnInit, OnDestroy  } from '@angular/core'; 
import { ActivatedRoute } from '@angular/router';
import { UserService, AuthenticationService, HeaderMenuService , LoadingService} from './service/index';
import { Router } from '@angular/router';
import { User } from './_models/user';

import {GlobalEventsManager} from "./includes/globals";

import {NotificationsService} from 'angular2-notifications';
import { AuthService } from "angular2-social-login";

@Component({
  selector: 'loginregister-app',
  templateUrl: './view/login-register.html',
  providers:[UserService]
})

export class LoginregisterComponent  {
    model: any = {};
    loading = false;
    _token:string;
    
    user: any;
    sub: any;
    constructor(
        private router: Router,
        private userService: UserService,
        private authenticationService: AuthenticationService,
        private _service: NotificationsService,
        public _auth: AuthService,
        private loadingService: LoadingService,
        private _headerMenuService: HeaderMenuService) {

         }

    socialSignIn(provider:any){

        this.sub = this._auth.login(provider).subscribe(
            data => {
                this.loadingService.setLoading(true);
                this.authenticationService.socialSignIn(data)
                .subscribe(
                    dt => {
                        this._headerMenuService.setIsLogin(true);
                        this.loadingService.setLoading(false);
                        this.router.navigate(['/my-account']);
                    },
                    er => {
                        this._headerMenuService.setIsLogin(false);
                        this._service.error("Error",'Error while login, Please try again.');  
                        this.loadingService.setLoading(false);
                    }
                );
            },
            error => {
                this._headerMenuService.setIsLogin(false);
                this._service.error("Error",'Error while login, Please try again.');
            }
        )

       
    }

    register(ngForm?:any) {
        this.loadingService.setLoading(true);
        this.userService.create(this.model)
            .subscribe(
                data => {
                    if(data.message !== undefined){
                        this._service.success("Success", data.message);
                    }else{
                        this._service.success("Success","You have registered successfully.");
                    }
                    ngForm.reset();
                    this.router.navigate(['/login']);
                    this.loadingService.setLoading(false);
                },
                error => {
                    var error_data = error.json();
                    this._service.error("Error",error_data.message);
                    this.loadingService.setLoading(false);
                }
            );
    }
    private login() : void {
        this.loadingService.setLoading(true);
        this.authenticationService.login(this.model.login_email, this.model.login_password)
            .subscribe(
                data => {
                    this._headerMenuService.setIsLogin(true);
                    this.router.navigate(['/my-account']);
                },
                error => {
                    var error_data = error.json();
                    this._headerMenuService.setIsLogin(false);
                    this._service.error("Error",error_data.message);

                    this.loadingService.setLoading(false)
                }
            );
    }
}


@Component({
  selector: 'loginregister-app',
  templateUrl: './view/login-register.html',
  providers:[UserService, GlobalEventsManager],
})
export class LogoutComponent {
    constructor(
        private router: Router, 
        private authenticationService: AuthenticationService, 
        private _headerMenuService: HeaderMenuService,
        public _auth: AuthService) { }

    ngOnInit() {
        this._headerMenuService.setIsLogin(false);
        this.authenticationService.logout();
        this._auth.logout().subscribe(
          (data)=>{console.log(data);}
        )
        this.router.navigate(['/login']);

    }
}

