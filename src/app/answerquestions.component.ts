import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from './service/page.service';
@Component({
  selector: 'faq-app',
  templateUrl: './view/answerquestions.html',
	providers: [PageService]
})


export class AnswerquestionsComponent  {
 answerquestions:any; 
  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getFAQ()
  			.subscribe(data =>this.answerquestions = data.data, 
                error => console.log(error),
               () => ''); 
 }
}