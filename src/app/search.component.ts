import { Component, OnInit, OnDestroy  } from '@angular/core'; 
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {NotificationsService} from 'angular2-notifications';

import { Search } from './_models/index';

import { PageService } from './service/page.service';
import { CompareService, SearchService , LoadingService} from './service/index';


import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 	'search-page',
	  templateUrl:'./view/search.html',
    providers: [SearchService, PageService],
})

export class SearchComponent  {
    model: any = {};
    formComponent:any; 
    university:any; 
    components:any; 
    courses:any; 
    streams:any; 

    search_date:any = {};
    _search_date:any = {};
    rangeValues:any = {};
    search_slider_value:any = {};

  	constructor (
      private _service: NotificationsService,
      private compareService: CompareService,
      private _searchService: SearchService,
      private _pageService: PageService,
      private loadingService: LoadingService
    ) {
      let search_data:any={};
      let sdata = this._searchService.getSearchData();
      if(sdata == undefined || sdata == null || sdata == "")
        search_data = {};
      else
        search_data = sdata;

      this._searchService.search_slider_value.subscribe(
          value => {
            this.search_slider_value = value;
          }
      );

      this._searchService.university.subscribe(
          value => {
            this.university = value;
          },
          error => {
            this._service.error("Error","Server Error.");
          }
      );

      this.loadingService.setLoading(true);
      this._pageService.getComponent().subscribe(
          value => {
            this.components = value.data;
            let component:any = {};

            let keyword = (search_data != undefined && search_data.keyword != undefined && search_data.keyword != null) ? search_data.keyword : "";
            
            let campus = (search_data != undefined && search_data.campus != undefined && search_data.campus != null) ? search_data.campus : "";

            let course = (search_data != undefined && search_data.course != undefined && search_data.course != null) ? search_data.course : "";
            
            let stream = (search_data != undefined && search_data.stream != undefined && search_data.stream != null) ? search_data.stream : "";

            if(search_data == null || search_data.fees == undefined  || search_data.fees.length == 0){
              search_data.fees = [this.components.fees.min,this.components.fees.max];
            }
            this.change_slider(search_data.fees, 'fees');

            this.formComponent = new FormGroup({
              keyword: new FormControl(keyword),
              campus: new FormControl(campus),
              course: new FormControl(course),
              stream: new FormControl(stream),
              duration: new FormControl(search_data.duration || null),
              intakes: new FormControl(search_data.intakes || null),
              fees: new FormControl(search_data.fees || null),
              last_education: new FormControl(search_data.last_education || null),
              last_education_percentage: new FormControl(search_data.last_education_percentage || null),
              entry_criteria: new FormControl(search_data.entry_criteria || null),
              marks_ietls: new FormControl(search_data.marks_ietls || null),
              marks_toefl: new FormControl(search_data.marks_toefl || null),
              marks_pte: new FormControl(search_data.marks_pte || null),
              marks_sat: new FormControl(search_data.marks_sat || null),
              marks_gre: new FormControl(search_data.marks_gre || null)
            });


          },
          error => {
            this._service.error("Error","Server Error.");
          }, 
          () => this.loadingService.setLoading(false)
      );


      this._pageService.getCourses().subscribe(
          value => this.courses = value.data,
          error => this._service.error("Error","Server Error.")
      );

      this._pageService.getStreams().subscribe(
          value => this.streams = value.data,
          error => this._service.error("Error","Server Error.")
      );

      this._searchService.reLoadResult();
    }

    

    isFound(obj:any){
      return this.compareService.isFound(obj);
    }

    isLimit(){
      return this.compareService.isLimit();
    }

    showLimitMessage(obj?:any){
      if(obj == undefined || obj == null){
        obj.id = 0;
      }
      if(this.isLimit() && !this.isFound(obj)){
        this._service.error("Error","You have already selected 3 universities. You can compare maximum 3 universities.");
      }
    }

    onChange(obj:any, event:any) {
      var isChecked = event.currentTarget.checked;
      if(isChecked){
        this.compareService.addToCompare(obj);
      }else{
        this.compareService.removeFromCompare(obj);
      }
    }

    change_slider(value:any, comp?:any){
      this.search_slider_value[comp] = value;
      this._searchService.setSliderValue(this.search_slider_value);
    }
    updateSearch(value?:any){
      if(value != undefined){
        this._search_date = value;
        this._search_date.limit = 12;
        this._search_date.page = 1;
      }
      this._searchService.setSearchData(this._search_date);
      this._searchService.reLoadResult();
    }

    paginate(event:any) {
      this._search_date.limit = event.rows;
      this._search_date.page = parseInt(event.page) + 1;
      this.updateSearch();
    }

    
}