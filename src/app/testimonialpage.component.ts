import {Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import {PageService } from './service/page.service';

import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {NotificationsService} from 'angular2-notifications';
import { LoadingService} from './service/index';


@Component({
  selector: 'testimonialpage-app',
  templateUrl: './view/testimonialpage.html',
	providers: [PageService]
})


export class TestimonialpageComponent  {
    testimonials:any; 


    constructor (
      private _pageService: PageService,
      private _service: NotificationsService,
      private loadingService: LoadingService
      ) { 
      this.getServerData({limit : 10, page : 1 }); 
    }
    paginate(event:any) {
        this.getServerData({limit : event.rows, page : (parseInt(event.page) + 1) }); 
    }

    getServerData(event:any){ 
      this.loadingService.setLoading(true);
      this._pageService.getTestimonial(event).subscribe(
        response =>{
          if(response.error) { 
             this._service.error("Error","Server Error");
          } else {  
            this.testimonials = response;
          }
        },
        error =>{
           this._service.error("Error","Server Error");
        },
        ()=> this.loadingService.setLoading(false)
      );
    }
}
 
/*export class TestimonialComponent {
  testimonialpage : any;
  constructor (private _pageService: PageService) {
      // this.getServerData(1); 
      console.log(123);
    }
}*/