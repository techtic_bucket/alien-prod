import { Component, OnInit } from '@angular/core';
 
import { User } from './_models/index';
import { UserService , LoadingService} from './service/index';
 
@Component({
    templateUrl: './view/test.html'
})
 
export class TestComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
 
    constructor(private userService: UserService, private loadingService: LoadingService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
 
    ngOnInit() {
       // this.loadAllUsers();
    }
 
    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }
 
    private loadAllUsers() {
    //    this.userService.getAll().subscribe(users => { this.users = users; });
    }
}