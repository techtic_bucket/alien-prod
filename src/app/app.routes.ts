import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from "./about.component";
import { HomeComponent } from "./home.component";
import { Coursescomponent } from "./courses-component";
import { BlogComponent } from "./blog.component";
import { BlogDetailComponent } from "./blogdetail.component";
import {PageComponent} from "./page.component";
import {TestimonialpageComponent} from "./testimonialpage.component";
import {UniversityComponent} from "./university.component";
import {LoginregisterComponent, LogoutComponent} from "./loginregister.component";
import {CompareComponent} from "./compare.component";
import {UniversitydetailsComponent} from "./universitydetails.component";
import {AnswerquestionsComponent} from "./answerquestions.component";
import {StudentratingpageComponent} from "./studentratingpage.component";
import {TestComponent} from "./test.component";
import {LatestnewslistComponent} from "./latestnewslist.component";
import {NewsdetailsComponent} from "./newsdetails.component";
import {EventsComponent} from "./events.component";
import {EventsdetailComponent} from "./eventsdetail.component";
import {ForgotComponent, ResetPasswordComponent} from "./forgot.component";
import {SearchComponent} from "./search.component"; 
import {AskQuestionComponent} from "./askquestion.component"; 
import {ContactUsComponent} from "./contact-us-component";


import {MyAccountComponent, MyAccountChanagePasswordComponent, MyAccountRatingComponent, MyAccountProfileEditComponent, MyAccountEditProfileComponent, ActivityComponent, EducationComponent, InteractiveAssessmentComponent } from "./my-account.component";  
import { AuthGuard } from './_guards/index';

const routes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'page/:id', component: PageComponent},
    {path: 'about', component: AboutComponent},
    {path: 'blog', component: BlogComponent},
    {path: 'blog/:slug', component: BlogDetailComponent},
    {path: 'testimonials', component: TestimonialpageComponent},  
    {path: 'login', component: LoginregisterComponent},
    {path: 'registration', component: LoginregisterComponent},
    {path: 'courses', component: Coursescomponent},
    {path: 'top-university', component: UniversityComponent},
    {path: 'compare', component: CompareComponent},
    {path: 'university/:slug', component: UniversitydetailsComponent}, 
    {path: 'answer-questions ', component: AnswerquestionsComponent},
    {path: 'student-rating', component: StudentratingpageComponent},
    {path: 'test', component:TestComponent}, 
    {path: 'latest-news', component:LatestnewslistComponent},
    {path: 'news/:slug',component:NewsdetailsComponent},
    {path: 'events',component:EventsComponent},
    {path: 'events/:slug',component:EventsdetailComponent},
    {path: 'forgot-password',component:ForgotComponent},
    {path: 'reset-password/:code',component:ResetPasswordComponent},
    {path: 'ask-questions',component:AskQuestionComponent},

    {path: 'my-account', canActivate: [AuthGuard],
        children: [
          { path: '', component:MyAccountComponent },
          { path: 'change-password', component: MyAccountChanagePasswordComponent },
          { path: 'rating', component: MyAccountRatingComponent },
          { path: 'edit', component: MyAccountProfileEditComponent },
          { path: 'upload-profile-image', component: MyAccountEditProfileComponent },
          { path: 'activity',component:ActivityComponent },
          { path: 'education',component:EducationComponent },
          { path: 'interactive-assessment',component:InteractiveAssessmentComponent },
        ]
    }, 
    {path: 'logout', component:LogoutComponent, canActivate: [AuthGuard]}, 
    {path: 'search', component:SearchComponent},  
    {path: 'contact-us', component:ContactUsComponent},    
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
 
