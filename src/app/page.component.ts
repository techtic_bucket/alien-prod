import { Component, OnInit, OnDestroy  } from '@angular/core'; 
import { ActivatedRoute } from '@angular/router';
import { PageService , LoadingService} from './service/index';

@Component({
  selector: 'app-page',
  templateUrl: './view/page.html',
  styles: [``]
})



export class PageComponent implements OnInit, OnDestroy  { 
  //id    = '1';
  sub: string;
  page: any;
  constructor(private route: ActivatedRoute,
              private loadingService: LoadingService,
              private _pageService: PageService){
       
  }


  ngOnInit(){
      this.route.params.subscribe(params => {
        let id = params['id']; 
        this._pageService.getpage({slug:id})
        .subscribe(data =>this.page = data.data, 
                error => console.log(error),
                () => console.log(this.page)); 
      });
	}

	ngOnDestroy(){
      // console.log(2);
	}


}
