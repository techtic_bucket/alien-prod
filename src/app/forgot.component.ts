import 'rxjs/Rx';
import { Component, OnInit, OnDestroy  } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { PageService, AuthenticationService, HeaderMenuService , LoadingService} from './service/index';
import { Router,ActivatedRoute } from '@angular/router';
import { User } from './_models/user';

import {GlobalEventsManager} from "./includes/globals";

import {NotificationsService} from 'angular2-notifications';
import { AuthService } from "angular2-social-login";

@Component({
  selector: 'forgotpassword-app',
  templateUrl: './view/forgot-password.html',
  providers:[PageService, GlobalEventsManager]
})
export class ForgotComponent {
  model: any = {};
  loading = false;
    constructor(
        private router: Router, 
        private authenticationService: AuthenticationService, 
        private _headerMenuService: HeaderMenuService,
        private _pageservice: PageService,
        private _service: NotificationsService,
        public _auth: AuthService,
        public loadingService:LoadingService) { }
 
    ngOnInit() {
      console.log(this.authenticationService.isLogin());
     if(this.authenticationService.isLogin())
          this.router.navigate(['/my-account']);

    }
    submitNow(form:any){

        this._pageservice.forgetPassword(this.model)
        .subscribe(
                data => {
                  this.loadingService.setLoading(false);
                    if(data.message !== undefined){
                        this._service.success("Success", data.message);
                    }else{
                        this._service.success("Success","Reset password link sent to your email.");
                    }
                    this.loading = false;
                    form.reset();
                },
                error => {
                  this.loadingService.setLoading(false);
                   var error_data = error.json();
                    if(error_data.message != undefined){
                    if(error_data.message.type == undefined){
                      error_data.message = {
                        type  : null,
                        message : error_data.message
                      };
                    }
                    this._service.error("Error",error_data.message.message);
                  }else{
                    this._service.error("Error", "there is some error occured.Please try later.");
                  }
                }
            );
    }
}


@Component({
  selector: 'resetpassword-app',
  templateUrl: './view/reset-password.html',
  providers:[PageService, GlobalEventsManager],
})

export class ResetPasswordComponent {
  model: any = {};
  loading = false;
  user:any;
  resetPasswordFrom:any;
  private sub:any;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService, 
        private _headerMenuService: HeaderMenuService,
        private _pageservice: PageService,
        private _service: NotificationsService,
        public _auth: AuthService,
        public loadingService:LoadingService) {

            this.resetPasswordFrom = new FormGroup({
            new_password: new FormControl(null),
            confirm_password: new FormControl(null)
          });
        }




    ngOnInit() {
      if(this.authenticationService.isLogin())
      {
          this.router.navigate(['/my-account']);
      }

      this.sub = this.route.params.subscribe(params => {
            let code = params['code'];
            console.log(code);
        this._pageservice.resetPassword({code:code})
        .subscribe(
                data =>this.user=data.data,
                error =>{
                   this.loadingService.setLoading(false);
                var error_data = error.json();
                          if(error_data.message != undefined){
                              if(error_data.message.type == undefined){
                                error_data.message = {
                                  type  : null,
                                  message : error_data.message
                                };
                              }
               
                  this._service.error("Error",error_data.message.message);
                  
                }
                else{
                  this._service.error("Error", "Incorrect code or Code is not found.");
                }
                this.router.navigate(['/login']);
                },
                ()=>console.log(this.user)
            );
        });
    }
    reset_password(value:any){
        this.loadingService.setLoading(true);
        value.id=this.user.id;
        value.code=this.user.reset_code;
      this._pageservice.reset_password(value).subscribe(
        data => {
          this.loadingService.setLoading(false);
          if(data.message !== undefined){
              this._service.success("Success", data.message);
          }else{
                this._service.success("Success","Password successfully reset.");
          }
          this.router.navigate(['/login']);
        },
        error => {
          this.loadingService.setLoading(false);
          var error_data = error.json();
                    if(error_data.message != undefined){
                        if(error_data.message.type == undefined){
                          error_data.message = {
                            type  : null,
                            message : error_data.message
                          };
                        }
       
            this._service.error("Error",error_data.message.message);
          }else{
            this._service.error("Error", "Error while changing password, Please try again.");
          }
        }
      );
    }
}