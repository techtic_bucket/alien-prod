import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'stripTags'})

export class HtmlStripTagsPipe implements PipeTransform {
   transform(value: string){
  	return  value ? String(value).replace(/<[^>]+>/gm, '') : '';
  }
}