import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';


@Pipe({name: 'stripTags'})

export class HtmlStripTagsPipe implements PipeTransform {
   transform(value: string){
    return  value ? String(value).replace(/<[^>]+>/gm, '') : '';
  }
}

@Pipe({name: 'trim_text'})

export class TrimTextPipe implements PipeTransform {
   transform(value: string, limit?:"10", after?:"..."){
    if (!value) return '';
        var max = parseInt(limit);
        if (!max) return value;
        if (value.length <= max) return value;
        value = value.substr(0, max);
        return value + after;
  }
}

@Pipe({name: 'image'})

export class ImagePipe implements PipeTransform {
   transform(value: string, w?:any, h?:any, zc?:"1"){
      var url = "http://45.79.111.106/alien/web-api/public/index.php/timthumb?";
      //var url = "http://localhost/alien/web-api/public/index.php/timthumb?";


      if(value != undefined && value != null){
        value = "http://45.79.111.106/alien/web-api/"+value;
        //value = "http://localhost/alien/web-api/"+value;
      }

      url += "src="+value;
      url += "&default=" +"http://45.79.111.106/alien/images/default_post.jpg";
      //url += "&default=" +"http://localhost/alien/images/default_post.jpg";

      if(w != undefined && w != null)
        url += "&w="+w;

      if(h != undefined && h != null)
        url += "&h="+h;

      if(zc != undefined && zc != null)
        url += "&zc="+zc;
      else{
        url += "&zc=1";
      }
      return url;
  }
}


@Pipe({name: 'capitalize'})

export class CapitalizePipe implements PipeTransform {
   transform(value: string){
      return (value != null && value != undefined) ? value.charAt(0).toUpperCase() + value.substr(1).toLowerCase() : '';
  }
}


@Pipe({name: 'gender'})

export class GenderLablePipe implements PipeTransform {
   transform(value: string){
     if(value=='male')
       return 'Male';
     else if(value=='female')
       return 'Female';
     else
       return '-';
  }
}
@Pipe({ name: 'safeurl' })
export class SafeUrlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url:string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
} 


@Pipe({name: 'datetotime'})

export class DatetotimePipe implements PipeTransform {
   transform(value: string, formate?:any){
    /*if(formate != undefined){
       formate = "dd-mm-yyyy";
    }*/
    return value;
  }
}
@Pipe({name: 'nl2br'})

export class Nl2BrPipe implements PipeTransform {
  transform(value: string){
    if(value != undefined && value != null)
        return value.replace(/\n/g, '<br>');
    else
      return value;
  }
}
@Pipe({name: 'meta_name'})

export class MetaNamePipe implements PipeTransform {
  transform(value: string){
    if(value != undefined && value != null)
        return value.replace(/_/g, ' ').replace(/-/g, ' ').toLowerCase().replace(/\b[a-z]/g, letter =>  letter.toUpperCase() );
    else
      return value;
  }
}