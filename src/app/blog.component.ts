import { Component  } from '@angular/core'; 
import { ActivatedRoute, Router } from '@angular/router';
import {NotificationsService} from 'angular2-notifications';
import { PageService, LoadingService} from './service/index';

import { Observable } from 'rxjs/Rx'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  	selector: 		'blog-app',
  	templateUrl: 	'./view/blog.html',
	providers: 		[PageService]
})

export class BlogComponent  {
 	blogdata:any; 
 	recentblog:any;
 	searchForm:any;
  fetch_data : any;

  	constructor (
  		private _pageService: PageService,
    	private _service: NotificationsService,
      private router: Router,
      private route: ActivatedRoute,
    	private loadingService: LoadingService
    ) {
  		this.fetch_data = {limit : 10, page : 1 };
  	}


    update_search(value:any){
      this.router.navigate(['/blog'], { queryParams: { search: value.search }});

      if(value.search != null){
        this.fetch_data.search = value.search;
      }

      this.getServerData(this.fetch_data); 
    } 

  	paginate(event:any) {
        this.fetch_data.limit = event.rows;
        this.fetch_data.page = (parseInt(event.page) + 1) ;
        this.getServerData(this.fetch_data); 
    }

  	getServerData(event?:any){ 
      this.loadingService.setLoading(true);
		  this._pageService.getBlog(event).subscribe(
			response =>{
				if(response.error) { 
          			this._service.error("Error", "Server error.");
				} else {
					this.blogdata = response;
				}
				this.loadingService.setLoading(false);
			},
			error =>{
          		this._service.error("Error", "Server error.");
				this.loadingService.setLoading(false);
			}
		);
		
		return event;
	}

	ngOnInit(){ 

    let search:any = this.route.snapshot.queryParams['search'] || null;
    if(search != null){
      this.fetch_data.search = search;
    }

    this.getServerData(this.fetch_data); 

    this.searchForm = new FormGroup({
        search: new FormControl(search),
    });

		this._pageService.getBlog({limit:5})
  		.subscribe(
  			data =>this.recentblog = data.data, 
            error => console.log(error)
        );
    } 


 

}