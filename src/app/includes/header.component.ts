import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import {Http, Headers} from "@angular/http";
import { PageService } from '../service/page.service';
import { UserService, AuthenticationService, HeaderMenuService } from '../service/index';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {GlobalEventsManager} from "./globals";

@Component({
  selector: 'header-app',
  templateUrl: '../view/header.html',
  providers: [PageService, GlobalEventsManager],
})

export class HeaderComponent implements OnInit {
  name = 'Techtic Solutions';
  email = 'abhishek@techtic.com';
  menu:string;
  found:any;
  page:any;
  is_login : any;


  constructor (
    private http: Http, 
    private _pageService: PageService, 
    private globalEventsManager: GlobalEventsManager,
    private _headerMenuService: HeaderMenuService,
    private authenticationService: AuthenticationService) { 


    let subscription = this._headerMenuService.is_login.subscribe(
          value => this.is_login = value.is_login,
          error => true
      );

  }

	ngOnInit(){   
      this._pageService.getHeader()
                .subscribe(data =>this.menu = data, 
                error => console.log(error),
                () => ''); 
	}

 
}
