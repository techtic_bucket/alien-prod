import { Component, Input } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import {Http, Headers} from "@angular/http";

@Component({
  selector: 'my-account-sidebar',
  templateUrl: '../view/my-account-sidebar.html',
})

export class MyAccountSidebarComponent  {
  @Input() active_menu:string;

  constructor (private http: Http) {}

}
