import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { LoadingService } from '../service/index';

@Component({
  selector: 'loading-custom-app',
  template: '<div *ngIf="is_loading" class="loading-main" style="background: rgba(255,255,255,0.7);top:0;left:0;width:100%;height:100%;position: fixed;z-index: 999;"><div class="loading-container" style="top :50%; left:50%; transform: translate(-50%, -50%); position:fixed; display: table;"> <div style="loading-container-logo"> <img src="./images/logo.svg" style="width:250px;"/> </div> <div style="margin-top:30px; margin-left:auto; display:table; margin-right:auto; font-size:70px; color:#094293;">  <span class="fa fa-circle-o-notch fa-spin"></span> </div> </div></div>',
})

export class LoadingComponent implements OnInit {
  is_loading : boolean = true;
  count:number = 0;
  constructor (
    private http: Http, 
    private _loadingService: LoadingService) { 
    this.is_loading = false;
    this.count = 0;
  }

  ngOnInit(){
    let subscription = this._loadingService.loading.subscribe(
          value => {this.is_loading = value.is_loading; this.count = value.count;},
          error => true
      );
  }
 
}
