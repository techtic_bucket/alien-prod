import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../../service/page.service';
import {RatingModule} from "ng2-rating";

@Component({
  selector: 'studentrating-app',
  templateUrl: '../../view/home/studentrating.html',
  providers: [PageService]
})

export class StudentratingComponent  {
   studentrating:any;

   	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getStudentRating({limit:3})
  			.subscribe(data => this.studentrating = data.data, 
                error => console.log(error),
                () => ''); 

  	}
    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];
}
