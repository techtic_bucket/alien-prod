import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../../service/page.service';

@Component({
  selector: 'recentblog-app',
  templateUrl: '../../view/home/recentblog.html',
  providers: [PageService]
})

export class RecentBlogComponent  {
 blog:any; 
  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getBlog({limit:2})
  			.subscribe(data =>this.blog = data.data, 
                error => console.log(error),
                () => ''); 

  	}
}
