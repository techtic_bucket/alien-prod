import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../../service/page.service';

@Component({
  selector: 'latestnews-app',
  templateUrl: '../../view/home/latestnews.html',
  providers: [PageService]
})

export class LatestnewsComponent  {
  	news:any; 
  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getNews({limit:4})
  			.subscribe(data =>this.news = data.data, 
                error => console.log(error),
                () => console.log(this.news)); 

  	}
}
