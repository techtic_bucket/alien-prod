import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../../service/page.service';
import {RatingModule} from "ng2-rating";

@Component({
	selector: 'topuniversities-app',
	templateUrl: '../../view/home/topuniversities.html',
	providers: [PageService]
})

export class TopuniversitiesComponent  {
  	universities:any;

  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getUniversity({limit:4})
  			.subscribe(data => this.universities = data.data, 
                error => console.log(error),
                () => ''); 

  	}
    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];
}
