import { Component } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Search } from '../../_models/index';
import { Router } from '@angular/router';

import { PageService } from '../../service/page.service';
import { SearchService } from '../../service/index';



@Component({
  selector: 'banner-app',
  templateUrl: '../../view/home/banner.html',
  providers: [SearchService, PageService],
})

export class BannerComponent  {
	model: any = {};
	
	constructor(
		private http: Http, 
        private router: Router,
		private _searchService : SearchService
	) { }

	private search() : void {
        this._searchService.setSearchData(this.model);
        this.router.navigate(['/search']);
    }
}
