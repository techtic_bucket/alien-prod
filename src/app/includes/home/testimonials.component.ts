import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../../service/page.service';

@Component({
  selector: 'testimonials-app',
  templateUrl: '../../view/home/testimonials.html',
  providers: [PageService]
})

export class TestimonialsComponent  {
 testimonial:any; 
  	constructor (private _pageService: PageService) {}

  	ngOnInit(){ 
  		this._pageService.getTestimonial({limit:2})
  			.subscribe(data =>this.testimonial = data.data, 
                error => console.log(error),
                () => ''); 

  	}
}
