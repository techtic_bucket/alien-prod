import { Component } from '@angular/core';
import {Http, Headers} from "@angular/http";
import { PageService } from '../service/page.service';
import { CompareService, LoadingService } from '../service/index';
import 'rxjs/Rx';

import {NotificationsService, SimpleNotificationsComponent} from 'angular2-notifications';


@Component({
  selector: 'footer-app',
  templateUrl: '../view/footer.html',
  providers: [PageService],
})



export class FooterComponent {
  data:any; 
  page:any;
  apikey:any;
	constructor (
    private _pageService: PageService,
    private loadingService: LoadingService,
    private _service: NotificationsService,
    ) {
    this.apikey = "0a59cd086055786c8d444e13fa18dd21-us15";
  }

    public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: true,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: 'visible',
        rtl: false,
        animate: 'scale',
        position: ['right', 'top']
    };
      
  	ngOnInit(){ 
  		this._pageService.getFooter()
  			.subscribe(
            data => this.data =  data.data, 
            error => console.log(error)
        ); 

  	}

    newsletterSubmit(value:any){
        this.loadingService.setLoading(true);
    }
    
    onError(error:any){
      this.loadingService.setLoading(false);
      if(error.email != undefined){
        this._service.error("Error",error.email[0]);
      }else{
       this._service.error("Error",error.message);
      }
    }
    
    onSuccess(value:any){
        this.loadingService.setLoading(false);
       this._service.success("Success",value.message);
    }
    
   
}

@Component({
  selector: 'compare-universities',
  templateUrl: '../view/compare-universities.html',
})

export class CompareUniversitiesComponent{
  university : any;
  constructor (private http: Http, private compareService:CompareService) {
    //this.university = compareService.getCompare();

    let subscription = this.compareService.university.subscribe(
        value => this.university = value,
        error => true,
        () => this.university
    );
  }

  onChange(id:any, event:any) {
    var isChecked = event.currentTarget.checked;
    if(isChecked){
      this.compareService.addToCompare(id);
    }else{
      this.compareService.removeFromCompare(id);
    }
  }
}
