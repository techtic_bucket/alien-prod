import { Component, Input } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import {Http, Headers} from "@angular/http";

@Component({
  selector: 'innerbanner-app',
  templateUrl: '../view/innerbanner.html',
})

export class InnerbannerComponent  {
  @Input() title:string;
  @Input() bc:any;

  constructor (private http: Http) {}

}
