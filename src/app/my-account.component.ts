import { Component  } from '@angular/core'; 
import { ActivatedRoute } from '@angular/router';
import { PageService } from './service/page.service';

import {RatingModule} from "ng2-rating";

import { Observable } from 'rxjs/Rx'; 
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { UserService, AuthenticationService, HeaderMenuService , LoadingService, CompareService} from './service/index';
import { Router } from '@angular/router';
import { ReactiveFormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {NotificationsService} from 'angular2-notifications';
import { User } from './_models/user';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Cons } from './service/cons';

@Component({
  	selector: 		'user-my-accout-app',
  	templateUrl: 	'./view/my-account/index.html',
	  providers: 		[PageService]
})

export class MyAccountComponent{
 	  user:any;
    cons:any;

  	constructor (
      private _pageService: PageService,
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService
      ) {
  		  let user = this.authenticationService.userdetail();

        this.user = user.user;
        this.cons = Cons.live();
  	}

  	
}


@Component({
    selector:     'user-my-accout-edit-app',
    templateUrl:   './view/my-account/edit.html',
    providers:     [PageService]
})
export class MyAccountProfileEditComponent {
    editProfile:any;
    user:any;
    private formData:any;
    private currentDate: NgbDateStruct;
    constructor (private _pageService: PageService,
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private userService: UserService,
      private router: Router,
      private _service: NotificationsService,
      private headerMenuService: HeaderMenuService,
      private fb: FormBuilder,
      private route: ActivatedRoute) {

      this.formData = new FormData();

      let user = this.authenticationService.userdetail();
      this.user = user.user;

      this.editProfile = new FormGroup({
          name: new FormControl(this.user.name || null),
          email: new FormControl(this.user.email || null),
          gender: new FormControl(this.user.gender || null),
          dob: new FormControl(this.user.dob || null),
          contact_number: new FormControl(this.user.contact_number || null),
          address: new FormControl(this.user.address || null),
          state: new FormControl(this.user.state || null),
          country: new FormControl(this.user.country || null),
          zip_code: new FormControl(this.user.zip_code || null),
          qualification: new FormControl(this.user.qualification || null)
        });
    }

    changeListener($event:any): void {
      console.log($event)
      this.postFile($event.target);
    }
    postFile(inputValue: any): void {
      this.formData = new FormData();
      this.formData.append("file",  inputValue.files[0]);
    }

    getFormData(value:any){
      /*$.map(value, (value, index) => this.formData.append(index, value));

      this.formData.append('_method', 'put');*/
    }

    update_profile(value:any){
      //this.getFormData(value);
      //console.log(this.formData);
      //value = this.formData;

      this.loadingService.setLoading(true);
      this.userService.update(value).subscribe(
        data => {
          this.userService.updateUser(data.data);
          this._service.success("Success", "Profile successfully updated.");

          this.loadingService.setLoading(false);
          this.router.navigate(['/my-account']);
        },
        error => {
          var error_data = error.json();
          this.loadingService.setLoading(false);
          if(error_data.message != undefined){
            if(error_data.message.type == undefined){
              error_data.message = {
                type  : null,
                message : error_data.message
              };
            }
            switch (error_data.message.type) {
              case "expired":
                this.headerMenuService.setIsLogin(false);
                this.authenticationService.logout();
                this._service.error("Error",error_data.message.message);
                this.router.navigate(['/login']);
                break;

              case "invalid":
                this._service.error("Error",error_data.message.message);
                break;
              
              default:
                this._service.error("Error",error_data.message.message);
                break;
            }
            
          }else{
            this._service.error("Error", "Error while updating profile, Please try again.");
          }
        }
      );
    }
}

@Component({
    selector:     'user-my-accout-education-app',
    templateUrl:   './view/my-account/education.html',
    providers:     [PageService]
})
export class EducationComponent {
    user:any;
    educationFrom:any;
    courses:any;
    streams:any;

    is_set:boolean;
    is_edit:Observable<boolean>;
    _is_edit: BehaviorSubject<boolean>;

    constructor (private _pageService: PageService,
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private userService: UserService,
      private router: Router,
      private _service: NotificationsService,
      private headerMenuService: HeaderMenuService,
      private route: ActivatedRoute) {

      this._is_edit = <BehaviorSubject<boolean>>new BehaviorSubject(false);
      this.is_edit = this._is_edit.asObservable();

      this.is_edit.subscribe(data => this.is_set = data);
      
      this._pageService.getCourses().subscribe(
          value => this.courses = value.data,
          error => this._service.error("Error","Server Error.")
      );

      this._pageService.getStreams().subscribe(
          value => this.streams = value.data,
          error => this._service.error("Error","Server Error.")
      );
      let user = this.authenticationService.userdetail();
      this.user = user.user;
      this._is_edit.next(true);
      
      if(this.user.user_education == "" || this.user.user_education == null || this.user.user_education == undefined){

        this._is_edit.next(false);

        this.user.user_education = {};
      }
      this.educationFrom = new FormGroup({
          last_education: new FormControl(this.user.user_education.last_education || null),
          last_education_percentage: new FormControl(this.user.user_education.last_education_percentage || null),
          interested_degree: new FormControl(this.user.user_education.interested_degree || null),
          interested_course: new FormControl(this.user.user_education.interested_course || null),
          interested_country: new FormControl(this.user.user_education.interested_country || null),
          markes_ietls: new FormControl(this.user.user_education.markes_ietls || null),
          markes_toefl: new FormControl(this.user.user_education.markes_toefl || null),
          markes_pte: new FormControl(this.user.user_education.markes_pte || null),
          markes_sat: new FormControl(this.user.user_education.markes_sat || null),
          markes_gre: new FormControl(this.user.user_education.markes_gre || null)
        });

      }

    cancel_edit(value:boolean){
      this._is_edit.next(value);
    }
    update(value:any){
      this.loadingService.setLoading(true);
      this.userService.updateEducation(value).subscribe(
        data => {
          this._pageService.displaySuccessMessage(data,"Changes successfully updated.");
          this.userService.updateUser(data.data);
          this.loadingService.setLoading(false);
          this._is_edit.next(false);
        },
        error => {
          this.loadingService.setLoading(false);
          this._pageService.displayErrorMessage(error,"Error while updating education, Please try again.");
        }
      );
    }
}

@Component({
    selector:     'user-my-accout-education-app',
    templateUrl:   './view/my-account/interactive-assessment.html',
    providers:     [PageService]
})
export class InteractiveAssessmentComponent {
    user:any;
    educationFrom:any;
    university:any;
    _search_date:any;


    constructor (private _pageService: PageService,
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private userService: UserService,
      private router: Router,
      private _service: NotificationsService,
      private headerMenuService: HeaderMenuService,
      private compareService: CompareService,
      private route: ActivatedRoute) {
   
      let user = this.authenticationService.userdetail();
      this.user = user.user;
      let user_education:any;
      if(this.user.user_education != undefined){
        user_education = this.user.user_education;
      }

      let search_data:any = {
        course: user_education.interested_degree || '',
        stream: user_education.interested_course || '',
        country: user_education.interested_country || '',
        last_education: user_education.last_education || '',
        last_education_percentage: user_education.last_education_percentage || '',
        marks_ietls: user_education.markes_ietls || '',
        marks_toefl: user_education.markes_toefl || '',
        marks_pte: user_education.markes_pte || '',
        marks_sat: user_education.markes_sat || '',
        marks_gre: user_education.markes_gre || '',
        limit:10,
      };
      this._search_date = search_data;
      this.getServerData();
    }

    paginate(event:any) {
      this._search_date.limit = event.rows;
      this._search_date.page = parseInt(event.page) + 1;
      this.getServerData();
    }

    getServerData(){  
      this.loadingService.setLoading(true);
      this._pageService.getUniversity(this._search_date).subscribe(
        response =>{
          if(response.error) { 
            this._service.error("Server Error ","Please reload page or try again later.");
          } else {
            this.university = response;
          }
        },
        error =>{
          this._service.error("Server Error","Please reload page or try again later.");
        },
        () => this.loadingService.setLoading(false)
      );
    }

    isFound(obj:any){
      return this.compareService.isFound(obj);
    }

    isLimit(){
      return this.compareService.isLimit();
    }

    showLimitMessage(obj?:any){
      if(obj == undefined || obj == null){
        obj.id = 0;
      }
      if(this.isLimit() && !this.isFound(obj)){
        this._service.error("Error","You have already selected 3 universities. You can compare maximum 3 universities.");
      }
    }

    onChange(obj:any, event:any) {
      var isChecked = event.currentTarget.checked;
      if(isChecked){
        this.compareService.addToCompare(obj);
      }else{
        this.compareService.removeFromCompare(obj);
      }
    }

    
    update(){
      this.loadingService.setLoading(true);
      this.userService.getInteractiveAssessment().subscribe(
        data => {
          this.loadingService.setLoading(false);
          this._pageService.displayErrorMessage(data,"Changes successfully updated.");
          this.router.navigate(['/my-account/education']);
        },
        error => {
          this.loadingService.setLoading(false);
          this._pageService.displayErrorMessage(error,"Error while updating education, Please try again.");
        }
      );
    }
}

@Component({
    selector:     'user-my-accout-change-password-app',
    templateUrl:   './view/my-account/change-password.html',
    providers:     [PageService]
})
export class MyAccountChanagePasswordComponent {
    changePasswordFrom:any;
    constructor (private _pageService: PageService,
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private userService: UserService,
      private router: Router,
      private _service: NotificationsService,
      private headerMenuService: HeaderMenuService,
      private route: ActivatedRoute) {


        this.changePasswordFrom = new FormGroup({
            old_password: new FormControl(null),
            new_password: new FormControl(null),
            confirm_password: new FormControl(null)
          });

      }

    chnage_password(value:any){
      this.loadingService.setLoading(true);
      this.userService.chnage_password(value).subscribe(
        data => {
          this._service.success("Success", 'Password successfully changed.');
          this.loadingService.setLoading(false);
          this.router.navigate(['/my-account']);
        },
        error => {
          var error_data = error.json();
          this.loadingService.setLoading(false);
          if(error_data.message != undefined){
            if(error_data.message.type == undefined){
              error_data.message = {
                type  : null,
                message : error_data.message
              };
            }
            switch (error_data.message.type) {
              case "expired":
                this.headerMenuService.setIsLogin(false);
                this.authenticationService.logout();
                this._service.error("Error",error_data.message.message);
                this.router.navigate(['/login']);
                break;

              case "invalid":
                this._service.error("Error",error_data.message.message);
                break;
              
              default:
                this._service.error("Error",error_data.message.message);
                break;
            }
            
          }else{
            this._service.error("Error", "Error while changing password, Please try again.");
          }
        }
      );
    }
}

@Component({
    selector:     'user-my-accout-edit-profile-app',
    templateUrl:  './view/my-account/edit-profile.html',
    providers:    [PageService]
})
export class MyAccountEditProfileComponent {
    cons:any;
    image_upload_url:any;
    constructor (private _pageService: PageService, 
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private userService: UserService,
      private router: Router,
      private _service: NotificationsService,
      private headerMenuService: HeaderMenuService,
      private route: ActivatedRoute) {

        this.cons = Cons.live();
        this.image_upload_url = this.userService.getTokenUrl(this.cons.image_upload);

    }

    uploadedFiles: any[] = [];
    //msgs: Message[];

    onBeforeSend(event:any){
      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      event.xhr.setRequestHeader('Authorization', 'Bearer ' + currentUser.token);
    }

    onError(event:any) {
      let error_data  = JSON.parse(event.xhr.responseText);

      if(error_data.message != undefined){
        if(error_data.message.type == undefined){
          error_data.message = {
            type  : null,
            message : error_data.message
          };
        }
        switch (error_data.message.type) {
          case "expired":
            this.headerMenuService.setIsLogin(false);
            this.authenticationService.logout();
            this._service.error("Error",error_data.message.message);
            this.router.navigate(['/login']);
            break;

          case "invalid":
            this._service.error("Error",error_data.message.message);
            break;
          
          default:
            this._service.error("Error",error_data.message.message);
            break;
        }
        
      }else{
        this._service.error("Error", "Error while updating profile, Please try again.");
      }
    }
    onUpload(event:any) {
        for(let file of event.files) {
            this.uploadedFiles.push(file);
        }

        let response  = JSON.parse(event.xhr.responseText);
        
        if(response.status == 'success'){
          this.userService.updateUser(response.data);
          this._service.success("Success", "Profile successfully updated.");
          this.router.navigate(['/my-account']);
        }
    }
}

@Component({
    selector:     'user-my-accout-rating-app',
    templateUrl:  './view/my-account/rating.html',
    providers:    [PageService]
})
export class MyAccountRatingComponent {
  studentratingpage:any;
    constructor (private userService: UserService, 
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private route: ActivatedRoute) {
    }
    ngOnInit(){ 
      this.getReatings({limit : 10, page:1});
    }

    getReatings(obj:any){
      this.loadingService.setLoading(true);
      this.userService.getStudentRating(obj)
        .subscribe(
          data =>{
            this.studentratingpage = data; 
            this.loadingService.setLoading(false);
          }, 
          error => {
            this.loadingService.setLoading(false);
          }
        );
    }

    paginate(event:any) {
        this.getReatings({limit : event.rows, page : (parseInt(event.page) + 1) }); 
    }


    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];
}


@Component({
    selector:     'user-my-accout-activity-app',
    templateUrl:  './view/my-account/activity.html',
    providers:    [PageService]
})
export class ActivityComponent {
  studentactivitypage:any;
    constructor (private userService: UserService, 
      private authenticationService: AuthenticationService,
      private loadingService: LoadingService,
      private route: ActivatedRoute) {
    }
    ngOnInit(){

      this.getActivity({limit : 10, page:1});
    }

    getActivity(obj:any){
      this.loadingService.setLoading(true);
      this.userService.getStudentActivity(obj)
        .subscribe(
          data =>{
            this.studentactivitypage = data.data; 
            
            this.loadingService.setLoading(false);
          }, 
          error => {
            this.loadingService.setLoading(false);
          }
        );
    }

    paginate(event:any) {
        this.getActivity({limit : event.rows, page : (parseInt(event.page) + 1) }); 
    }


    private ratingStatesItems:any = [
        {stateOn: 'glyphicon-ok-sign', stateOff: 'glyphicon-ok-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-star-empty'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'},
        {stateOn: 'glyphicon-heart', stateOff: 'glyphicon-ban-circle'}
    ];

    get_searchdata(){

    }

}
