import { AlienPordPage } from './app.po';

describe('alien-pord App', function() {
  let page: AlienPordPage;

  beforeEach(() => {
    page = new AlienPordPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
